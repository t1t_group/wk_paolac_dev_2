<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 20 20" enable-background="new 0 0 20 20" xml:space="preserve">
<path d="M14.404,6.53h-3V4.485c0-0.768,0.49-0.947,0.836-0.947c0.344,0,2.117,0,2.117,0V0.164l-2.916-0.012
	c-3.238,0-3.975,2.517-3.975,4.128v2.25H5.595v3.477h1.871c0,4.464,0,9.841,0,9.841h3.938c0,0,0-5.43,0-9.841h2.656L14.404,6.53z"/>
</svg>
