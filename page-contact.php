<?php
/**
 * Template Name: Contact
 *
 */
get_header(); ?>

<div class="wrap">
	<?php if (have_posts()) : ?>
		<?php while (have_posts()) : the_post(); ?>
			<div class="page-contatti">
				<div class="row">
					<div class="col-12">
						<h2 class="linea">Contact Us</h2>
					</div>
				</div>
				<div class="row">
					<div class="col-12 notop nobot">
						
						
						<div class="col-6 padding20 notop nobot bordata module">
							<div id="map"></div><br/>
							<?php the_content();?>
						</div>
						<div class="col-5 notop nobot module">
							
							<?php 

							/*<form>
								<input type="text" placeholder="Name">
								<input type="text" placeholder="Mail">
								<input type="text" placeholder="Telephone">
								<textarea>Your message here</textarea>
								<input type="submit" value="send">
							</form>*/

							if(ICL_LANGUAGE_CODE=='it'){
								
								echo do_shortcode('[contact-form-7 id="57" title="Contatto ITA"]');
							}
							else{
								echo do_shortcode('[contact-form-7 id="59" title="Contatto ENG"]');
							}
							

							?>
						</div>
						<div class="col-1"></div>
					</div>
				</div>
				<br/><br/><br/><br/><br/>
			</div>
		<?php endwhile; ?>
	<?php endif; ?>

</div>
<?php get_footer(); ?>