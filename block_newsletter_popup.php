<?php

function get_newsletter_form($lang, $class = ""){

	$pp_title = ot_get_option( 'titolo_popup_newsletter' );
	$pp_subtitle = ot_get_option( 'testo_popup_newsletter' );
	?>

	<form action="https://a3b1i0.emailsp.com/Frontend/subscribe.aspx" class="<?php echo $class;?>" id="newsletter_form">
		<style type="text/css" skip_me="1" wp_skip_me="1">
			#pc_tipo {
				height: 30px;
				width: 100%;
			    margin: 0 0 20px 0;
			    line-height: 30px;
			    text-indent: 15px;
			    color: #000;
			    border: 0;
			    background: rgba(255,255,255,.9);
			    float: left;
			    border-radius: 0!important;
			    font: 400 100%/1.5 TradeGothicLTStd-BoldExt,sans-serif;
			    font-size: .55rem;
			    line-height: 1.8em;
			    text-transform: uppercase;
			    letter-spacing: .1rem;
			    padding-top: 4px;
			    background-image: url('<?php echo get_template_directory_uri(); ?>/img/arrow_bottom.png');
			    background-position: 95% center;
			    background-size: 10px 5px;
			    background-repeat: no-repeat;
			}

			#pc_tipo * {
				font-size: 1rem;
			}
		</style>
		
	    <input type="hidden" id="apgroup" name="apgroup" value="87">
	    <input type="hidden" name="list" value="6">
		<input type="hidden" id="lingua" name="campo32" value="<?php echo get_language_nl_code($lang); ?>">
		<input type="hidden" id="paese" name="campo8" value="<?php echo get_request_country(); ?>">
		<input type="hidden" name="confirm" value="off" />

	    <div class="field-group title-group">
	    	<div class="newsletter-text testo-medio">
	    		<span class="newsletter-title"><?php echo $pp_title; ?></span>
	    		<span class="newsletter-subtitle"><?php echo $pp_subtitle; ?></span>
    		</div>
    	</div>

    	<div class="field-group tipologia">
    		<select id="pc_tipo" name="campo39">
    			<option value=""><?php echo __("Select a typology", "webkolm"); ?></option>
    			<option value="cliente privato"><?php echo __("Private customer", "webkolm"); ?></option>
    			<option value="distributore rivenditore"><?php echo __("Distributor reseller", "webkolm"); ?></option>
    			<option value="press"><?php echo __("Press", "webkolm"); ?></option>
    		</select>
    	</div>

	    <div class="field-group email-group">
	        <input type="text" id="email" name="EMAIL" value="" placeholder="<?php echo __('enter your e-mail','webkolm'); ?>" required="required">
			<input name="Submit" type="submit" class="button" value="<?php echo __("Subscribe","webkolm"); ?>">
	    </div>
	    <div class="field-group field-checkbox">
	    	<!--label class="check-newsletter">
				<input type="checkbox" name="privacy" required="required">
			</label-->
			<div class="label-check-newsletter">
				<?php 
				if($lang == 'it'){ ?>
					Acconsento al trattamento dei miei dati personali nel rispetto del Reg 2016/679 UE e dichiaro di aver letto l’informativa sul trattamento dei dati personali*
				<?php } else { ?>
					I agree to the processing of my personal data in accordance with Reg 2016/679 EU and declare that I have read the information on the processing of personal data*
				<?php } ?>
			</div>
	    </div>

		<!--div class="overlay-trigger"></div-->
	</form>
	<?php 
}
?>