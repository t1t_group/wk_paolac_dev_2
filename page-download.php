<?php
/**
 * Template Name: Download
 *
 */
get_header(); ?>
	<?php if (have_posts()) : ?>
		<?php while (have_posts()) : the_post(); ?>

			<div class="row">
				
				<?php 
					if(isset($_GET['code']) && $_GET['code']=="active"){
						the_content();
					}
					else{
						?>
						<div class="col-1"></div>
						<div class="col-10">
							<h2><?php _e("Download area", 'paolac') ?></h2>
							<div class="padding20"><p><?php _e("Please provide your name and E-mail address to access the Download Area", "paolac");?></p>
								<?php
									if(ICL_LANGUAGE_CODE=='it'){
										echo do_shortcode('[CONTACT_FORM_TO_EMAIL id="1"]');
									}
									else{
										echo do_shortcode('[CONTACT_FORM_TO_EMAIL id="2"]');
									}
								?>
							</div>
						</div>
						<div class="col-1"></div>
						<?php
					}
				?>
			</div>
				
		<?php endwhile; ?>
	<?php endif; ?>
<?php get_footer(); ?>