<?php

 if($_GET['rel']!='tab'){
  get_header(); } else { ?>

<script>
     $(".overlay-close").click(function(){
          $("body").toggleClass("bloccoscroll");
          $("html").toggleClass("bloccoscroll");
          $("#singolo-overlay").removeClass("open");
          $("#singolo-overlay").removeClass("loadingOff");
          $(".wrapper-singolo").fadeOut();
          
          var paginaold="<?php echo $_POST['paginaold'];?>"
          window.history.pushState({path:paginaold},'',paginaold);
    });
</script>

<?php } ?>


<?php if(have_posts()) : while(have_posts()) : the_post(); 

    $thumb_m = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );
    $url_m = $thumb_m['0'];
    $link=types_render_field("url", array("raw"=>"true"));
    $id_designer=get_the_ID();
  ?>
  <div class="wrapper-singolo <?php if($_GET['rel']!='tab'){ echo "pagina-singola";}?>" >
    <span class="overlay-close"></span>

    <div class="row">
      <div class="col-1"></div>
      <div class="col-5 foto-designer module">
        <img src="<?php echo $url_m; ?>">
      </div>
      <div class="col-5 desc-designer module">
        <br/>
        <h3 class="linea"><?php the_title();?></h3>
        <div class="padding10">
            <?php the_content(); ?>

            <a href="<?= $link;?>" class="link_designer"><?= $link; ?></a>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-1"></div>
      <div class="col-10 corr-designer">
        
          <?php 

          // PRODOTTI CORRELATI DESIGNER

            $connected = new WP_Query( array(
            'connected_type' => 'designer_to_prodotti',
            'connected_items' => get_queried_object(),
            'posts_per_page' => 4,
             'orderby' => 'rand'
            ) );
              

            if ( $connected->have_posts() ) :
            ?>
          <?php 
          $id_corretto=icl_object_id(4, 'page', false);
          $url_shop=get_the_permalink($id_corretto);
          $id_designer=$post->ID;
          $slug_designer = $post->post_name;

          $url_definitiva=$url_shop.'/?designer='.$id_designer;

          ?>
          <h3><a href="<?=$url_definitiva?>"><?php _e("Designed for Paola C"); ?></a></h3>
          <div class="row nuovi_prodotti prodotti_quadrati products">
            <?php while ( $connected->have_posts() ) : $connected->the_post(); ?>
            
              <?php include(locate_template('block-related.php')); ?>

            <?php endwhile; ?>
           </div>
           <?php 
             $shop_page_url = get_permalink( icl_object_id(7692, 'page', false) );
           ?>
           <a href="<?= $shop_page_url?>?<?= $slug_designer ?>=0" class="centrato"><?php _e('See more products','paolac');?></a>
            
            <?php endif;
            wp_reset_query();

          ?>
        
        
      </div>
    </div>

  </div>
  <?php if($_GET['rel']!='tab'){  }else { ?><div id="loader-singolo"></div><?php } ?>

<?php endwhile; endif; ?>


<?php if($_GET['rel']!='tab'){ get_footer(); }else {

  ?><script>
  // DETERMINO ALTEZZA DEL BOX 
  function marginBox(){
    $altezza_box=$(".wrapper-singolo").outerHeight()+40;
    $altezza_page=$(window).height();

    if($altezza_box>=$altezza_page){
      $(".wrapper-singolo").addClass("marginato");
    }else{
      $(".wrapper-singolo").removeClass("marginato");
    }
  }
  marginBox()


  /* MANAGE RESIZE */

  if(is_touch_device()){
    $( window ).on( "orientationchange", function( event ) {
      marginBox()

    });
  }else{
    $(window).on('resize', function(e) {

      fissaFooter();
      marginBox()
    });
  }</script><?php

} ?>
