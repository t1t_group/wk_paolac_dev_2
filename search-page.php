<?php
/*
Template Name: Search Page
*/
?>
<?php
get_header(); ?>
<div class="wrap">
	<div class="row">
		<div class="col-12">
			<h1><? _e('Search', 'paolac')?></h1>

			<br/>
				<form role="search" method="get" class="search-form single_page" action="<?php echo home_url( '/' ); ?>" >
			    	<input type="search" class="search-field" placeholder="<?php _e('Search a product', 'paolac');?>" value="" name="s" title="Cerca:" /><br/><br/><br/>
			  		<input type="submit" class="search-submit" value="<?php _e('Start research', 'paolac');?>" />
			  		<input type="hidden" name="post_type" value="product" />
				</form><br/><br/><br/>
		</div>
	</div>
</div>			

<?php get_footer();