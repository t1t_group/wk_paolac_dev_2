<div class="row">
	<div class="col-12 filter-wrapper">
		<?php 
			$shop_page_url = get_permalink( woocommerce_get_page_id( 'shop' ) );
			$current_url='http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
			_e("FILTER BY:", 'paolac');

			// LISTA CATEGORIE PRODOTTO
			if($titolo_filtro!=""){
				echo "<span id='clear-filter' data-url='".$shop_page_url."'>".$titolo_filtro."</span>";
			}
			
		?>

			<?php // LISTA DESIGNER ?>
				<select class="filter-selection">
					<?php 
						$your_query = new WP_Query( array(
						    'post_type' => array( 'designers'),				
						    'posts_per_page' => -1,
							'post_status' => 'publish',
							'orderby' => 'menu_order',
		                	'order'=>'ASC'
					     ));

						
						/*
						if(!isset($_GET['designer'])){
							echo '<option>'.__("Designer", 'paolac').'</option>';
						}
						*/
						echo '<option>'.__("Designer", 'paolac').'</option>';


						while ( $your_query->have_posts() ) : $your_query->the_post();
							$id_designer=get_the_ID();
							$url_designer=$shop_page_url.'?designer='.$id_designer;
							//if($current_url==$url_designer){ $selected="selected";}else{ $selected="";}
							$selected="";
							?>
								<option value="<?= $url_designer?>" <?= $selected ?>><?= the_title(); ?></option>
							<?php
						endwhile;
						// reset post data (important!)
						wp_reset_postdata();					
					?>
				</select>
				
			<select class="filter-selection category-selection">
				<?php 

					/*
					if(is_archive() && !is_shop()){
						//echo '<option value="' . esc_url( $term_link ) . '"">'.$nome_tasso.'</option>';
					}else {
						echo '<option>'.__("Category", 'paolac').'</option>';
					}*/
					echo '<option>'.__("Category", 'paolac').'</option>';


					$terms = get_terms( 'product_cat' );
					foreach ( $terms as $term ) {
						$nome_tasso= $term->name ;
						
						if(ICL_LANGUAGE_CODE=="it"){
							$nome_tasso=substr($nome_tasso, 0, strpos($nome_tasso, ' /'));
						}
						else { 
							$nome_tasso=strstr($nome_tasso, ' /'); 
							$nome_tasso=substr($nome_tasso, 2); 
						}

					    $term_link = get_term_link( $term );
					    if(ICL_LANGUAGE_CODE=="it"){
					    	$term_link=str_replace("product-category", "categoria-prodotto", $term_link);
					    }
					    //if($current_url==$term_link){ $selected="selected";}else{ $selected="";}
					    $selected="";
					    echo '<option value="' . esc_url( $term_link ) . '"" '.$selected.'>'.$nome_tasso.'</option>';
					}

				?>
			</select>

		
	
	</div>
</div>