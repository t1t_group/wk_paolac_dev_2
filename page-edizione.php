<?php
/**
 * Template Name: Editions
 *
 */
get_header(); ?>
	<?php if (have_posts()) : ?>
		<?php while (have_posts()) : the_post(); ?>

			<?php
				if("yes"=="yes"){

				  echo the_content();

				}else{
			?>

			<?php 
			// ELENCO POST EDIZIONI
			$your_query = new WP_Query( array(
			    'post_type' => array( 'paolac-edizione'),				
			    'posts_per_page' => -1,
				'post_status' => 'publish',
				'orderby' => 'menu_order',
            	'order'=>'ASC'
		     ));
			$contatore=0;

			while ( $your_query->have_posts() ) : $your_query->the_post();

          		$sfondo=types_render_field("immagine-di-sfondo", array('output'=>'raw'));
				?>

				<?php if($contatore>0){
					?><div class="separate-editions"><span></span></div><?php
				} ?>
				
				
				<div id="link<?php 
						$nome_cat=get_the_title();
						$nome_cat=create_slug($nome_cat);
						echo $nome_cat; 
					?>" class="row expandible pagina-storie" style="background-image:url('<?php echo $sfondo;?>'); position:relative;">
					<!--<a name="link<?php 
						$nome_cat=get_the_title();
						$nome_cat=create_slug($nome_cat);
						echo $nome_cat; 
					?>">&nbsp;</a>-->

					<?php the_content(); ?>
				</div>
				

				<?php
				$contatore=1;
			endwhile;
			// reset post data (important!)
			wp_reset_postdata();
			?>


			<?php } // FINE TEST ?>



		<?php endwhile; ?>
	<?php endif; ?>
<?php get_footer(); ?>
<script>
$(window).load(function() {

	setTimeout(function() {
	        $ancora=window.location.hash;
	        console.log($ancora);
	        $offset=$($ancora).offset().top-60;
	        $('html, body').animate({
	                scrollTop: $offset
	            }, 1000);
	   }, 1000);
});
</script>