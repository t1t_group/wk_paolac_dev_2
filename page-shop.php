<?php
/**
 * 
 * Template Name: Shop
 */
get_header();
?>
<div class="wrap">
	<?php if (have_posts()) : ?>
		<?php while (have_posts()) : the_post(); ?>
			<div class="page-shop">
				<div class="row">
					<div class="filter-panel">
						<?php get_template_part('block_filtri_shop'); ?>
					</div>

					<div class="filter-button wk_pulsante_container align-center onlymobile">
					    <button type="button" disabled><?php echo __("Show results","webkolm");?> ></button>
					</div>

					<div class="grid-shop">
						<style skip_me="1" wp_skip_me="1">

/* MOBILE */
body .grid-shop .grid-item a { 
	width: 100%;
	padding-top: 100% !important;
	position: relative;

}

body .grid-shop .grid-item a .mask_outline {
	position: absolute;
	top: 0;
}
/* TABLET */
@media all and (min-width: 48em) {
body .grid-shop .grid-item a { 
		width: 100%;
		padding-top: 100% !important;
		position: relative;

	}

	body .grid-shop .grid-item a .mask_outline {
		position: absolute;
		top: 0;
	}

}
/* DESKTOP */
@media all and (min-width: 62.5em) {
	body .grid-shop .grid-item a { 
		width: 100%;
		padding-top: 100% !important;
		position: relative;

	}

	body .grid-shop .grid-item a .mask_outline {
		position: absolute;
		top: 0;
	}
}
/* WIDE */
@media all and (min-width: 78.75em) {
	body .grid-shop .grid-item a { 
		width: 100%;
		padding-top: 100% !important;
		position: relative;

	}

	body .grid-shop .grid-item a .mask_outline {
		position: absolute;
		top: 0;
	}
}

/*
body .grid-shop {
	flex-shrink: 0;

	@include mq($from: tablet){
		position: relative;
		width: 73%;
		left: 27%;
	    top: 0;
	}

	@include mq($from: desktop){
		width: 75%;
		left: 25%;
	}

	@include mq($from: wide){
		width: 80%;
	    left: 20%;
	}

	.grid-sizer, .grid-item {
		width: 49%;
		flex-shrink: 0;
		flex-grow: 0;

		@include mq($from: tablet){
			width: 33%;
		}

		@include mq($from: wide){
			width: 24%;
		}

	}

	.grid-sizer {
		overflow: hidden;
		height: 0;
		margin: 0;
		padding: 0;
	}

	/*
	li.grid-item {
		min-height: 70vw;

		@include mq($from: tablet){
	    	min-height: 28vw;
	    }

	    @include mq($from: desktop){
	    	min-height: 32vw;
	    }

	    @include mq($from: wide){
	    	min-height: 27vw;
	    }
	}

} */</style>
						<?php

							$your_query = new WP_Query(
							    array(
							      'post_type' => 'product',
							      'posts_per_page' => -1,
							      'orderby' => 'post_title',
							      'order'=>'ASC',
							      //'taxonomy' => 'collezione',
							  )
							);

							woocommerce_product_loop_start();

							while ( $your_query->have_posts() ) : $your_query->the_post();
								
								global $product, $woocommerce_loop;

								$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail' );
								$url = $thumb['0'];
								$short_desc=types_render_field("sottotitolo", array("raw"=>"true"));
								$item_classes = get_taxonomies_terms_classes($post->ID);
								$product_title = get_the_title();
								$new_label = '';
								$data_new = 1;

								if ( has_term( 'novita', 'utilizzo' ) || has_term( 'novita', 'utilizzo' ) ) {
									$new_label = '<span class="new_flag">NEW</span>';
									$data_new = 0;
								}

								if ( has_term( 'exclusive-deal', 'utilizzo' ) ) {
									$new_label .= '<span class="offerta-esclusiva_flag">' . __('Exclusive deal', 'webkolm') . '</span>';
								}


								// 
								//	PRODUCT VARIATION
								//

								$args = array(
								    'post_type'     => 'product_variation',
								    'post_status'   => array( 'private', 'publish' ),
								    'numberposts'   => -1,
								    'orderby'       => 'menu_order',
								    'order'         => 'asc',
								    'post_parent'   => get_the_ID() // get parent post-ID
								);
								$variations = get_posts( $args );

								$parent_class = " wk_single wk_variation ". $item_classes . " ";
								if(!empty($variations)){
									$parent_class= " wk_parent ";
								}

								?>
								
								<li class="grid-item wk_product_item <?php echo $parent_class;?>" data-new="<?php echo $data_new; ?>">
									<a href="<?php echo get_the_permalink();?>">
										<div class="mask_outline">
											<img src="<?= $url; ?>" title="<?php echo $product_title; ?> - <?php echo $short_desc; ?>" alt="<?php echo $product_title; ?> - <?php echo $short_desc; ?>"/>
											<?php echo $new_label; ?>
										</div>
										<h4 class="prodotto_titolo"><?php echo $product_title; ?>
											
										<?php 
										/*
											if ( $product->is_type( 'variable' ) ) {
												echo '<span class="more_variations nomobile">'.__("+ Variations", "paolac").'</span>';
											}
										*/
										?>
										</h4><span class="prodotto_sottotitolo"><?php echo $short_desc; ?></span><span class="prodotto_prezzo"><?php echo $product->get_price_html(); ?></span>
									</a>
								</li>
							<?php

							foreach ( $variations as $variation ) {

							    // get variation ID
							    $variation_ID = $variation->ID;

							    // get variations meta
							    $product_variation = new WC_Product_Variation( $variation_ID );

							    // get variations long title
							 	$variation_title = getVariationTitle($product_title, $product_variation);

							    // get variation featured image
							    $variation_image = $product_variation->get_image();

							    // get variation price
							    $variation_price = $product_variation->get_price_html();

							    // get variation classes
							    //$item_classes = get_taxonomies_terms_classes($variation_ID);

							    get_post_meta( $variation_ID , '_text_field_date_expire', true ); 
							    ?>

							    <li class="grid-item wk_product_item wk_variation wk_hidden <?php echo $item_classes; ?>" data-new="<?php echo $data_new; ?>">
									<a href="<?php echo get_the_permalink($variation_ID);?>">
										<div class="mask_outline">
											<?= $variation_image; ?>
											<?php echo $new_label; ?>
										</div>
										<h4 class="prodotto_titolo"><?php echo $product_title; ?></h4>
										<span class="prodotto_sottotitolo"><?php echo $short_desc; ?> <?php echo strtolower(str_replace($product_title,"",$variation_title)); ?></span>
										<span class="prodotto_prezzo"><?php echo $variation_price; ?></span>
									</a>
								</li>

							<?php }

							// 
							//	END PRODUCT VARIATION
							//

							endwhile;
							// reset post data (important!)
							wp_reset_postdata();

							woocommerce_product_loop_end();

							?>
					</div>
					
				</div>
			</div>
		<?php endwhile; ?>
	<?php endif; ?>

</div>

<?php get_footer(); 


function getVariationTitle($parent_title, $variation){
	$result = $parent_title;

	foreach ($variation->attributes as $value) {
		$result .= ' ' . $value;
	}

	return $result;
}

?>