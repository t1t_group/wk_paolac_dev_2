<?php
/**
 * Template Name: Homepage
 *
 */
 
get_header(); 
?>
  <?php if (have_posts()) : ?>
    <?php while (have_posts()) : the_post(); ?>
      <?php 

      if("yes"=="yes"){

        echo the_content();


      }else{

      // SLIDER PROGETTI POST SALONE
     // if($_GET["test"]=="yes"){

        ?>
        <div class="slider-prodotti-salone">
          <ul class="slides">
            <!--<li><div class="col-12 testshort"><img src="http://www.paolac.com/wp-content/themes/paolac/img/salone2017_cover.jpg"></div>
              <div class="col-6 testshort"><h1>Other Signs</h1></div><div class="col-6 testshort">
              <?php if(ICL_LANGUAGE_CODE=='it'){ ?>
                Nel rinnovato showroom di via Solferino 11 progettato da Elisa Ossino, PAOLA C. presenta la nuova storia, Dolce Vita di Cristina Celestino, Lunar Landscape di Elisa Ossino, Monkos e Pipoz di Jaime Hayon e Riflessi di Böjte—Bottari. Direzione artistica di Aldo Cibic
              <?php }else{ ?>
                Riflessi is a collection of vases made of blown borosilicate glass that recalls the botanical structure of flowers. The solid shape of the cylinder unfastens as an archway and enhances like a corolla the floral elements with it’s feeble glass reflections.
              <?php } ?></div><div class="col-2 testshort"></div>
            </li>-->
            <li><div class="col-12 testshort"><img src="http://www.paolac.com/wp-content/themes/paolac/img/salone2017_dolcevita_new.jpg"></div>
              <div class="col-6 testshort"><h1>Dolce Vita<br/>Cristina Celestino</h1></div><div class="col-6 testshort">
              <?php if(ICL_LANGUAGE_CODE=='it'){ ?>
                    Dolce vita è una collezione che celebra in chiave contemporanea il fascino dell’Italia degli anni '50, un'epoca tesa verso leggerezza e joi de vivre, ironia e ottimismo, dove assaporare la bellezza dei degli o ggetti e dei piccoli riti a questi legati.
              <?php }else{ ?>
                    Dolce Vita is a collection that celebrates in a contemporary tone the charme of Italy in the Fifties - an era addressed to levity and joie de vivre, irony and optimism - in which you could taste the beauty of the objects and the practices related to them.
              <?php } ?></div><div class="col-2 testshort"></div>
            </li>
            <li><div class="col-12 testshort"><img src="http://www.paolac.com/wp-content/themes/paolac/img/salone2017_lunarlandscape.jpg"></div>
              <div class="col-6 testshort"><h1>Lunar Landscape<br/>Elisa Ossino</h1></div><div class="col-6 testshort">
              <?php if(ICL_LANGUAGE_CODE=='it'){ ?>
                Elisa Ossino disegna per Paola C. una serie di accessori per la tavola, rileggendo in chiave modernista e Bauhaus temi classici della mise en place e suggestioni figurative ispirate alla pittura di natura morta.
              <?php }else{ ?>
                Elisa Ossino designes for Paola C. a tableware collection, revisiting the classic themes of mise en place and the figurative influences inspired by still life in a modernist and Bauhaus tone.
              <?php } ?></div><div class="col-2 testshort"></div>
            </li>
            <li><div class="col-12 testshort"><img src="http://www.paolac.com/wp-content/themes/paolac/img/salone2017_riflessi_new.jpg"></div>
              <div class="col-6 testshort"><h1>Riflessi<br/>Bojte-Bottari</h1></div><div class="col-6 testshort">
              <?php if(ICL_LANGUAGE_CODE=='it'){ ?>
                Riflessi è una collezione di vasi in vetro borosilicato soffiato che rievoca la struttura botanica del fiore. La forma solida del cilindro si apre ad arco e come una corolla avvolge ed esalta gli elem enti floreali con le tenui riflessioni del vetro.
              <?php }else{ ?>
                Riflessi is a collection of vases made of blown borosilicate glass that recalls the botanical structure of flowers. The solid shape of the cylinder unfastens as an archway and enhances like a corolla the floral elements with it’s feeble glass reflections.
              <?php } ?></div><div class="col-2 testshort"></div>
            </li>
            <li><div class="col-12 testshort"><img src="http://www.paolac.com/wp-content/themes/paolac/img/salone2017_monkospepe.jpg"></div>
              <div class="col-6 testshort"><h1>Monkos &amp; Pipoz<br/>Jaime Hayon</h1></div><div class="col-6 testshort">
              <?php if(ICL_LANGUAGE_CODE=='it'){ ?>
                Il pezzo è un piccolo oggetto da tavola indimenticabile. Mostra la raffinatezza del materiale e la tecnica di manifattura artigianale in metallo.
              <?php }else{ ?>
                The piece is a small object to be remembered at the table. It shows the sophistication of the material and its crafting technique in metal.
              <?php } ?></div>
            </li>
          </ul>
        </div>
        <br class="nomobile"/>
        <?php

     // }


      ?>
      <?php // the_content();?>

      <!-- PRIMA SEZIONE HIGLIGHT 
      <div class="row">
        <div class="col-12">
          <img src="<?php echo bloginfo( 'stylesheet_directory' ); ?>/img/test_home1.jpg"/>
          <br/><br/>
        </div>
        <div class="col-6">
          <h1>Ondina &amp; Routin</h1>
        </div>
        <div class="col-4">Aldo e Matteo Cibic presentano nuovi prodotti per Paola C. Ondina, eleganti vassoi in acciaio con manici e Routine una famiglia di brocche e bicchieri in vetro soffiato.</div>
        <div class="col-2"></div>
      </div>
      

      <br/><br/><br/><br/>
      <!-- SECONDA SEZIONE HIGLIGHT 

      <div class="row">
        <div class="col-7"><img src="<?php echo bloginfo( 'stylesheet_directory' ); ?>/img/test_home2.jpg"/></div>
        <div class="col-5">
          <br/><br/><br/><br/>
          <h1>Collection</h1>
          <div class="padding20">
              Kit consists of more than a hundred ready-to- use elements that you can combine to get the exact prototype you want.
              <br/>
              <a href="#" class="freccia_sx"><?php _e('See more', 'paolac');?></a>
          </div>
        </div>
      </div>

      -->

      <?php 

      $collections=ot_get_option( 'box_collezione_paolac' , array() );
      foreach( $collections as $collection ) {?>
<br class="nomobile" /><br class="nomobile" />
      <div class="row ">
        <?php 
          if(ICL_LANGUAGE_CODE=='it'){ 
            // SE IN LINGUA ITALIANA
            $titolo_collection=$collection['titolo_italiano'];
            $testo_collection=$collection['testo_in_italiano'];
          }
          else{
            // SE IN LINGUA INGLESE
            $titolo_collection=$collection['titolo_inglese'];
            $testo_collection=$collection['testo_in_inglese'];
          }

          $gallery_collection=$collection['galleria_immagini_del_blocco'];
          // LA TRASFORNO IN ARRAY
          $array_foto = explode(',', $gallery_collection);

          // SELEZIONO UNA FOTO RANDOM
          $rand_keys=array_rand($array_foto,1);
          $foto_collection=$array_foto[$rand_keys];
          $url_big=wp_get_attachment_image_src( $foto_collection, 'large' );

          $shop_page_url = get_permalink( woocommerce_get_page_id( 'shop' ) );

        ?>
        
        <div class="col-1"></div>
        <div class="col-6 module widefromtablet"><img src="<?= $url_big[0];?>"/></div>
        <div class="col-4 module widefromtablet">
          <br class="nomobile" /><br class="nomobile" />
          <h1><?= $titolo_collection; ?></h1>
          <div class="padding20">
              <?= $testo_collection; ?>
              <br/>
              <a href="<?= $shop_page_url; ?>" class="freccia_sx"><?php _e('Buy now', 'paolac');?></a>
          </div>
        </div>
        <div class="col-1"></div>
      </div>
      <br class="nomobile" /><br class="nomobile" />
      <?php
      }
      ?>


      <!-- SLIDER EDIZIONI -->
      

      <div class="row module">
        <div class="col-1 nomobile"></div>
        <div id="slider-editions" class="col-10 module">
          <ul class="slides">
            <?php 
            // ELENCO POST EDIZIONI
            $your_query = new WP_Query( array(
                'post_type' => array( 'paolac-edizione'),       
                'posts_per_page' => -1,
              'post_status' => 'publish',
              'orderby' => 'menu_order',
                    'order'=>'ASC'
               ));

            while ( $your_query->have_posts() ) : $your_query->the_post();

                    $sfondo=types_render_field("immagine-di-sfondo", array('output'=>'raw'));
                    $testohome=types_render_field("testo-breve-home-page", array('output'=>'raw'));
                    $terms = get_the_terms( get_the_ID(), 'edizione' ); 
                      foreach ( $terms as $term ) {
                          $url_cat = get_term_link( $term );
                      }
                    $thumb_m = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );
                    $url_m = $thumb_m['0'];

                    $id = icl_object_id(1358, 'page', true,ICL_LANGUAGE_CODE);
                    $link_edizione  = get_permalink($id);
                    $nome_cat=get_the_title();
                    $nome_cat=create_slug($nome_cat);
                    $link_edizione.='#link'.$nome_cat;
              ?>
              <li>
                 <div class="ed_descrizione col-6" style="background-image:url('<?php echo $sfondo;?>');" >
                    <div class="ed_overlay">
                      <h1><?php _e('Stories', 'paolac'); ?></h1>
                      <div class="padding20">
                          <?php echo apply_filters('the_content', $testohome); ?>
                          <br/>
                          <a href="<?php echo $link_edizione; ?>" class="freccia_dx"><?php _e('Buy now', 'paolac');?></a>
                      </div>
                    </div>
                 </div>
                 <div class="ed_immagine col-6 nomobile" style="background-image:url('<?php echo $url_m; ?>');">
                  <!--<img src="<?php echo $url_m; ?>"/>-->
                 </div>
              </li>

              <?php
            
            endwhile;
            // reset post data (important!)
            wp_reset_postdata();
            ?>
          </ul>
          <!--<div class="contatore-slide">
            <span class="current-slide">1</span>/<span class="total-slides">3</span>
          </div>-->
        </div>
        <div class="col-1 nomobile"></div>
      </div>



      <!-- NUOVI PRODOTTI -->
      <br class="nomobile" /><br class="nomobile" />

      <div class="row">
        <div class="col-12">
          <h2><?php _e("Highlight", "paolac"); ?></h2>
          <div class="row nuovi_prodotti products products_home">
            <?php
             //PRENDO 3 PRODOTTI RANDOM TRA QUELLI SEGNALATI "IN EVIDENZA"

              $your_query = new WP_Query( array(
                 'post_type' => array( 'product' ),                
                'posts_per_page' => 4,
                'post_status' => 'publish',
               
                 'meta_query' => array(
                      array(
                       'key' => 'wpcf-evidenzia-in-homepage',
                       'value' => 1,
                          'compare' => '=',
                     ),

                                     
                 ),
                  'orderby'=>'rand',
                  'order'=>'ASC'
              ));

              $numero_prodotto=1;
              while ( $your_query->have_posts() ) : $your_query->the_post();
                  
                  $custom_field = get_post_custom($post->ID);
                  $url_prod=get_permalink();
                  $sottotitolo_prod=types_render_field("sottotitolo");
                  $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium' );
                  $url = $thumb['0'];

                  ?>
                  <div class="col-4 module <?php if($numero_prodotto=="4"){ echo "onlymobile"; } ?>">
                    <a href="<?= $url_prod; ?>" class="<?php if(has_term('','edizione')){ echo "editions";} ?>">
                      <div class="mask_outline"><img src="<?= $url ?>"/></div>
                      <h4 class="prodotto_titolo"><?php the_title(); ?></h4>
                      <span class="prodotto_sottotitolo"><?= $sottotitolo_prod; ?></span>
                      <span class="prodotto_prezzo"><?php echo $product->get_price_html() ?></span>
                    </a>
                  </div>
                  <?php
                  $numero_prodotto++;
              endwhile;
              // reset post data (important!)
              wp_reset_postdata();
             ?>
             <!--
            <div class="col-4">
              <a href="#">
                <img src="<?php echo bloginfo( 'stylesheet_directory' ); ?>/img/test_prodotto1.jpg"/>
                <h4 class="prodotto_titolo">Product name</h4>
                <span class="prodotto_sottotitolo">Short description</span>
                <span class="prodotto_prezzo">599.00 €</span>
              </a>
            </div>
            <div class="col-4">
              <a href="#">
                <img src="<?php echo bloginfo( 'stylesheet_directory' ); ?>/img/test_prodotto2.jpg"/>
                <h4 class="prodotto_titolo">Product name</h4>
                <span class="prodotto_sottotitolo">Short description</span>
                <span class="prodotto_prezzo">599.00 €</span>
              </a>
            </div>
            <div class="col-4">
              <a href="#">
                <img src="<?php echo bloginfo( 'stylesheet_directory' ); ?>/img/test_prodotto3.jpg"/>
                <h4 class="prodotto_titolo">Product name</h4>
                <span class="prodotto_sottotitolo">Short description</span>
                <span class="prodotto_prezzo">599.00 €</span>
              </a>
            </div>
          -->
          </div>
        </div>
      </div>

       <?php } ?>

    <?php endwhile; ?>
  <?php endif; ?>

<div class="contenitore-pagina-bottom"-->
  <div class="wrap">
    
      <!-- BOX HOMEPAGE -->
      <br class="nomobile" /><br class="nomobile" />

      <div class="row">
         <!--
        <div class="col-4">
          <div class="box-home" style="background-image:url('<?php echo bloginfo( 'stylesheet_directory' ); ?>/img/test_sfondobox1.jpg');">
            <div class="box_overlay">
              <h3>About</h3>
              Kit consists of more than a hundred ready-to- use
              <a href="" class="freccia">link</a>
            </div>
          </div>
        </div>
        <div class="col-4">
          <div class="box-home" style="background-image:url('<?php echo bloginfo( 'stylesheet_directory' ); ?>/img/test_sfondobox2.jpg');">
            <div class="box_overlay">
              <h3>Event</h3>
              <b>New Roman</b><br/>
              Fuori Salone, Milano<br/>
              08-13 Aprile 2016
              <a href="" class="freccia">link</a>
            </div>
          </div>
        </div>
       
        <div class="col-4">
          <div class="box-home">
            <div class="no_overlay">
              <h3>Newsletter</h3>
              Subscribe to our newsletter
            </div>
          </div>
        </div>
        -->
        <?php 
        // OUTPUT DEI BOX

          
          /* get the slider array */
          $slides = ot_get_option( 'box_homepage', array() );
          
          if ( ! empty( $slides ) ) {
            $c = 0;
            foreach( $slides as $slide ) {
              if($slide['immagine_di_sfondo_del_box']!=""){
                $sfondo=$slide['immagine_di_sfondo_del_box'];
                $sfondo=wp_get_attachment_image_src( $sfondo, 'medium' );
                $sfondobox='style="background-image:url(\''.$sfondo[0].'\');"';
                $sfondoclasse='box_overlay';
                $link_box=$slide['link_url_ita'];
                $boxcorto="";
                if($c == 2){
                    $boxcorto="boxcorto";
                }

              } else {
                $sfondobox='';
                $sfondoclasse='no_overlay';
                $link_box=$slide['link_url_eng'];
                $boxcorto="boxcorto";
              }



              // Get the value saved on Theme Options Page
              // Return a comma separated list of image attachment IDs
              $spyr_demo_gallery = ot_get_option( 'immagine_di_sfondo_del_box' );

              // Get the list of IDs formatted into an array 
              // and ready to use for looping through them
              $gallery_img_ids = wp_parse_id_list( $spyr_demo_gallery );
              ?>
              <div class="col-4 module">
                <div class="box-home <?php echo $boxcorto; ?>" <?php echo $sfondobox; ?>>
                  <div class="<?php echo $sfondoclasse; ?>">
                    <?php 
                      if(ICL_LANGUAGE_CODE=='it'){
                    ?>
                    <h3 class="nolinea"><?= $slide['titolo_italiano'] ?></h3>
                    <?php echo nl2br($slide['testo_italiano']); ?>
                    <?php
                      }else{
                    ?>
                    <h3 class="nolinea"><?= $slide['titolo_inglese'] ?></h3>
                    <?php echo nl2br($slide['testo_inglese']); ?>
                    <?php
                      }

                      if($c==2){ ?>
                            <br/>
                            <br/>
                            <form action="https://a3b1i0.emailsp.com/Frontend/subscribe.aspx">
                              <input type="hidden" id="apgroup" name="apgroup" value="87">
                              <input type="hidden" name="list" value="6">
                              <input type="hidden" id="lingua" name="campo32" value="<?php echo get_language_nl_code($lang); ?>">
                              <input type="hidden" id="paese" name="campo8" value="<?php echo get_request_country(); ?>">
                              <input type="hidden" name="confirm" value="off" />

                              <div class="field-group tipologia">
                                <select id="pc_tipo" name="campo39">
                                  <option value=""><?php echo __("Select a typology", "webkolm"); ?></option>
                                  <option value="cliente privato"><?php echo __("Private customer", "webkolm"); ?></option>
                                  <option value="distributore rivenditore"><?php echo __("Distributor reseller", "webkolm"); ?></option>
                                  <option value="press"><?php echo __("Press", "webkolm"); ?></option>
                                </select>
                              </div>
                              
                              <div class="field-group email-group">
                                  <input type="text" id="email" name="EMAIL" value="" placeholder="<?php echo __('enter your e-mail','webkolm'); ?>" required="required">
                                <input name="Submit" type="submit" class="button" value="<?php echo __("Subscribe","webkolm"); ?>">
                              </div>
                            </form>

                      <?php }
                      
                      if(ICL_LANGUAGE_CODE=='it'){
                         $link_box=$slide['link_url_ita'];
                      }else{
                         $link_box=$slide['link_url_eng'];
                      }
                    
                      if($link_box!=""){
                        echo '<a href="'.$link_box.'" class="freccia">link</a>';
                      }
                    ?>

                  </div>
                </div>
              </div>
              <?php
              $c++;
            }

          }
         

        ?>


      </div>
  </div>
  
</div>

<?php get_footer(); ?>
