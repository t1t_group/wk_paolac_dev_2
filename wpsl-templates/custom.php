<?php 
global $wpsl_settings, $wpsl;

$output         = $this->get_custom_css(); 
$autoload_class = ( !$wpsl_settings['autoload'] ) ? 'class="wpsl-not-loaded"' : '';

$output .= '<div class="row"><div class="col-12"><h2 class="linea">'.get_the_title().'</h2></div></div>';
$output .= '<div class="row"><div class="col-1"></div><div class="col-10"><div id="wpsl-wrap" class="wpsl-store-below">';
    
$output .= '<div id="wpsl-gmap" class="wpsl-gmap-canvas"></div>';
$output .=  '<div class="wpsl-search wpsl-clearfix ' . $this->get_css_classes() . '">';
$output .= '<div id="wpsl-search-wrap">';
/*
$output .= '<div id="wpsl-search-wrap"><div style="padding-left:20px; font-size:0.8em;">';
$output .= esc_html(  __( 'Enter the name of your location and the extent of the search range to find out all the nearest dealers.', 'paolac' )  ) ;
$output .= '</div><br/><br/>';
*/
$output .= '<form autocomplete="off">';
$output .= '<div class="wpsl-input">';
$output .= '<!--<div><label for="wpsl-search-input">' . esc_html( $wpsl->i18n->get_translation( 'search_label', __( 'Your location', 'wpsl' ) ) ) . '</label></div>-->';
$output .= '<input id="wpsl-search-input" type="text" value="' . apply_filters( 'wpsl_search_input', '' ) . '" name="wpsl-search-input" placeholder="' . esc_html( $wpsl->i18n->get_translation( 'search_label', __( 'Your location', 'wpsl' ) ) ) . '" aria-required="true" />';
$output .= '</div>';

if ( $wpsl_settings['radius_dropdown'] || $wpsl_settings['results_dropdown']  ) {
    
    $output .= '<div class="wpsl-select-wrap">';

    if ( $wpsl_settings['radius_dropdown'] ) {
        $output .= '<div id="wpsl-radius">';
        $output .= '<label for="wpsl-radius-dropdown">' . esc_html( $wpsl->i18n->get_translation( 'radius_label', __( 'Search radius', 'wpsl' ) ) ) . '</label>';
        $output .= '<select id="wpsl-radius-dropdown" class="wpsl-dropdown" name="wpsl-radius">';
        $output .= $this->get_dropdown_list( 'search_radius' );
        $output .= '</select>';
        $output .= '</div>';
    }

    if ( $wpsl_settings['results_dropdown'] ) {
        $output .= '<div id="wpsl-results">';
        $output .= '<label for="wpsl-results-dropdown">' . esc_html( $wpsl->i18n->get_translation( 'results_label', __( 'Results', 'wpsl' ) ) ) . '</label>';
        $output .= '<select id="wpsl-results-dropdown" class="wpsl-dropdown" name="wpsl-results">';
        $output .= $this->get_dropdown_list( 'max_results' );
        $output .= '</select>';
        $output .= '</div>';
    } 

    $output .= '</div>';
}

if ( $wpsl_settings['category_filter'] ) {
    $output .= $this->create_category_filter();
}

$output .= '<div class="wpsl-search-btn-wrap"><input id="wpsl-search-btn" type="submit" value="' . esc_attr( $wpsl->i18n->get_translation( 'search_btn_label', __( 'Search', 'wpsl' ) ) ) . '"></div>';

$output .= '</form>';
$output .= '</div>';
$output .= '</div>';
$output .= '<div id="wpsl-result-list">';
$output .= '<div id="wpsl-stores" '. $autoload_class .'>';
$output .= '<ul></ul>';
$output .= '</div>';
$output .= '</div>';

$output .= '</div></div></div>';

return $output;