<?php
global $woocommerce;
global $product;

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'menuwide'   => __( 'Menu principale', 'paolac' ),
		'menumobile' => __( 'Mobile Menu', 'paolac' ),
	) );

/* featured image */
add_theme_support( 'post-thumbnails' );


/* CUSTOM EXCERPT  */
function excerpt($limit) {
    return wp_trim_words(get_the_excerpt(), $limit);
}

 
function limit_text($text, $limit) {
      if (str_word_count($text, 0) > $limit) {
          $words = str_word_count($text, 2);
          $pos = array_keys($words);
          $text = substr($text, 0, $pos[$limit]) . '...';
      }
      return $text;
    }

/* GESTIONE GALLERY CUSTOM */
	

add_shortcode('gallery', 'my_gallery_shortcode');    

function my_gallery_shortcode($attr) {
    $post = get_post();

static $instance = 0;
$instance++;
$random_number=rand(0,100000);

if ( ! empty( $attr['ids'] ) ) {
    // 'ids' is explicitly ordered, unless you specify otherwise.
    if ( empty( $attr['orderby'] ) )
        $attr['orderby'] = 'post__in';
    $attr['include'] = $attr['ids'];
}

// Allow plugins/themes to override the default gallery template.
$output = apply_filters('post_gallery', '', $attr);
if ( $output != '' )
    return $output;

// We're trusting author input, so let's at least make sure it looks like a valid orderby statement
if ( isset( $attr['orderby'] ) ) {
    $attr['orderby'] = sanitize_sql_orderby( $attr['orderby'] );
    if ( !$attr['orderby'] )
        unset( $attr['orderby'] );
}

extract(shortcode_atts(array(
    'order'      => 'ASC',
    'orderby'    => 'menu_order ID',
    'id'         => $post->ID,
    'itemtag'    => 'dl',
    'icontag'    => 'dt',
    'captiontag' => 'dd',
    'columns'    => 3,
    'size'       => 'thumbnail',
    'include'    => '',
    'exclude'    => ''
), $attr));

$id = intval($id);
if ( 'RAND' == $order )
    $orderby = 'none';

if ( !empty($include) ) {
    $_attachments = get_posts( array('include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );

    $attachments = array();
    foreach ( $_attachments as $key => $val ) {
        $attachments[$val->ID] = $_attachments[$key];
    }
} elseif ( !empty($exclude) ) {
    $attachments = get_children( array('post_parent' => $id, 'exclude' => $exclude, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );
} else {
    $attachments = get_children( array('post_parent' => $id, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );
}

if ( empty($attachments) )
    return '';

if ( is_feed() ) {
    $output = "\n";
    foreach ( $attachments as $att_id => $attachment )
        $output .= wp_get_attachment_link($att_id, $size, true) . "\n";
    return $output;
}

$itemtag = tag_escape($itemtag);
$captiontag = tag_escape($captiontag);
$icontag = tag_escape($icontag);
$valid_tags = wp_kses_allowed_html( 'post' );
if ( ! isset( $valid_tags[ $itemtag ] ) )
    $itemtag = 'dl';
if ( ! isset( $valid_tags[ $captiontag ] ) )
    $captiontag = 'dd';
if ( ! isset( $valid_tags[ $icontag ] ) )
    $icontag = 'dt';

$columns = intval($columns);
$itemwidth = $columns > 0 ? floor(100/$columns) : 100;
$float = is_rtl() ? 'right' : 'left';

$selector = "gallery-{$instance}";

$gallery_style = $gallery_div = '';
	if ( apply_filters( 'use_default_gallery_style', true ) )
		$gallery_style = "";
	$size_class = sanitize_html_class( $size );
	
	$gallery_div = "<div class=\"slider-interno\"><ul class=\"slides\">";
	
	$output = apply_filters( 'gallery_style', $gallery_style . "\n\t\t" . $gallery_div );
	
$i = 0;
foreach ( $attachments as $id => $attachment ) {
    $link = isset($attr['link']) && 'file' == $attr['link'] ? wp_get_attachment_link($id, $size, false, false) : wp_get_attachment_link($id, $size, true, false);
	$immagine=wp_get_attachment_image_src( $id, 'large' );
	$immagine_small=wp_get_attachment_image_src( $id, 'medium' );
	$alt = get_post_meta($attachment->ID, '_wp_attachment_image_alt', true);
	$image_title = $attachment->post_title;
	$caption = $attachment->post_excerpt;
	$description = $image->post_content;
	$output .= "\n\t<li><img src=\"".$immagine_small[0]."\"></li>";
}

$output .= "
		</ul></div>";

return $output;
}






// PAGINAZIONE




function twentythirteen_paging_nav() {
    global $wp_query;

    // Don't print empty markup if there's only one page.
    if ( $wp_query->max_num_pages < 2 )
        return;
    ?>
    <nav class="woocommerce-pagination"><?php
    $big = 999999999; // need an unlikely integer
    echo paginate_links( array(
        'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
        'format' => '/page/%#%',
        'prev_text' => '<img src="http://www.paolac.com/wp-content/themes/paolac/img/freccia_sx_nera.png" class="arrow" />',
        'next_text' => '<img src="http://www.paolac.com/wp-content/themes/paolac/img/freccia_dx_nera.png" class="arrow" />',
        'current' => max( 1, get_query_var('paged') ),
        'total' => $wp_query->max_num_pages,
        'type' => 'list'
    ) );
    
?></nav>
    <?php
}






/* SHORTCODES */

function col_init($atts, $content = null) {
  extract(shortcode_atts(array(
    "tipo"=>'',
    "num" => '1'
  ), $atts));
  return '<div class="col-'.$num.' testshort">'.do_shortcode($content).'</div>';
}

add_shortcode('col', 'col_init');


function row_init($atts, $content = null) {
  return '<div class="row">'.do_shortcode($content).'</div>';
}

add_shortcode('row', 'row_init');

function spaziosx_init($atts, $content = null) {
  return '<div class="padding20">'.do_shortcode($content).'</div>';
}

add_shortcode('spaziosx', 'spaziosx_init');


function product_button_init($atts, $content = null) {
  return '<a class="freccia_dx mostra_prodotti"><span class="first">'.__("View Products", "paolac").'</span><span class="second">'.__("Hide Products", "paolac").'</span></a>';
}

add_shortcode('product_button', 'product_button_init');


function product_list_init($atts, $content = null) {
    
    $output= '<div class="row expand_row">
        <span class="close_row"></span><span class="close_row close_basso"></span>';
        
        // RICAVO CATEGORIA

        $terms = get_the_terms( get_the_ID(), 'edizione' ); 
        foreach ( $terms as $term ) {
            $termid = $term->term_id;
        }

        $prod_query = new WP_Query( array(
            'post_type' => array( 'product'),               
            'posts_per_page' => -1,
            'post_status' => 'publish',
            'orderby' => 'title',
            'order'=>'ASC',
            'tax_query' => array(array(
                'taxonomy' => 'edizione',
                'field' => 'term_id',
                'terms' => $termid, // Don't display products in the knives category on the shop page
                'operator' => 'IN'
            ))
         ));
        $output.=woocommerce_product_loop_start(0); 
        $output.='<div class="grid-sizer">grid sizer</div>';
        while ( $prod_query->have_posts() ) : $prod_query->the_post();
            $output.=load_template_part();
        endwhile;
        $output.=woocommerce_product_loop_end(0); 
        // reset post data (important!)
        wp_reset_postdata();
    

    $output.="</div>";
    return $output;
}

add_shortcode('product_list', 'product_list_init');



function load_template_part() {
    ob_start();
    wc_get_template_part( 'content', 'product' ); 
    $var = ob_get_contents();
    ob_end_clean();
    return $var;
}


add_filter('show_admin_bar', '__return_false');





/* DATI DELL'IMMAGINE PASSANDO l'ID */

function wp_get_attachment( $attachment_id ) {

    $attachment = get_post( $attachment_id );
    return array(
        'alt' => get_post_meta( $attachment->ID, '_wp_attachment_image_alt', true ),
        'caption' => $attachment->post_excerpt,
        'description' => $attachment->post_content,
        'href' => get_permalink( $attachment->ID ),
        'src' => $attachment->guid,
        'title' => $attachment->post_title
    );
}



// WOOCOMMERCE 

add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}


//CUSTOM LANGUAGE SWITCHER



function custom_language_selector(){
        $languages = icl_get_languages('skip_missing=0&orderby=code');
        if(!empty($languages)){
            foreach($languages as $l){
                if(!$l['active'])
                    {
                        echo '<a class="selettore_lingua" href="'.$l['url'].'" >';
                        echo $l['language_code'];
                        echo '</a>';
                    }
            }
        }
    }
    

function custom_language_selector_mobile(){
        $languages = icl_get_languages('skip_missing=0&orderby=code');
        if(!empty($languages)){
            echo '<ul id="lingue_mobile" style="margin-top:0 !important;">';
            foreach($languages as $l){
                echo '<li class="nochild">';
                $active = "";
                if($l['active']){
                    $active = " active ";
                }

                echo '<a href="'.$l['url'].'" class="selettore_lingua '.$active.'">';
                echo $l['language_code'];
                echo '</a>';
                
                echo '</li>';
            }
            echo '</ul>';
        }
    }


/* GESTIONE SVG */

function svg_mime_types( $mimes ) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;}
add_filter( 'upload_mimes', 'svg_mime_types' );


function requireToVar($file){
    ob_start();
    include(ABSPATH . $file);
    return ob_get_clean();

}

function svg_line($url) {
     if ( strpos( $url[1], '.svg' ) !== false ) {
         $url = str_replace( site_url(), '', $url[1]);
         //echo $url;
         $test=requireToVar(substr($url, 1));
     }
     else {
        $test='<img src="'.$url[1].'">';
     }
     return $test;

}

add_filter('the_content', 'svg_inliner');
function svg_inliner($content) {

       global $post;
       
       $pattern ='#<img.+?src="([^"]*)".*?/?>#i';
       $content = preg_replace_callback($pattern, "svg_line", $content);
       
       return $content;
       
}


/* RIMUOVO P PRIMA DEI SHORTCODE */

/**
 * Don't auto-p wrap shortcodes that stand alone
 *
 * Ensures that shortcodes are not wrapped in <<p>>...<</p>>.
 *
 * @since 2.9.0
 *
 * @param string $content The content.
 * @return string The filtered content.
 */
function shortcode_unautop_bis( $content ) {
    global $shortcode_tags;

    if ( empty( $shortcode_tags ) || !is_array( $shortcode_tags ) ) {
        return $content;
    }

    $tagregexp = join( '|', array_map( 'preg_quote', array_keys( $shortcode_tags ) ) );

    $pattern =
          '/'
        . '<p>'                              // Opening paragraph
        . '\\s*+'                            // Optional leading whitespace
        . '('                                // 1: The shortcode
        .     '\\['                          // Opening bracket
        .     "($tagregexp)"                 // 2: Shortcode name
        .     '(?![\\w-])'                   // Not followed by word character or hyphen
                                             // Unroll the loop: Inside the opening shortcode tag
        .     '[^\\]\\/]*'                   // Not a closing bracket or forward slash
        .     '(?:'
        .         '\\/(?!\\])'               // A forward slash not followed by a closing bracket
        .         '[^\\]\\/]*'               // Not a closing bracket or forward slash
        .     ')*?'
        .     '(?:'
        .         '\\/\\]'                   // Self closing tag and closing bracket
        .     '|'
        .         '\\]'                      // Closing bracket
        .         '(?:'                      // Unroll the loop: Optionally, anything between the opening and closing shortcode tags
        .             '[^\\[]*+'             // Not an opening bracket
        .             '(?:'
        .                 '\\[(?!\\/\\2\\])' // An opening bracket not followed by the closing shortcode tag
        .                 '[^\\[]*+'         // Not an opening bracket
        .             ')*+'
        .             '\\[\\/\\2\\]'         // Closing shortcode tag
        .         ')?'
        .     ')'
        . ')'
        . '\\s*+'                            // optional trailing whitespace
        . '<\\/p>'                           // closing paragraph
        . '/s';

    return preg_replace( $pattern, '$1', $content );
}
remove_filter( 'the_content', 'wpautop' );
add_filter( 'the_content', 'wpautop' , 99);
add_filter( 'the_content', 'shortcode_unautop_bis',100 );


/**
 * Extends WP_Query with a posts_join filter allowing you to query by taxonomy instead of tax_query using terms
 * Usage: <code> $query = new Query_By_Taxonomy( array( 'posts_per_page' => $foo, 'orderby' => $bar ) ); </code>
 *
 * @class Query_By_Taxonomy
 */
class Query_By_Taxonomy extends WP_Query {

    var $posts_by_taxonomy;
    var $taxonomy;

    function __construct( $args = array() ) {
        add_filter( 'posts_join', array( $this, 'posts_join' ), 10, 2 );
        $this->posts_by_taxonomy = true;
        $this->taxonomy = $args['taxonomy'];

        unset( $args['taxonomy'] );

        parent::query($args);
    }

    function posts_join( $join, $query ) {
        if ( isset( $query->posts_by_taxonomy ) && false !== $query->posts_by_taxonomy ) {
            global $wpdb;
            $join .= $wpdb->prepare(
                 "INNER JOIN {$wpdb->term_relationships} ON {$wpdb->term_relationships}.object_id={$wpdb->posts}.ID
                  INNER JOIN {$wpdb->term_taxonomy} ON {$wpdb->term_taxonomy}.term_taxonomy_id={$wpdb->term_relationships}.term_taxonomy_id
                  AND {$wpdb->term_taxonomy}.taxonomy=%s",
                $this->taxonomy );
        }
        return $join;
    }
}



/* TASTINI QTY BUTTON */

add_action( 'wp_enqueue_scripts', 'wcqi_enqueue_polyfill' );
function wcqi_enqueue_polyfill() {
    wp_enqueue_script( 'wcqi-number-polyfill' );
}



/* WP STORE LOCATOR */

add_filter( 'wpsl_templates', 'custom_templates' );

function custom_templates( $templates ) {

    /**
     * The 'id' is for internal use and must be unique ( since 2.0 ).
     * The 'name' is used in the template dropdown on the settings page.
     * The 'path' points to the location of the custom template,
     * in this case the folder of your active theme.
     */
    $templates[] = array (
        'id'   => 'custom',
        'name' => 'Custom template',
        'path' => get_stylesheet_directory() . '/' . 'wpsl-templates/custom.php',
    );

    return $templates;
}

/* P2P */

function my_connection_types() {
    p2p_register_connection_type( array(
        'name' => 'designer_to_prodotti',
        'from' => 'designers',
        'to' => 'product'
    ) );

    p2p_register_connection_type( array(
        'name' => 'hospitality_to_prodotti',
        'from' => 'hospitalities',
        'to' => 'product'
    ) );
}
add_action( 'p2p_init', 'my_connection_types' );




// TRADUTTORE DI STRINGHE CON LO SLASH */

function traducistringa( $stringa, $lang_code ){

    // CONTROLLO SE C'E UN CARATTERE

    if (strpos($stringa, '/') == false) {
        return $stringa;
    }
    else{

        if($lang_code=="it"){
            $stringa_finale=substr($stringa, 0, strpos($stringa, ' /'));
            return $stringa_finale;
        }
        else { 
            $stringa_finale=strstr($stringa, ' /');
            $stringa_finale=substr($stringa_finale, 2); 
            return $stringa_finale;
        }
        
    }
}

// POST CATEGORI NUMERO

function number_of_posts_on_archive($query){
    if ($query->is_archive && $query->is_main_query()) {
            $query->set('posts_per_page', 24);
   }
   
    return $query;
}

add_filter('pre_get_posts', 'number_of_posts_on_archive');



/**
 * Only display minimum price for WooCommerce variable products
 **/
add_filter('woocommerce_variable_price_html', 'custom_variation_price', 10, 2);

function custom_variation_price( $price, $product ) {

     $price = __("From  ", "paolac");
    //$price ="";

     $price .= woocommerce_price($product->get_price());

     return $price;
}


// NUMERO DI PRODOTTI NELLO SHOP


//add_filter( 'loop_shop_per_page', create_function( '$cols', 'return 24;' ), 20 );


/* AGGIUNGI CLASSE PULSANTI */

// Add Formats Dropdown Menu To MCE
if ( ! function_exists( 'wpex_style_select' ) ) {
    function wpex_style_select( $buttons ) {
        array_push( $buttons, 'styleselect' );
        return $buttons;
    }
}
add_filter( 'mce_buttons', 'wpex_style_select' );


// Add new styles to the TinyMCE "formats" menu dropdown
if ( ! function_exists( 'wpex_styles_dropdown' ) ) {
    function wpex_styles_dropdown( $settings ) {

        // Create array of new styles
        $new_styles = array(
            array(
                'title' => __( 'Stile personalizzato', 'wpex' ),
                'items' => array(
                    array(
                        'title'     => __('Pulsante FRECCIA','wpex'),
                        'selector'  => 'a',
                        'classes'   => 'freccia'
                    ),
                    array(
                        'title'     => __('Pulsante FRECCIA SX','wpex'),
                        'selector'  => 'a',
                        'classes'   => 'freccia_sx'
                    ),
                    array(
                        'title'     => __('Pulsante FRECCIA DX','wpex'),
                        'selector'  => 'a',
                        'classes'   => 'freccia_dx'
                    ),
                ),
            ),

        );

        // Merge old & new styles
        $settings['style_formats_merge'] = true;

        // Add new styles
        $settings['style_formats'] = json_encode( $new_styles );

        // Return New Settings
        return $settings;

    }
}
add_filter( 'tiny_mce_before_init', 'wpex_styles_dropdown' );



// NASCONDERE PRODOTTI EDITION DA SHOP

add_action( 'pre_get_posts', 'custom_pre_get_posts_query' );

function custom_pre_get_posts_query( $q ) {

    // get all terms in the taxonomy
    $terms = get_terms( 'edizione' ); 
    // convert array of term objects to array of term IDs
    //$term_ids = wp_list_pluck( $terms, 'term_id' );
    $term_ids=array();
    foreach ( $terms as $term ) {
        $term_ids[]=$term->term_id;
    }

    
    //$term_ids=array('39','49','47','54');

    if ( ! $q->is_main_query() ) return;
    if ( ! $q->is_post_type_archive() ) return;
    
    if ( ! is_admin() && is_shop() && !isset($_GET['s']) ) {

        $q->set( 'tax_query', array(

            array(
                'taxonomy' => 'edizione',
                'field' => 'term_id',
                'terms' => $term_ids, // Don't display products in the knives category on the shop page
                'operator' => 'NOT IN',
                'orderby' => 'title',
                'order' => 'ASC'
                
                ),
            array(
                'taxonomy' => 'product_cat',
                'field' => 'term_id',
                'terms' => array(81,82), // Don't display products in the knives category on the shop page
                'operator' => 'NOT IN',
                ),

        ));

        $q->set( 'orderby', 'title' );
        $q->set( 'order', 'ASC' );
    
    }

    remove_action( 'pre_get_posts', 'custom_pre_get_posts_query' );

}


// SLUGIFY
function create_slug($string){
   $slug=preg_replace('/[^A-Za-z0-9-]+/', '-', $string);
   return $slug;
}

/* FIX SINGLE PRODUCT ON PAGE */
// show variation price even if all variations are the same price
add_filter('woocommerce_show_variation_price', function() {return true;});


// Change WooCommerce Variation Image Size to 'entry'
// currently 'shop_single'
// =============================================================================

function change_wc_variation_image_size( $child_id, $instance, $variation ) {
  $attachment_id = get_post_thumbnail_id( $variation->get_variation_id() );
  $attachment    = wp_get_attachment_image_src( $attachment_id, 'full' );
  $image_src     = $attachment ? current( $attachment ) : '';
  $child_id['image_src'] = $image_src;

  return $child_id;
}

add_filter( 'woocommerce_available_variation', 'change_wc_variation_image_size', 10, 3 );


// retrieves the attachment ID from the file URL
function pippin_get_image_id($image_url) {
    global $wpdb;
    $attachment = $wpdb->get_col($wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE guid='%s';", $image_url )); 
        return $attachment[0]; 
}


// retrieves the taxonomies terms classes for a single product
// return string
function get_taxonomies_terms_classes($post_id){
    $output .= " ";
    $term_list = wp_get_post_terms( $post_id, 'product_cat', array( 'fields' => 'all' ) );
    foreach ($term_list as $term) {
        $output .= $term->slug . " ";
    }

    $term_list = wp_get_post_terms( $post_id, 'utilizzo', array( 'fields' => 'all' ) );
    foreach ($term_list as $term) {
        $output .= $term->slug . " ";
    }

    $term_list = wp_get_post_terms( $post_id, 'materiale', array( 'fields' => 'all' ) );
    foreach ($term_list as $term) {
        $output .= $term->slug . " ";
    }

    $connected = get_posts(array(
        'connected_type' => 'designer_to_prodotti',
        'connected_items' => $post_id,
        'posts_per_page' => -1,
    ) );

    foreach ($connected as $conn) {
        $output .= $conn->post_name . " ";
    }

    return $output;
}


/*****  FORM NEWSLETTER  *****/

/* 
    Dato l'IP della richiesta, restituisce il codice dello stato 
    del richiedente
*/
function get_request_country(){
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_URL, 'https://webkolm.com/geoip/?ip=' . $_SERVER['REMOTE_ADDR']);
    $result = curl_exec($ch);
    curl_close($ch);

    $obj = json_decode($result);
    return $obj->country_iso;
}


// LINGUA A 3 CARATTERI
function get_language_nl_code($lang){
    // italiano
    if($lang == 'it'){
        $lang = 'ITA';
    // inglese
    } elseif($lang == 'en'){
        $lang = 'ENG';
    }
    return $lang;
}



// VISUAL COMPOSER ADDON
include("vc_addon.php");


/**
 * Google GA4
 

add_action('wp_head', 'wk_analytics');
function wk_analytics() {?>
	<!-- Google tag (gtag.js) -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-4DFLKBXW9P"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-4DFLKBXW9P');
    </script>

<?
}
*/
?>