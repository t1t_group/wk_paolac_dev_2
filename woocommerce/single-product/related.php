<?php
/**
 * Related Products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/related.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */
/*
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
global $product, $woocommerce_loop;
if ( $related_products ) : ?>



	<section class="related products">

		<h2><?php esc_html_e( 'Related products', 'woocommerce' ); ?></h2>

		<?php woocommerce_product_loop_start(); ?>

			<?php foreach ( $related_products as $related_product ) : ?>
			test
				<?php
				 	$post_object = get_post( $related_product->get_id() );
					setup_postdata( $GLOBALS['post'] =& $post_object );
					include(locate_template('block-related.php')); ?>

			<?php endforeach; ?>

		<?php woocommerce_product_loop_end(); ?>

	</section>

<?php endif;
wp_reset_postdata();
*&

/**
 * Related Products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/related.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */



if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product, $woocommerce_loop;
global $lista_related;
global $storia_related;

$numerostampati=0;

if ( empty( $product ) || ! $product->exists() ) {
	
	return;
}

$related = $product->get_related( $posts_per_page );

if ( sizeof( $related ) == 0 ) {

	
}else{

	$lista_related[]=$product->id;


	$args = apply_filters( 'woocommerce_related_products_args', array(
		'post_type'            => 'product',
		'ignore_sticky_posts'  => 1,
		'no_found_rows'        => 1,
		'posts_per_page'       => 4,
		'orderby'              => $orderby,
		'post__in'             => $related,
		'post__not_in'         => $lista_related,
		'tax_query' => array(
		    array(
		        'taxonomy'  => 'edizione',
		        'field'     => 'term_id',
		        'terms'     => $storia_related, // exclude items media items in the news-cat custom taxonomy
		        'operator'  => 'NOT IN'

		        )
		   )
	) );

	$products = new WP_Query( $args );


	if ( $products->have_posts() ) : ?>
		
			<?php woocommerce_product_loop_start(); 

				?>

				<?php while ( $products->have_posts() ) : $products->the_post(); global $product; ?>

					<?php include(locate_template('block-related.php')); ?>

					<?php $numerostampati++; ?>

				<?php endwhile; // end of the loop. ?>

			

	<?php endif; 
	wp_reset_postdata();?>


	<?php 
}


$difference=4-$numerostampati;
if($difference>=1){

	$lista_related[]=$product->id;

	$args = apply_filters( 'woocommerce_related_products_args', array(
		'post_type'            => 'product',
		'ignore_sticky_posts'  => 1,
		'no_found_rows'        => 1,
		'posts_per_page'       => $difference,
		'orderby'              => $orderby,
		'post__not_in'         => $lista_related,
		'tax_query' => array(
		    array(
		        'taxonomy'  => 'edizione',
		        'field'     => 'term_id',
		        'terms'     => $storia_related, // exclude items media items in the news-cat custom taxonomy
		        'operator'  => 'NOT IN')

		        ),
		   
	) );

	$products = new WP_Query( $args );


	if ( $products->have_posts() ) : ?>


				<?php while ( $products->have_posts() ) : $products->the_post(); global $product; ?>

					<?php include(locate_template('block-related.php')); ?>

				<?php endwhile; // end of the loop. ?>

			

	<?php endif; 
	wp_reset_postdata();

}?>

<?php woocommerce_product_loop_end(); 


