<?php
defined( 'ABSPATH' ) || exit;

if ( ! function_exists( 'wc_get_gallery_image_html' ) ) {
	return;
}
global $product;


?>

<div class="images">

	<?php
	
	  $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );
    $thumb_min = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail' );
	  $url = $thumb['0'];
?>
	  <div id="slider-prodotto" >
        <ul class="slides" id="links">
          
          
          <li data-thumb="<?php echo $thumb_min['0']; ?>">
              <a href="<?php echo $url; ?>" data-gallery="<?php echo $gallery; ?>" style="background-image:url('<?php echo $url; ?>');" >

              	<figure class="woocommerce-product-gallery__wrapper">
              		<div class="woocommerce-product-gallery__image--placeholder">
              		<img src="<?php echo $url; ?>" id="immagine-principale" itemprop="image" class="wp-post-image  zoom" />
              		</div>
              	</figure>
              </a>
          </li>	
          
          <?php
        
           	do_action( 'woocommerce_product_thumbnails' ); 
          ?>


        </ul>
    </div>
	
</div>

	