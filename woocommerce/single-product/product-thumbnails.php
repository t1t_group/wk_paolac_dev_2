<?php
/**
 * Single Product Thumbnails
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-thumbnails.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post, $product, $woocommerce;

$attachment_ids = $product->get_gallery_attachment_ids();

if ( $attachment_ids ) {
	?>
	<?php
		foreach ( $attachment_ids as $attachment_id ) {


			$image_link = wp_get_attachment_url( $attachment_id );

			if ( !$image_link )
				continue;

			$thumb = wp_get_attachment_image_src( $attachment_id, 'large' );
			$thumb_min = wp_get_attachment_image_src( $attachment_id, 'thumbnail' );
			$url = $thumb['0'];

			?>
			<li data-thumb="<?php echo $thumb_min['0']; ?>">
			    <a href="<?php echo $url; ?>" data-gallery="<?php echo $gallery; ?>"  style="background-image:url('<?php echo $url; ?>');" ><img src="<?php echo $url; ?>"/></a>
			      
			</li>
			<?php

		}

	?>

<?php


?>

	

	<?php
}



