<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' ); ?>


	<!--
<div class="row">
	<div class="col-6">
		<a class="select_collection select_shop" style="background-image:url('<?php echo bloginfo( 'stylesheet_directory' ); ?>/img/test_collection.jpg');">
			<div class="col-2"></div>
			<div class="col-9">
				<h2>Collection</h2>
				<div class="padding20">
					Kit consists of more than a hundred ready-to- use elements that you can.
				</div>
			</div>
		</a>
	</div>
	<div class="col-6">
		<a class="select_edition select_shop" style="background-image:url('<?php echo bloginfo( 'stylesheet_directory' ); ?>/img/test_edition.jpg');">
			<div class="col-2"></div>
			<div class="col-9">
				<h2>Editions</h2>
				<div class="padding20">
					Kit consists of more than a hundred ready-to- use elements that you can.
				</div>
			</div>
		</a>
	</div>

</div>
-->

<div class="wrap">
<?php




	// CONTROLLO SE SIAMO IN UNA PAGINA DEL DESIGNER
	if(isset($_GET['designer'])){
		/*
		$my_postid = 4;//This is page id or post id
		$content_post = get_post($my_postid);
		$content = $content_post->post_content;
		$content = apply_filters('the_content', $content);
		$content = str_replace(']]>', ']]&gt;', $content);
		echo $content;
		*/

	?>
	<!--
	<div class="row">
		<div class="col-12">
			<h3><?php _e("Product of", "paolac");?>: <?php echo get_the_title($_GET['designer']); ?></h3>
		</div>
	</div>-->
	<?php 
	$titolo_filtro=get_the_title($_GET['designer']);
	include(locate_template('block_filtri.php')); ?>
	<div class="row">
		<?php 

		// PRODOTTI CORRELATI DESIGNER

		  $connected = new WP_Query( array(
		  'connected_type' => 'designer_to_prodotti',
		  'connected_items' => $_GET['designer'],
		  'posts_per_page' => -1,
		   'orderby' => 'title',
		   'order'=> 'ASC'
		  ) );
		    

		  if ( $connected->have_posts() ) :
		  ?>
		  <?php woocommerce_product_loop_start(); ?>
		  	<?php while ( $connected->have_posts() ) : $connected->the_post(); ?>
		  		<?php wc_get_template_part( 'content', 'product' ); ?>     
		    <?php endwhile; ?>
		  <?php woocommerce_product_loop_end(); ?>
		  <?php 
		  
		  endif;
		  wp_reset_query();

		?>
	</div>


	<?php
	}
	elseif(isset($_GET['s'])){

		// SIAMO IN UNA PAGINA DI RICERCA
	?>
	<div class="row">
		<div class="col-12">
			<h3><?php _e("Product search", "paolac");?>: "<?php echo $_GET['s']; ?>"</h3>

		</div>
	</div>
	<div class="row">
		<?php 
		/*
			$mySearch =& new WP_Query("s=$s & showposts=-1");
			$NumResults = $mySearch->post_count;
			if($NumResults==0){
				echo '<div class="col-12"><h4>'.__("No products was found for your research", 'paolac').'</h4></div>';
			}
			*/
		?>
			<?php if ( have_posts() ) : ?>


				<?php woocommerce_product_loop_start(); ?>

					<?php while ( have_posts() ) : the_post(); ?>

						<?php wc_get_template_part( 'content', 'product' ); ?>
						
					<?php endwhile; // end of the loop. ?>

				<?php woocommerce_product_loop_end(); ?>

				<?php
					/**
					 * woocommerce_after_shop_loop hook.
					 *
					 * @hooked woocommerce_pagination - 10
					 */
					do_action( 'woocommerce_after_shop_loop' );
				?>

			<?php endif; ?>

		
	</div>
	<?php
	}
	elseif(is_shop()){

	// SIAMO NELLO SHOP	
	$my_postid = 4;//This is page id or post id
	$content_post = get_post($my_postid);
	$content = $content_post->post_content;
	$content = apply_filters('the_content', $content);
	$content = str_replace(']]>', ']]&gt;', $content);
	echo $content;

?>

<?php include(locate_template('block_filtri.php')); ?>
<div class="row">
		<?php if ( have_posts() ) : ?>


			<?php woocommerce_product_loop_start(); ?>
							<?php 

							// VISUALIZZO PRIMA I PRODOTTI NEW

							$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
								// are we on page one?
								if(1 == $paged) {
								
							// get all terms in the taxonomy
							$terms = get_terms( 'edizione' ); 
							// convert array of term objects to array of term IDs
							//$term_ids = wp_list_pluck( $terms, 'term_id' );
							$term_ids=array();
							foreach ( $terms as $term ) {
							    $term_ids[]=$term->term_id;
							}


							// ELENCO POST EDIZIONI
							$your_query = new WP_Query( array(
							    'post_type' => 'product',				
							    'posts_per_page' => -1,
								'post_status' => 'publish',
								'orderby' => 'title',
								'order' => 'ASC',
				            	'tax_query' => array(
				            			array(
				            				'taxonomy' => 'product_cat',
				            				'field' => 'term_id',
				            				'terms' => array(81,82), // Don't display products in the knives category on the shop page
				            				'operator' => 'IN',
				            			),
				            			array(
				            				'taxonomy' => 'edizione',
								            'field' => 'term_id',
								            'terms' => $term_ids, // Don't display products in the knives category on the shop page
								            'operator' => 'NOT IN',
								            'orderby' => 'title',
								            'order' => 'ASC',
				            			),
				            		)
						     ));


							
							while ( $your_query->have_posts() ) : $your_query->the_post();

					          	wc_get_template_part( 'content', 'product' );

							endwhile;
							// reset post data (important!)
							wp_reset_postdata();

						}
							?>

				<?php while ( have_posts() ) : the_post(); ?>

					<?php wc_get_template_part( 'content', 'product' ); ?>

				<?php endwhile; // end of the loop. ?>

			<?php woocommerce_product_loop_end(); ?>

			<?php
				/**
				 * woocommerce_after_shop_loop hook.
				 *
				 * @hooked woocommerce_pagination - 10
				 */
				do_action( 'woocommerce_after_shop_loop' );
			?>

		<?php endif; ?>

	
</div>

<?php }elseif(is_archive()) {

	// SIAMO IN UNA PAGINA DI CATEGORIA
	if(is_tax('edizione')){
		
		?>
		<div class="row">
			<div class="col-12">
				<h3>
					<?php echo single_cat_title();?>
				</h3>

			</div>
		</div>
		<?php
	}else {
		/*
	$my_postid = 4;//This is page id or post id
	$content_post = get_post($my_postid);
	$content = $content_post->post_content;
	$content = apply_filters('the_content', $content);
	$content = str_replace(']]>', ']]&gt;', $content);
	echo $content;
*/
	}
		?>
		<!--
		<div class="row">
			<div class="col-12">
				<h3>-->
					<?php
					
						if(is_tax('edizione')){
							// echo single_cat_title();
						}else{
							$nome=single_cat_title('', false); 
							 if(ICL_LANGUAGE_CODE=="it"){
							 	$nome=substr($nome, 0, strpos($nome, ' /'));
							 }
							 else { 
							 	$nome=strstr($nome, ' /'); 
							 	$nome=substr($nome, 2); 
							 }
							$titolo_filtro=$nome; 
							include(locate_template('block_filtri.php'));
						}
					
					?><!--
				</h3>

			</div>
		</div>-->
		<div class="row">
			<?php if ( have_posts() ) : ?>


				<?php woocommerce_product_loop_start(); ?>

					<?php while ( have_posts() ) : the_post(); ?>
						
						<?php wc_get_template_part( 'content', 'product' ); ?>

					<?php endwhile; // end of the loop. ?>

				<?php woocommerce_product_loop_end(); ?>

				<?php
					/**
					 * woocommerce_after_shop_loop hook.
					 *
					 * @hooked woocommerce_pagination - 10
					 */
					do_action( 'woocommerce_after_shop_loop' );
				?>

			<?php endif; ?>
		</div>
	</div>
		<?php

	} ?>

<?php get_footer( 'shop' ); ?>
