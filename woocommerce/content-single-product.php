<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
$product = new WC_Product( get_the_ID() );
do_action( 'woocommerce_before_single_product' );
$short_desc=types_render_field("sottotitolo", array("raw"=>"true"));
$spedizioni=types_render_field("informazioni-sulla-consegna", array("raw"=>"true"));
$anno=types_render_field("anno", array("raw"=>"true"));


?>


<div class="row">
	<div class="col-6">
	<?php
		/**
		 * woocommerce_before_single_product_summary hook
		 *
		 * @hooked woocommerce_show_product_sale_flash - 10
		 * @hooked woocommerce_show_product_images - 20
		 */

		
		remove_action('woocommerce_show_product_sale_flash','woocommerce_before_single_product_summary');
		do_action( 'woocommerce_before_single_product_summary' );
	?>
	</div>

	<div class="col-4 dati-prodotto module">
		<h1 class="prodotto_titolo"><?php the_title(); ?></h1>
		<span class="prodotto_sottotitolo"><?php echo $short_desc; ?></span>
		<?php 
		add_action( 'azione_solo_prezzo', 'woocommerce_template_single_price' );
		do_action( 'azione_solo_prezzo' );
		?>
		
		
		<div class="desc-prodotto">
			<?php 
			
			$stringa_desc=__("Description", "paolac");
			$stringa_det=__("Details", "paolac");
			$stringa_del=__("Delivery", "paolac");

			?>
			<ul class="list_tab_prodotto">
				<li><h5 data-tab="tab_details"><?php _e('details', 'paolac');?></h5></li>
				<li><h5 data-tab="tab_delivery"><?php _e('delivery', 'paolac');?></h5></li>
			</ul>
			<div id="tab_details" class="tab_prodotto">
				<?php
				//$descrizione_designer=apply_filters( 'woocommerce_short_description', $post->post_excerpt );
				// DATI DESIGNER CORRELATO
				$connected = new WP_Query( array(
				'connected_type' => 'designer_to_prodotti',
				'connected_items' => get_queried_object_id(),
				'posts_per_page' => -1,
				) );
				  

				if ( $connected->have_posts() ) :
				?>
				<?php while ( $connected->have_posts() ) : $connected->the_post(); ?>
					<?php 
						$nome_designer=get_the_title();
					?>
					<?php endwhile; ?>
					<?php _e("Designed by", 'paolac')?> 

					<?php foreach ($connected as $conn ) { ?>
					<?php 
						
						//$numItems = count($conn);
						$i = 0;
						foreach ($conn as $key => $con) {
							if ($con->ID) {
								echo get_the_title($con->ID);
							}
							if($con->ID && ++$i != $numItems) {
								echo ' & ';
							}
						}


					?>
					<?php }

				 endif;
				wp_reset_query();?>
				<?php the_content();?></div>
			<div id="tab_delivery" class="tab_prodotto"><?php echo $spedizioni; ?></div>
		</div>
		<br/><br/>
		<?php 
		add_action( 'azione_solo_aggiungi', 'woocommerce_template_single_add_to_cart' );
		do_action( 'azione_solo_aggiungi' );
		?>

		<?php get_template_part( 'block_share' ); ?>
		
	</div>
</div>
<br/><br/>
