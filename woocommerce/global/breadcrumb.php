<?php
/**
 * Shop breadcrumb
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/global/breadcrumb.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.0
 * @see         woocommerce_breadcrumb()
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

?>
<ul class="breadcrumb-shop">

	<li><a href="<?php echo get_permalink( woocommerce_get_page_id( 'shop' ) );?>"><?php _e('Products', 'paolac');?></a></li>
	<?php 

		if(has_term('','edizione')){ 
			
			// LINK TAXONOMY EDITIONS
			$tassonomia=get_permalink( icl_object_id(1358, 'page', false) );
			echo '<li><a href="'.$tassonomia.'">Editions</a></li>';
			
			// LINK SINGLE TERMS
			$terms = get_the_terms( get_the_ID(), 'edizione' );
			print_r($terms); 
		    foreach ( $terms as $term ) {
		        $nome = $term->name;
		        $url = get_term_link( $term );
		    }
			echo '<li><a href="'.$url.'">'.$nome.'</a></li>';

			

		}
		else if(has_term('','collezione')){ 
			// LINK TAXONOMY COLLEZIONE
			$tassonomia=get_permalink( icl_object_id(1360, 'page', false) );
			echo '<li><a href="'.$tassonomia.'">Collection</a></li>';
			// LINK SINGLE TERMS
			$terms = get_the_terms( get_the_ID(), 'collezione' ); 
		    foreach ( $terms as $term ) {
		        $nome = $term->name;
		        $url = get_term_link( $term );
		    }
			echo '<li><a href="'.$url.'">'.$nome.'</a></li>';

		}
		else {
			// LINK SINGLE TERMS
			$terms = get_the_terms( get_the_ID(), 'product_cat' ); 
		    foreach ( $terms as $term ) {
		        $nome = $term->name;
		        $url = get_term_link( $term );
		    }
			echo '<li><a href="'.$url.'">'.$nome.'</a></li>';

		}
	?>
	<li><?php the_title ()?></li>
</ul>