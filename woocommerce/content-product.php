<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see     http://docs.woothemes.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product, $woocommerce_loop;

$url = ;
$titolo=get_the_title();

$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium' );
$url = $thumb['0'];
$short_desc=types_render_field("sottotitolo", array("raw"=>"true"));
?>
<li class="col-4 grid-item wk_product_item"><a href="<?php echo get_the_permalink();?>" class="module <?php if(has_term('','edizione')){ echo "editions";} ?>"><div class="mask_outline"><img src="<?= $url; ?>" title="<?php echo $titolo; ?> - <?php echo $short_desc; ?>" alt="<?php echo $titolo; ?> - <?php echo $short_desc; ?>"/>

	<?php 

	if ( has_term( 'novita', 'utilizzo' ) || has_term( 'novita-new', 'product_cat' ) ) {

		echo '<span class="new_flag">NEW</span>';
	}
	if ( has_term( 'exclusive-deal', 'utilizzo' ) ) {
		echo '<span class="offerta-esclusiva_flag">' . __('Exclusive deal', 'webkolm') . '</span>';
	}
	?></div><h4 class="prodotto_titolo"><?php echo $titolo; ?>
	
	<?php 

		if ( $product->is_type( 'variable' ) ) {
			echo '<span class="more_variations nomobile">'.__("+ Variations", "paolac").'</span>';
		}

	?>
</h4><span class="prodotto_sottotitolo"><?php echo $short_desc; ?></span><span class="prodotto_prezzo"><?php echo $product->get_price_html(); ?></span></a></li>


<?php
/*
		if($product->product_type == 'variable'){
			echo '<span class="more_variations nomobile">'.__("+ Variations", "paolac").'</span>';
		}*/
?>