<?php
/**
 * The Template for displaying all single products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' ); ?>


<?php // get_template_part('block_breadcrumbs'); ?>	
<div itemscope itemtype="<?php echo woocommerce_get_product_schema(); ?>"  <?php post_class(); ?>>

	
		<?php  while ( have_posts() ) : the_post(); ?>

			<?php wc_get_template_part( 'content', 'single-product' ); ?>
			<?php //include(locate_template('woocommerce/content-single-product.php')); ?>

		<?php endwhile; // end of the loop. ?>

<?php include(locate_template('block_correlati_prodotto.php')); ?>
<br/><br/>
<div class="row module">
	<div class="col-12">
		<h3 class="linea"><?php _e('You may also like', 'paolac'); ?></h3>
	</div>
</div>

<div class="row nuovi_prodotti products">
		<?php 
				add_action( 'azione_solo_correlati', 'woocommerce_output_related_products' );
				do_action( 'azione_solo_correlati' );

		?>
</div>
</div>

<?php get_footer( 'shop' ); ?>
