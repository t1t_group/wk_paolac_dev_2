<?php
global $lista_related;
global $storia_related;

// SE È EDITION
if(has_term('','edizione')){ 

	$id_current_product=$post->ID;
	$terms = get_the_terms( get_the_ID(), 'edizione');
	foreach( $terms as $term ){
	  $titolo_collezione= $term->name ;
	  $edizione=$term->slug;
	  $edizione_ID=$term->term_id;
	}
	$sottotitolo=__("Other pieces from this Story", 'paolac');

	$storia_related = $edizione_ID;

	// reset post data (important!)
	wp_reset_postdata();

?>


<!-- 	HIGHLIGHTS	-->
	<br/><br/>
	<div class="row" id="trigger">
		<div class="col-12">
			<h3 class="linea"><?= $titolo_collezione; ?></h3>
		</div>
	</div>
	<?php 
		$wkyour_query = new WP_Query( array(
		  	'post_type' => 'product',
		  	'edizione' => $edizione,
		  	'post_status' => 'publish',
		  	'posts_per_page' => 8,
		  	'post__not_in' => array($id_current_product),
		  	'orderby'=>'rand',
		  	'order'=>'ASC'
		));
	?>
	<div class="highlights-container">
		<div class="highlights-row" id="target">

			<?php
			while ( $wkyour_query->have_posts() ) : $wkyour_query->the_post(); global $product;
			    
			    $custom_field = get_post_custom($post->ID);
			    $url_prod=get_permalink();
			    $sottotitolo_prod=types_render_field("sottotitolo");
			    $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium' );
			    $url = $thumb['0'];
			    $lista_related[]=$post->ID;

			    ?>
			    <div class="prodotto-correlato highlights-item module">
			      <a href="<?= $url_prod; ?>" class="<?php if(has_term('','edizione')){ echo "editions";} ?>">
			        <div class="prodotto_mask mask_outline" style="background-image:url('<?= $url; ?>');"><?php 
						if ( has_term( 'novita', 'utilizzo' ) || has_term( 'novita-new', 'product_cat' ) ) {
							echo '<span class="new_flag">NEW</span>';
						}
						if ( has_term( 'exclusive-deal', 'utilizzo' ) ) {
							echo '<span class="offerta-esclusiva_flag">' . __('Exclusive deal', 'webkolm') . '</span>';
						}
						?>
					</div>
						
					<h4 class="prodotto_titolo"><?php the_title(); ?><?php 
						if ( $product->is_type( 'variable' ) ) {
							echo '<span class="more_variations nomobile">'.__("+ Variations", "paolac").'</span>';
						} ?>
					</h4>
				    <span class="prodotto_sottotitolo"><?= $sottotitolo_prod; ?></span>
				    <span class="prodotto_prezzo"><?php echo $product->get_price_html() ?></span>
				  </a>
				</div>
				<?php
				    
				endwhile;
				// reset post data (important!)
				wp_reset_postdata();
			//} ?>
		</div>
	</div>


<!-- 	SE NON C'E' EDIZIONE, USO LA FAMIGLIA		-->
<!-- 	FAMIGLIA		-->

<?php } elseif(has_term('','famiglia')){ 

	$id_current_product=$post->ID;
	$terms = get_the_terms( get_the_ID(), 'famiglia');
	foreach( $terms as $term ){
	  $titolo_collezione= $term->name ;
	  $famiglia=$term->slug;
	  $famiglia_ID=$term->term_id;
	}
?>

	<!-- HIGHLIGHTS	-->
	<br/><br/>
	<div class="row" id="trigger">
		<div class="col-12">
			<h3 class="linea"><?= $titolo_collezione; ?></h3>
		</div>
	</div>
	<?php 
		$wkyour_query = new WP_Query( array(
		  	'post_type' => 'product',
		  	'famiglia' => $famiglia,
		  	'post_status' => 'publish',
		  	'posts_per_page' => 8,
		  	'post__not_in' => array($id_current_product),
		  	'orderby'=>'rand',
		  	'order'=>'ASC'
		));
	?>
	<div class="highlights-container">
		<div class="highlights-row" id="target">

			<?php
			while ( $wkyour_query->have_posts() ) : $wkyour_query->the_post(); global $product;
			    
			    $custom_field = get_post_custom($post->ID);
			    $url_prod=get_permalink();
			    $sottotitolo_prod=types_render_field("sottotitolo");
			    $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium' );
			    $url = $thumb['0'];
			    $lista_related[]=$post->ID;

			    ?>
			    <div class="prodotto-correlato highlights-item module">
			      <a href="<?= $url_prod; ?>" class="<?php if(has_term('','famiglia')){ echo "editions";} ?>">
			        <div class="prodotto_mask mask_outline" style="background-image:url('<?= $url; ?>');"><?php 
						if ( has_term( 'novita', 'utilizzo' ) || has_term( 'novita-new', 'product_cat' ) ) {
							echo '<span class="new_flag">NEW</span>';
						}
						if ( has_term( 'exclusive-deal', 'utilizzo' ) ) {
							echo '<span class="offerta-esclusiva_flag">' . __('Exclusive deal', 'webkolm') . '</span>';
						}
						?>
					</div>
						
					<h4 class="prodotto_titolo"><?php the_title(); ?><?php 
						if ( $product->is_type( 'variable' ) ) {
							echo '<span class="more_variations nomobile">'.__("+ Variations", "paolac").'</span>';
						} ?>
					</h4>
				    <span class="prodotto_sottotitolo"><?= $sottotitolo_prod; ?></span>
				    <span class="prodotto_prezzo"><?php echo $product->get_price_html() ?></span>
				  </a>
				</div>
				<?php
				    
				endwhile;
				// reset post data (important!)
				wp_reset_postdata();
			//} ?>
		</div>
	</div>

<?php } ?>

<!-- 	END FAMIGLIA		-->


<!--  LOOKBOOK  -->

<?php
$gallery_lookbook = get_post_meta($post->ID, 'wpcf-immagini-lookbook');

if( !empty($gallery_lookbook[0]) ){
?>
	<br/><br/>
	<div class="row">
		<div class="col-12 lookbook-header">
			<h3 class="lookbook-titolo">Lookbook</h3>
			<span class="lookbook-subtitle">Be inspired</span>
		</div>
	</div>
	<div class="lookbook-container">
		<div class="lookbook-row">

			<div id="lookbook_slider" class="webkolm-slider">
				<ul class="slides">
					<?php

			        	preg_match('/\[gallery.*ids=.(.*).\]/', $gallery_lookbook[0], $ids);
			        	$array_id = explode(",", $ids[1]);
			        	$numslide=0;
			        	foreach($array_id as $foto){

			        		$class = "";
			        		if($numslide % 2 == 0){ $class=" odd "; }

			        		$thumb_img = get_post( $foto ); 
			        		$images_s= wp_get_attachment_image_src( $foto, 'medium' );
			        		$images_m= wp_get_attachment_image_src( $foto, 'large' );
			        		$images_b= wp_get_attachment_image_src( $foto, 'full' );

			        		?>

			        		<li class="slide-lookbook <?php echo $class; ?>">
			        		    <div class="slideimg-lookbook-<?= $numslide ?> slideimg">
			        		    	<!--img src="<?= $images_m['0'] ?>" /-->
			        		    	<style skip_me="1" wk_skip_me="1">
			        		    		.slideimg-lookbook-<?= $numslide ?> {
			        		    			background-image: url('<?= $images_m['0'] ?>');
			        		    		}
			        		    	</style>
			        		    </div>
			        		</li>
			        		<?php
			        		$numslide++;
			        	}
					?>
				</ul>
			</div>
		</div>
	</div>
<?php } ?>






<!-- 	DESIGNER	-->
<br/><br/>
<?php
$descrizione_designer=apply_filters( 'woocommerce_short_description', $post->post_excerpt );
// DATI DESIGNER CORRELATO
$connected = new WP_Query( array(
	'connected_type' => 'designer_to_prodotti',
	'connected_items' => get_queried_object(),
	'posts_per_page' => 1,
) );
  

if ( $connected->have_posts() ) :
?>
<div class="row">
		<div class="col-12 lookbook-header">
			<h3 class="lookbook-titolo">Designer</h3>
		</div>
	</div>
<div class="row">
<?php while ( $connected->have_posts() ) : $connected->the_post(); ?>
	<?php 
		$thumb_m = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );
		$thumb_designer = $thumb_m['0'];
		$nome_designer=get_the_title();
		$id_designer=get_the_ID();
		$shop_page_url = get_permalink( woocommerce_get_page_id( 'shop' ) );

		if($descrizione_designer==""){
			$descrizione_designer=apply_filters('the_content', get_the_content());
			$descrizione_designer=limit_text($descrizione_designer, 80);
			$link_designer="0";
		}
		
		
	?>
	<?php endwhile; ?>
	<div class="col-2"></div>
	<div class="col-4 module">
		<img src="<?= $thumb_designer ?>">
	</div>
	<div class="col-5 module">
		<h3 class="padding10 titolo_designer"><?= $nome_designer; ?></h3>
		<div class="padding10">
			<?= $descrizione_designer; ?>
			<br/>
			<?php 
				if($link_designer==1){
					?><a class="freccia_sx" href="<?= $shop_page_url?>?designer=<?= $id_designer; ?>"><?php _e('Products','paolac');?><!-- <?= $nome_designer; ?>--></a><?php
				}else{
					?><div class="freccia_sx item-overlay" href="<?php echo get_the_permalink();?>"><?php _e('Discover','paolac');?><!-- <?= $nome_designer; ?>--></div><?php
				}
			?>
		</div>
	</div>
</div>
<?php endif;
wp_reset_query();


?>

  	



