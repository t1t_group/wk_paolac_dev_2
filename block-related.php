<?php
$custom_field = get_post_custom($post->ID);
$url_prod=get_permalink();
$sottotitolo_prod=types_render_field("sottotitolo");

/*
$img_quadrata=types_render_field("immagine-quadrata", array("raw"=>"true"));
if($img_quadrata!=""){
  
  $url = $img_quadrata;
}
*/
  $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium' );
  $url = $thumb['0'];




?>
<div class="col-3 prodotto-correlato module">
  <a href="<?= $url_prod; ?>" class="<?php if(has_term('','edizione')){ echo "editions";} ?>">
    <div class="prodotto_mask mask_outline" style="background-image:url('<?= $url; ?>');"><?php 

	if ( has_term( 'novita', 'utilizzo' ) || has_term( 'novita-new', 'product_cat' ) ) {

		echo '<span class="new_flag">NEW</span>';
	}
  if ( has_term( 'exclusive-deal', 'utilizzo' ) ) {
    echo '<span class="offerta-esclusiva_flag">' . __('Exclusive deal', 'webkolm') . '</span>';
  }
	?></div>
    <h4 class="prodotto_titolo"><?php the_title(); ?><?php 

		if ( $product->is_type( 'variable' ) ) {
			echo '<span class="more_variations nomobile">'.__("+ Variations", "paolac").'</span>';
		}
	
	?></h4>
    <span class="prodotto_sottotitolo"><?= $sottotitolo_prod; ?></span>
    <span class="prodotto_prezzo"><?php echo $product->get_price_html() ?></span>
  </a>
</div>
