<?php
/**
 * 
 * Template Name: About
 */
get_header();

// PAGINA ABOUT STATICA
 ?>
 <div class="wrap">
	<?php if (have_posts()) : ?>
		<?php while (have_posts()) : the_post(); ?>
			<div class="pagina-about">
				<?php the_content();?>

			<!--
			<div class="row">
				<div class="col-5">
					<br/><br/><br/>
					<h1>Paola C.</h1>
					<br/>
					<div class="padding20">
						<p>The Paola C. showroom is located in the courtyard of via Solferino 11, Milan and hosts a tableware collection based on a youthful yet refined interpretation.</p>
						<p>The Paola C. collections of objects made using a variety of materials are created by Italian and foreign designers and artists under the art direction of Aldo Cibic.</p>
					</div>
				</div>
				<div class="col-1"></div>
				<div class="col-5">
					<img src="<?php echo bloginfo( 'stylesheet_directory' ); ?>/img/test_about1.jpg"/>
				</div>
			</div>
			
			<div class="row">
				<blockquote>“...proposes herself as the editor of simple collections out of the ordinary tableware.”</blockquote>
			</div>

			<div class="row">
				<div class="col-1"></div>
				<div class="col-5">
					<img src="<?php echo bloginfo( 'stylesheet_directory' ); ?>/img/test_about2.jpg"/>
				</div>
				<div class="col-1"></div>
				<div class="col-4">
					IPaola C.’s collaboration with Aldo Cibic started in 2000 with a series of hand-finished ceramics and a line of mouth-blown glassware. Over the years, the collection as well as the search for new materials have expanded, maintaining the goal to create practical, elegant and poetic objects. 
					Paola C.’s commitment to offering opportunities to the young – and Aldo Cibic’s relationship with various universities – started a constant and fruitful collaboration with schools through a series of exhibitions, each dedicated to a single school. These initiatives helped create collections of products, most of which are now well-known and successful.
				</div>
			</div>
			-->
			</div>
		<?php endwhile; ?>
	<?php endif; ?>
<?php get_footer(); ?>