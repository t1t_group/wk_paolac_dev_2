<div class="filter-header">
    <h3 class="filter-title"><?php echo __("Filters","webkolm"); ?><span class="icona_filter"><?php get_template_part('img/filter.svg'); ?></span></h3>
    <div class="close-btn">
      <span class="line"></span>
      <span class="line"></span>
    </div>
</div>

<!-- FILTRI UTILIZZO -->
<h3 class="filter-title linea"><?php echo __("Use for","webkolm"); ?></h3>

<?php
$terms = get_categories(
  array(
    'taxonomy' => 'utilizzo',
    'hide_empty' => true,
  )
);

if(!empty($terms)){ ?>

  <ul class="filter-container filter-utilizzo">

    <?php
    foreach ($terms as $term) { 

        ?>
         <li class="filter-item" data-category=".<?php echo $term->slug; ?>" data-gruppo=".utilizzo">
            <span class="filter-item-name"><?php echo get_filter_label($term->name); ?></span>
         </li>
    <?php } ?>

  </ul>
<?php } ?>


<!-- FILTRI CATEGORIE -->
<h3 class="filter-title linea"><?php echo __("Categories","webkolm"); ?></h3>

<?php
$terms = get_categories(
  array(
    'taxonomy' => 'product_cat',
    'hide_empty' => true,
  )
);

if(!empty($terms)){ ?>

  <ul class="filter-container filter-category">

    <?php
    foreach ($terms as $term) { 

        ?>
         <li class="filter-item" data-category=".<?php echo $term->slug; ?>" data-gruppo=".categorie">
            <span class="filter-item-name"><?php echo $term->name; ?></span>
         </li>
    <?php } ?>

  </ul>
<?php } ?>


<!-- FILTRI MATERIALE -->
<h3 class="filter-title linea"><?php echo __("Materials","webkolm"); ?></h3>

<?php
$terms = get_categories(
  array(
    'taxonomy' => 'materiale',
    'hide_empty' => true,
  )
);

if(!empty($terms)){ ?>

  <ul class="filter-container filter-materiale">

    <?php
    foreach ($terms as $term) { 

        ?>
         <li class="filter-item" data-category=".<?php echo $term->slug; ?>" data-gruppo=".materiale">
            <span class="filter-item-name"><?php echo get_filter_label($term->name); ?></span>
         </li>
    <?php } ?>

  </ul>
<?php } ?>



<!-- FILTRI DESIGNERS -->
<div class="filtri-designer">
  <h3 class="filter-title linea"><?php echo __("Designers","webkolm"); ?></h3>

  <?php
  $designers = get_posts( array(
      'post_type' => 'designers',       
      'posts_per_page' => -1,
      'post_status' => 'publish',
      'orderby' => 'menu_order',
      'order'=>'ASC',
      'suppress_filters' => false,
  ));

  if(!empty($designers)){ ?>

    <ul class="filter-container filter-designer">

      <?php
      foreach ($designers as $designer) { 

          ?>
           <li class="filter-item" data-category=".<?php echo $designer->post_name; ?>" data-gruppo=".designer">
              <span class="filter-item-name"><?php echo $designer->post_title; ?></span>
           </li>
      <?php } ?>

    </ul>
  <?php } ?>
</div>

<?php
function get_filter_label($label){
    $label_lang = explode(" / ", $label);
    if(ICL_LANGUAGE_CODE == 'it'){
        return $label_lang[0];
    } else {
        return $label_lang[1];
    }
}

?>