<?php 
/**
 * 
 * Template Name: Hospitality
 */
get_header(); ?>

	<?php if (have_posts()) : ?>
		<?php while (have_posts()) : the_post(); ?>
			<?php the_content(); ?>
		<?php endwhile; ?>
	<?php endif; ?>


	<?php 
	// ELENCO POST EDIZIONI
	$your_query = new WP_Query( array(
	    'post_type' => array( 'hospitalities'),				
	    'posts_per_page' => -1,
		'post_status' => 'publish',
		'orderby' => 'menu_order',
    	'order'=>'ASC'
     ));
	?>

	<?php
	if($your_query->have_posts()): ?>
	<div class="wk_hospitality_list wrap">
		<div class="wk_hospitality_header">
			<h3><?php echo __("our projects and collaborations", "webkolm"); ?></h3>
		</div>
		<?php
		$num_item = 0;
		while( $your_query->have_posts() ) : $your_query->the_post(); 

    		$gallery = types_render_field("galleria-hospitality", array("raw"=>"true"));
    		    
    		// STAMPO LA GALLERY SE PRESENTE
    		
    		if($gallery!=""){
				$post_content = $gallery;
				preg_match('/\[gallery.*ids=.(.*).\]/', $post_content, $ids);
				$array_id = explode(",", $ids[1]);
				$url_thumb = wp_get_attachment_image_src( $array_id[0], 'medium' );
				$url_thumb_big = wp_get_attachment_image_src( $array_id[0], 'large' );
			}

			$image_sizes = getimagesize($url_thumb[0]);
			$classes = " horizontal-item ";
			if($image_sizes[0] <= $image_sizes[1]){
				$classes = " vertical-item ";
			}

			$is_even = false;
			$item_classes = " odd "; 
			if($num_item % 2){ 
				$is_even = true;
				$item_classes = "";
			}
			?>

	  		<div class="wk_hospitality_item <?php echo $item_classes; ?>">
	  			
	  			<div class="wk_item_cover <?php echo $classes; ?>">
	  				<img src="<?php echo $url_thumb[0]; ?>" class="wk_item_cover_img">
	  			</div>

	  			<div class="wk_item_content">
	  				<?php
	  			    $periodo=types_render_field("anno-hospitality", array("row" => "true"));
	  			    $location=types_render_field("location", array("row" => "true"));
	  			    $designer=types_render_field("designer", array("row" => "true"));

	  			    $term_obj_list = get_the_terms( $post->ID, 'tipologia-hospitality' );
	  			    $terms_string = join(' - ', wp_list_pluck($term_obj_list, 'name'));
					?>
					<span class="hospitality-tag"><?php echo $terms_string; ?></span>
					<h2>
						<?php the_title();?><br>
						- <?php echo $location; ?><br>
						by <?php echo $designer; ?>
					</h2>	

					<div class="wk_pulsante_container">
					    <a href="<?php echo get_the_permalink($post->ID); ?>" class="<?php if($is_even){ echo 'freccia_dx'; }else{echo 'freccia_sx';}?>"><?php echo __("See more", "webkolm");?></a>
					</div>
	  			</div>

	  		</div>

		<?php 
		$num_item++;
		endwhile; ?>
	</div>
	<?php endif;
	// reset post data (important!)
	wp_reset_postdata();
	?>
	


<?php get_footer(); ?>
