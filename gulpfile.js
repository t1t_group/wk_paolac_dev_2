var gulp = require('gulp');
var repoWatch = require('gulp-repository-watch');
var gutil = require('gulp-util');
var git = require('gulp-git');
var runSequence = require('run-sequence');


// debugger

var plumber = require('gulp-plumber');
var notify = require('gulp-notify');
var uglify = require('gulp-uglify');
var sass = require('gulp-sass');
var compass = require('compass-importer')
  var concat = require('gulp-concat');

var minifycss = require('gulp-minify-css');
var prefix = require('gulp-autoprefixer');
var gulpif = require('gulp-if'); 
var prompt = require('gulp-prompt');
var rsync  = require('gulp-rsync');
var argv   = require('minimist')(process.argv);

gulp.task('check', function() {
  repoWatch({
          repository: 'https://bitbucket.org/zeronicola3/wk_paolac_dev_2.git'
      })
    .on('check', function() {
      git.fetch('', '', {args: '--all'}, function (err) {
        if (err) throw err;
      });
      gulp.watch(['./*.php'], ['changed']);
      gulp.watch(['./css/*.scss'], ['changed']);
      gulp.watch('./js/*.js', ['uglify','changed']);

    })
    .on('change', function(newHash, oldHash) {
      console.log('Changed from ', oldHash, ' to ', newHash);
    });
});

// Run git fetch
// Fetch refs from all remotes
gulp.task('fetch', function(){
  git.fetch('', '', {args: '--all'}, function (err) {
    if (err) throw err;
  });
});

// Run git fetch
// Fetch refs from origin
gulp.task('fetch', function(){
  git.fetch('origin3', '', function (err) {
    if (err) throw err;
  });
});


gulp.task('deploy', function() {
// Dirs and Files to sync
  rsyncPaths = ['*'];
// Default options for rsync
  rsyncConf = {
    progress: true,
    incremental: true,
    relative: true,
    emptyDirectories: true,
    recursive: true,
    clean: true,
    exclude: ['gulpfile.js', 'sftp-config.json'],
  };

  rsyncConf.hostname = '164.92.205.113'; // hostname
  rsyncConf.username = 'serverpilot'; // ssh username
  rsyncConf.destination = '/srv/users/serverpilot/apps/paolac/public/wp-content/themes/paolac'; // path where uploaded files go
  // Use gulp-rsync to sync the files 
  return gulp.src(rsyncPaths)
  .pipe(gulpif(
      argv.production, 
      prompt.confirm({
        message: 'Heads Up! Are you SURE you want to push to PRODUCTION?',
        default: false
      })
  ))
  .pipe(rsync(rsyncConf));
 
});

function throwError(taskName, msg) {
  throw new gutil.PluginError({
      plugin: taskName,
      message: msg
    });
}

  var plumberErrorHandler = { errorHandler: notify.onError({
      title: 'Gulp',
      message: 'Error: <%= error.message %>'
    })
  };

// compile all your Sass
  gulp.task('sass', function (){
    gulp.src(['./css/*.scss', '!./css/_*.scss', '!./css/stile.css'])
      .pipe(plumber(plumberErrorHandler))
      .pipe(sass({ includePaths: [
       '../node_modules/compass-mixins/lib'
      ] }))
      /*.pipe(plumber(plumberErrorHandler))
      .pipe(sass({
        includePaths: ['./css/sass'],
        outputStyle: 'nasted'
      })) */
      .pipe(prefix(
        "last 1 version", "> 1%", "ie 8", "ie 7"
        ))
      //.pipe(gulp.dest('./css'))
      .pipe(minifycss())
      .pipe(gulp.dest('./css'));
    });  

  // Uglify JS
    gulp.task('uglify', function(){
      //gulp.src('./js/*.js')
      gulp.src([
        './js/jquery-1.7.1.min.js',
        './js/jquery.blueimp-gallery.min.js',
        './js/jquery.flexslider-min.js',
        './js/jquery.visible.min.js',
        './js/jquery.waypoints.min.js',
        './js/jscroll.js',
        './js/isotope.pkgd.min.js',
        './js/infinitescroll.min.js.js',
        './js/imagesloaded.pkgd.min.js',
        './js/masonry.pkgd.min.js',
        './js/modernizr.custom.js',
        './js/IEffembedfix.jQuery.js',
        './js/respond.min.js',
        './js/sidebarEffects.js',
        './js/sito.js',
        ])
        //.pipe(plumber(plumberErrorHandler))
        //.pipe(uglify())
        
        .pipe(concat('sito-min.js'))
        .pipe(gulp.dest('./js/min/'));
    });



gulp.task('commit-changes', function () {
  return gulp.src('.')
    .pipe(git.add())
    .pipe(git.commit('[mac2]'));
});

gulp.task('push-changes', function (cb) {
  git.push('origin3', 'master', cb);
});

gulp.task('default', function () {
  //gulp.watch(['**/*.php'], ['changed']);
  gulp.watch(['./css/*.scss','./css/**/*.scss'], ['sass']);
  gulp.watch('./js/*.js', ['uglify']);

});

gulp.task('changed', function(callback) {
  runSequence(
    //'deploy',
    // 'commit-changes',
    // 'pull',
    // 'push-changes',

    function (error) {
      if (error) {
        console.log(error.message);
      } else {
        console.log('RELEASE FINISHED SUCCESSFULLY');
      }
      callback(error);
    });
});


gulp.task('pull', function(){
  git.pull('origin3', 'master', function (err) {
    if (err) throw err;
  });
});


/*
var runSequence = require('run-sequence');
var gutil = require('gulp-util');
var git = require('gulp-git');
var fs = require('fs');

gulp.task('commit-changes', function () {
  return gulp.src('.')
    .pipe(git.add())
    .pipe(git.commit('[Prerelease] Bumped version number'));
});

gulp.task('push-changes', function (cb) {
  git.push('origin', 'master', cb);
});
 
gulp.task('pull', function(){
  git.pull('origin', 'master'], function (err) {
    if (err) throw err;
  });
});

gulp.task('seq', function (callback) {
  runSequence(
    'commit-changes',
    'push-changes',
    'create-new-tag',
    'github-release',
    function (error) {
      if (error) {
        console.log(error.message);
      } else {
        console.log('RELEASE FINISHED SUCCESSFULLY');
      }
      callback(error);
    });
});

*/
