<?php
get_header();

 /*éif($_GET['rel']!='tab'){
  get_header(); } else { ?>

<script>
     $(".overlay-close").click(function(){
          $("body").toggleClass("bloccoscroll");
          $("html").toggleClass("bloccoscroll");
          $("#singolo-overlay").removeClass("open");
          $("#singolo-overlay").removeClass("loadingOff");
          $(".wrapper-singolo").fadeOut();
          
          var paginaold="<?php echo $_POST['paginaold'];?>"
          window.history.pushState({path:paginaold},'',paginaold);
    });
</script>

<?php } */?>


<?php if(have_posts()) : while(have_posts()) : the_post(); 

    $thumb_m = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );
    $url_m = $thumb_m['0'];
  ?>
  <div class="wrapper-singolo pagina-singola single-hospitality" >

    <div class="row">
      <div class="col-2"></div>
      <div class="col-8 module">
      	<div class="slider-news slider-hospitality">
        <ul class="slides">
        	<?php 
        		// SLIDER
        		$gallery = types_render_field("galleria-hospitality", array("raw"=>"true"));
        		    
        		
        		// STAMPO LA GALLERY SE PRESENTE
        		
        		if($gallery!=""){
		          $post_content = $gallery;
		          preg_match('/\[gallery.*ids=.(.*).\]/', $post_content, $ids);
		          $array_id = explode(",", $ids[1]);

		          
		          foreach ($array_id as &$item) {
		              $url_thumb=wp_get_attachment_image_src( $item, 'medium' );
		              $didascalia=wp_get_attachment($item);
		              $didascalia=$didascalia['caption'];
                  $slide_num = rand(99, 99999);
		              ?>
		              <li class="wk-hospitality-slide wk-hospitality-slide-<?php echo $slide_num; ?>">
                      <img src="<?php echo $url_thumb[0]; ?>" class="onlymobile slideimg" />
                      <style skip_me="1" wp_skip_me="1">
                          .wk-hospitality-slide-<?php echo $slide_num; ?> {
                              background-image:url('<?php echo $url_thumb[0]; ?>'); 
                          }
                      </style>
		              </li>
		              <?php 
		          }
        		}
        	?>
        </ul>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-2"></div>
      <div class="col-8 content-hospitality module">
      	<?php
          $periodo=types_render_field("anno-hospitality", array("row" => "true"));
          $location=types_render_field("location", array("row" => "true"));
          $designer=types_render_field("designer", array("row" => "true"));

          $term_obj_list = get_the_terms( $post->ID, 'tipologia-hospitality' );
          $terms_string = join(' - ', wp_list_pluck($term_obj_list, 'name'));
        ?>
        <span class="hospitality-tag"><?php echo $terms_string; ?></span>
      	<h2>
          <?php the_title();?><br>
          - <?php echo $location; ?><br>
          by <?php echo $designer; ?>
        </h2>
      </div>
      <div class="col-2"></div>
    </div>
    <div class="row">
      <div class="col-2"></div>
      <div class="hospitality-content col-8"><?php the_content(); ?></div>
    </div>
  </div>
<?php endwhile; 
endif; ?>


<!--  HIGHLIGHTS  -->
<?php 
  $connected = new WP_Query( array(
    'connected_type' => 'hospitality_to_prodotti',
    'connected_items' => get_queried_object(),
    'posts_per_page' => 8,
  ) );

  if($connected->found_posts > 4){
      $highlight_classes = " highlights-row ";
      $highlight_id = "target";
  } else {
      $highlight_classes = " highlights-row no-animation ";
      $highlight_id = "";
  }
?>
<?php if( $connected->have_posts() ) : ?>

    <br/><br/>
    <div class="row" id="trigger">
      <div class="col-12">
        <h3 class="related-title"><?php echo __("Product used in this project", "webkolm"); ?></h3>
      </div>
    </div>
    <div class="highlights-container">
      <div class="<?php echo $highlight_classes; ?>" id="<?php echo $highlight_id; ?>" >

        <?php
        while ( $connected->have_posts() ) : $connected->the_post(); global $product;
            
            $custom_field = get_post_custom($post->ID);
            $url_prod=get_permalink();
            $sottotitolo_prod=types_render_field("sottotitolo");
            $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium' );
            $url = $thumb['0'];
            $lista_related[]=$post->ID;

            ?>
            <div class="prodotto-correlato highlights-item module">
              <a href="<?= $url_prod; ?>" class="<?php if(has_term('','edizione')){ echo "editions";} ?>">
                <div class="prodotto_mask mask_outline" style="background-image:url('<?= $url; ?>');"><?php 
              if ( has_term( 'novita', 'utilizzo' ) || has_term( 'novita-new', 'product_cat' ) ) {
                echo '<span class="new_flag">NEW</span>';
              }
              if ( has_term( 'exclusive-deal', 'utilizzo' ) ) {
                echo '<span class="offerta-esclusiva_flag">' . __('Exclusive deal', 'webkolm') . '</span>';
              }
              ?>
            </div>
              
            <h4 class="prodotto_titolo"><?php the_title(); ?><?php 
              if ( $product->is_type( 'variable' ) ) {
                echo '<span class="more_variations nomobile">'.__("+ Variations", "paolac").'</span>';
              } ?>
            </h4>
              <span class="prodotto_sottotitolo"><?= $sottotitolo_prod; ?></span>
              <span class="prodotto_prezzo"><?php echo $product->get_price_html() ?></span>
            </a>
          </div>
          <?php
              
          endwhile;
          // reset post data (important!)
          wp_reset_postdata();
        //} ?>
      </div>
    </div>
    <div class="row wk_contattaci">
      <div class="col-2"></div>
      <div class="col-8">
          <p><a href="<?php echo get_the_permalink(icl_object_id(46, 'page', true)); ?>" >
          <?php if(ICL_LANGUAGE_CODE == 'it'){ ?>
            Ti interessa collaborare con Paola C. per un progetto contract o su misura? Contattaci per scoprire di più.
          <?php } else { ?>
            Are you interested in collaborating with Paola C. for a contract or custom project? Contact us to find more information.
          <?php } ?>
          </a></p>
          <br>
          <div class="wk_pulsante_container align-center">
              <a href="<?php echo get_the_permalink(icl_object_id(46, 'page', true)); ?>" class="freccia_dx"><?php echo __("Contact us", "webkolm");?></a>
          </div>
      </div>
    </div>
<?php endif; ?>

<?php get_footer(); ?>
	<script>
	if($(".slider-news").length > 0){
		$('.slider-news').flexslider({
		    animation: "fade",
		    animationLoop: true,
		    slideshowSpeed : "4000",
		     pauseOnHover: true,
		    multipleKeyboard: true,
		    keyboard: true,

		});
	}

	</script>
