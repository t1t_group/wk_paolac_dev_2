<?php 

/* SOCIAL ADD ON */
add_action( 'vc_before_init', 'wk_social_share_build' );
function wk_social_share_build() {
    vc_map( array(
        "name" => __( "Social share", "webkolm" ),
        "base" => "social_share",
        "icon" => get_template_directory_uri() . "/img/VC/w.png",
        "description" => __("Insert social share buttons", 'webkolm'),
        "class" => "wk-social_share",
        "category" => __( "webkolm addons", "webkolm"),
        "params" => array(
            array(
                'type' => 'textfield',
                'heading' => "Text",
                'param_name' => 'wk_text',
                'value' => "",
                'description' => __( "es. share on", "webkolm" )
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Select alignment", "webkolm" ),
                "param_name" => "wk_alignment",
                "value" => array( "left", "center", "right" ),
                "description" => __( "Choose the alignment of the buttons (default left)", "webkolm" )
            ),
            array(
                "type" => "colorpicker",
                "heading" => __( "Select color", "webkolm" ),
                "param_name" => "primary_color",
                "value" => "",
                "description" => __( "Color for text and icon (hover will be red)", "webkolm" )
            ),
            
        )
    ) );
}



add_shortcode( 'social_share', 'wk_social_share_func' );
function wk_social_share_func( $atts ) {
    extract( shortcode_atts( array(
        'wk_text' => '',
        'wk_alignment' => '',
        'primary_color' => ''
    ), $atts ) );

    // ALIGNMENT
    if($wk_alignment!=""){
        $alignment=' wk-'.$wk_alignment.' ';
    }
    // COLOR
    if($primary_color==""){
        // STANDARD COLOR
        $primary_color="#575757";
    }

       $social_url=get_the_permalink();
       $social_title=get_the_title();
       $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium' );
       $social_media = $thumb['0'];
    
    return '<div class="social_share '.$alignment.'">
                 <div class="share_buttons">
                    <h6 style="color:'.$primary_color.'">'.$wk_text.'</h6>
                    <ul class="share_list">
                        <li class="i_facebook"><a style="color:'.$primary_color.'" href="http://www.facebook.com/share.php?u='.$social_url.'&title='.$social_title.'" target="_blank"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 20 20" enable-background="new 0 0 20 20" xml:space="preserve"><path fill="'.$primary_color.'" d="M14.404,6.53h-3V4.485c0-0.768,0.49-0.947,0.836-0.947c0.344,0,2.117,0,2.117,0V0.164l-2.916-0.012c-3.238,0-3.975,2.517-3.975,4.128v2.25H5.595v3.477h1.871c0,4.464,0,9.841,0,9.841h3.938c0,0,0-5.43,0-9.841h2.656L14.404,6.53z"/></svg></a></li>
                        <li class="i_twitter"><a style="color:'.$primary_color.'" href="http://twitter.com/intent/tweet?status='.__('Discover Henry glass', 'henryglass').' - '.$social_title.'+'.$social_url.'" target="_blank"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 20 20" enable-background="new 0 0 20 20" xml:space="preserve"><path fill-rule="evenodd" fill="'.$primary_color.'" clip-rule="evenodd" d="M15.936,5.158c-0.012-0.006-0.024-0.021-0.037-0.033c-0.738-0.692-1.607-1.022-2.605-0.982L13.264,4.07c0.005-0.006,0.012-0.006,0.017-0.006c0.906-0.208,1.442-0.422,1.603-0.654c0.056-0.188-0.013-0.291-0.21-0.317c-0.455,0.059-0.875,0.155-1.25,0.304c0.475-0.304,0.659-0.517,0.555-0.628c-0.469,0.013-0.975,0.253-1.504,0.726c0.197-0.337,0.271-0.519,0.223-0.551c-0.253,0.175-0.475,0.363-0.666,0.562c-0.4,0.446-0.721,0.861-0.967,1.236l-0.013,0.032c-0.635,1.029-1.078,2.063-1.343,3.118L9.61,7.973L9.591,7.991C9.209,7.512,8.753,7.111,8.206,6.787c-0.647-0.419-1.405-0.821-2.28-1.209C4.972,5.086,4.004,4.679,3.024,4.361C3.019,5.455,3.554,6.328,4.62,6.962v0.02C4.244,6.975,3.881,7.033,3.53,7.143c0.067,1.016,0.789,1.715,2.156,2.103L5.679,9.265C5.143,9.234,4.7,9.421,4.355,9.808c0.449,0.881,1.244,1.3,2.396,1.269c-0.228,0.116-0.407,0.244-0.523,0.388c-0.209,0.221-0.277,0.479-0.209,0.775c0.246,0.447,0.689,0.653,1.342,0.621l0,0l0.037,0.046c-0.006,0.006-0.012,0.013-0.018,0.02c-1.127,1.164-2.489,1.688-4.085,1.578l-0.024,0.014c-0.974-0.008-2.015-0.473-3.142-1.404c1.133,1.63,2.636,2.806,4.497,3.545c2.131,0.705,4.263,0.757,6.387,0.155h0.037c2.064-0.596,3.813-1.818,5.255-3.668c0.665-0.972,1.084-1.896,1.251-2.782c1.077,0.039,1.854-0.271,2.334-0.931l-0.013-0.02c-0.357,0.128-1.059,0.091-2.101-0.122V9.174c0-0.005,0-0.005,0.007,0c1.14-0.128,1.811-0.497,2.027-1.106c-0.796,0.312-1.578,0.316-2.354,0.027C17.316,7.021,16.811,6.043,15.936,5.158z"/></svg></a></li>
                        <li class="i_google"><a style="color:'.$primary_color.'" href="https://plus.google.com/share?url='.$social_url.'" target="_blank"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 20 20" enable-background="new 0 0 20 20" xml:space="preserve"><path fill-rule="evenodd" clip-rule="evenodd" fill="'.$primary_color.'" d="M16.423,5.823V9.29h-0.87V5.823h-3.481V4.956h3.481V1.489h0.87v3.467h3.48v0.867H16.423z M8.202,2.356C9.302,3.224,9.89,4.135,9.89,5.446c0,1.103-0.613,2.128-1.479,2.803C7.565,8.906,7.405,9.182,7.405,9.74c0,0.478,0.905,1.289,1.378,1.623c1.383,0.975,1.831,1.879,1.831,3.391c0,1.884-1.831,3.757-5.146,3.757c-2.909,0-5.362-1.179-5.362-3.062c0-1.912,2.238-3.76,5.146-3.76c0.316,0,0.607-0.013,0.908-0.013c-0.398-0.384-0.712-0.863-0.712-1.446c0-0.346,0.111-0.69,0.266-0.984c-0.158,0.011-0.32-0.008-0.486-0.008C2.84,9.237,1.24,7.5,1.24,5.402c0-2.052,2.209-3.913,4.557-3.913c1.309,0,5.236,0,5.236,0L9.861,2.356H8.202z M5.807,12.177c-2.01-0.021-3.811,1.22-3.811,2.711c0,1.523,1.451,2.792,3.461,2.792c2.826,0,3.811-1.191,3.811-2.713c0-0.184-0.023-0.362-0.064-0.538c-0.221-0.861-1.003-1.29-2.094-2.045C6.713,12.257,6.275,12.182,5.807,12.177z M8.007,5.627C7.774,3.862,6.491,2.446,5.143,2.405c-1.35-0.04-2.254,1.312-2.021,3.076C3.354,7.246,4.637,8.71,5.986,8.749C7.335,8.79,8.239,7.393,8.007,5.627z"/></svg></a></li>
                        <li class="i_pinterest"><a style="color:'.$primary_color.'" href="http://pinterest.com/pin/create/bookmarklet/?media='.$social_media.'&url='.$social_url.'&is_video=false&description='.$social_title.'" target="_blank"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 20 20" enable-background="new 0 0 20 20" xml:space="preserve"><path fill="'.$primary_color.'" d="M8.634,13.188c-0.519,2.719-1.152,5.326-3.029,6.688C5.026,15.766,6.456,12.679,7.12,9.402c-1.132-1.905,0.136-5.74,2.524-4.795c2.937,1.162-2.544,7.083,1.135,7.823c3.843,0.771,5.411-6.666,3.028-9.085c-3.441-3.493-10.02-0.08-9.211,4.921c0.197,1.223,1.46,1.594,0.505,3.28C2.897,11.059,2.24,9.322,2.325,7.005c0.136-3.792,3.407-6.446,6.687-6.813c4.149-0.464,8.043,1.523,8.58,5.425c0.606,4.405-1.872,9.175-6.309,8.833C10.081,14.355,9.576,13.76,8.634,13.188z"/></svg></a></li>
                        <li class="i_mail"><a style="color:'.$primary_color.'" href="mailto:?subject=Henry glass - '.$social_title.'&body='.__('Look at this product!', 'henryglass').' '.$social_url.'"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 20 20" enable-background="new 0 0 20 20" xml:space="preserve"><path fill-rule="evenodd" fill="'.$primary_color.'" clip-rule="evenodd" d="M19.881,15.846c0,0.855-0.467,1.568-0.979,1.568H1.093c-0.514,0-0.974-0.713-0.974-1.568V3.943c0-0.691,0.425-1.263,0.938-1.286l0.018-0.071h17.808l0.019,0.071h0.229h-0.229c0.53,0,0.979,0.571,0.979,1.286V15.846L19.881,15.846z M18.688,13.623V5.322l-5.195,4.067L18.688,13.623L18.688,13.623zM18.688,4.181H1.312c2.891,1.77,5.782,4.013,8.673,6.281C12.876,8.194,15.767,5.951,18.688,4.181L18.688,4.181z M18.688,15.504l-6.262-5.286l-1.66,1.317H9.179l-1.635-1.292l-6.232,5.261H18.688L18.688,15.504z M6.478,9.389L1.312,5.322v8.301L6.478,9.389L6.478,9.389z"/></svg></a></li>
                    </ul>
                 </div>
            </div>';
        
}




?>