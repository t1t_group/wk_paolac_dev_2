<?php


function featured_posts($type) {
    $args = array(
        'post_type' => $type,
        'posts_per_page' => -1,
        'orderby' =>'title',
        'order' => 'desc',
        'suppress_filters' => 0,
    );
    $query = new WP_Query( $args );

    $list_ids= array();
    $list_ids[__('Choose an option or leave random','webkolm')]='0';

    if ( $query->have_posts() ) :
            while ( $query->have_posts() ) : $query->the_post(); 
         // my loop code, which returns title, thumbnail, etc -- see image below
                $id_post=get_the_ID();
                $array_lingua=wpml_get_language_information($id_post);
                if($array_lingua['language_code']=="it"){
                    $nome_elenco=get_the_title().' - '.$id_post;
                    $list_ids[$nome_elenco]=$id_post;
                }

             endwhile;
         wp_reset_postdata();
         return $list_ids;
    endif;
}


function list_ids($type) {

    global $post;
    global $sitepress;
     
    //changes to the default language
    $sitepress->switch_lang( $sitepress->get_default_language() );

    $args = array(
        'posts_per_page'    =>  -1,
        'post_type'         =>  $type,
        'suppress_filters'  => false
    );

    $list_query = new WP_Query( $args );

    $list_ids= array();

    while ( $list_query->have_posts() ) : $list_query->the_post();
        $list_ids[$post->post_title]=$post->ID;
    endwhile;

    return $list_ids;

    //changes to the current language
    $sitepress->switch_lang( ICL_LANGUAGE_CODE );
}


add_action( 'vc_before_init', 'wk_bloccostorie_build' );
function wk_bloccostorie_build() {

    vc_map( array(
        "name" => __( "Blocco storia PaolaC", "webkolm" ),
        "base" => "webkolm_bloccostorie",
        "icon" => get_template_directory_uri() . "/img/VC/w.png",
        "description" => __("Crea un blocco con gallery e testo", 'webkolm'),
        "class" => "wk-bloccostorie",
        "category" => __( "webkolm addons", "webkolm"),
        "params" => array(
            array(
                "type" => "dropdown",
                "heading" => __( "Seleziona storia", "webkolm" ),
                "param_name" => "wk_bloccostorie_storia",
                "admin_label" => true,
                'value' => featured_posts('paolac-edizione'),
                "description" => __( "Seleziona una storia", "webkolm" )
            ),
            array(
                'type' => 'textfield',
                'value' => '',
                'heading' => __( "Numero storia", "webkolm" ),
                'param_name' => 'wk_bloccostorie_numero',
            ),
            array(
                'type' => 'textarea',
                'value' => '',
                'heading' => __( "Testo del blocco", "webkolm" ),
                'param_name' => 'wk_bloccostorie_testo',
                'admin_label' => true,
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Disposizione del testo", "webkolm" ),
                "param_name" => "wk_bloccostorie_disposizione",
                "value" => array( "destra", "sinistra" ),
                "description" => __( "Posizione del testo rispetto alla gallery", "webkolm" )
            ),
            array(
                        "type" => "attach_image",
                        "holder" => "img",
                        "class" => "",
                        "heading" => __( "Logo storia", "webkolm" ),
                        "param_name" => "wk_bloccostorie_image",
                        "value" => "",
                    ),
            //PARAMS GROUP
            array(
                'type' => 'param_group',
                'value' => '',
                'param_name' => 'slides',
                // Note params is mapped inside param-group:
                'params' => array(
                    array(
                        "type" => "attach_image",
                        "holder" => "img",
                        "class" => "",
                        "heading" => __( "Seleziona immagine", "webkolm" ),
                        "param_name" => "wk_slide_image",
                        "value" => "",
                    ),
                )
            )
            
        )
    ) );
}


global $javascript_append;

add_shortcode( 'webkolm_bloccostorie', 'wk_bloccostorie_func' );
function wk_bloccostorie_func( $atts, $content = null ) {
    extract( shortcode_atts( array(
        'wk_bloccostorie_storia' => '',
        'wk_bloccostorie_testo' => '',
        'wk_bloccostorie_numero' => '',
        'wk_bloccostorie_disposizione' => 'destra',
        'wk_bloccostorie_image' => '',
    ), $atts ) );
    $slides= vc_param_group_parse_atts( $atts['slides'] );


    // RANDOM ID SLIDER
    $id_slider=rand(0,99999);

    if($wk_bloccostorie_disposizione=="destra"){
        $direzionefreccia="sx";
    }else{
        $direzionefreccia="dx";
    }

    // CREO BLOCCO PAOLAC
    $output.="<div class='wk_bloccopaolac wk_bloccostorie posizione_testo".$wk_bloccostorie_disposizione."'>";

        // CREO SLIDER FLEXSLIDER
        $output.='<div id="slider-wk-'.$id_slider.'" class="module slider-interno slider_bloccopaolac '.$slider_class.'" ><ul class="slides">';
        $numslide=0;
        $random_number=rand(0,100000);

            // CICLO LE SLIDES
            foreach( $slides as $slide ){
                $images_small = wp_get_attachment_image_src($slide['wk_slide_image'], 'medium')[0];
                $images_big = wp_get_attachment_image_src($slide['wk_slide_image'], 'large')[0];

                $output.='
                    <style skip_me="1" wp_skip_me="1">
                      .slideimg-'.$id_slider.'-'.$numslide.' { background-image:url('.$images_small.');}
                      @media (min-width: 768px) {  .slideimg-'.$id_slider.'-'.$numslide.' { background-image:url('.$images_big.'); } }
                    </style>
                    <li class="slide-'.$id_slider.'-'.$numslide.' '.$li_class.' ">
                        <div class="slideimg-'.$id_slider.'-'.$numslide.' slideimg"></div>
                    </li>';

                $numslide++;
            }

        // CHIUDO SLIDER
        $output .='</ul></div>';

        // CREO BLOCCO TESTO
        $output.='<div class="testo_bloccopaolac module">';


            // CARICO TITOLO
            $logo_storia = wp_get_attachment_image_src($wk_bloccostorie_image, 'full')[0];
            $output.='<div class="titolo_bloccopaolac"><h6 class="numerazione-storia">Nr. '.$wk_bloccostorie_numero.'</h6></div><img src="'.$logo_storia.'" class="logo_storia">';

            // CARICO CONTENUTO
            $output.='<div class="contenuto_bloccopaolac"><div>'.$wk_bloccostorie_testo.'</div>';

                    // TASTO SHOP
                    $post_id = $wk_bloccostorie_storia;
                    $post = get_post($post_id); 
                    $slug = $post->post_name;
                    if(ICL_LANGUAGE_CODE=='it'){
                        $wk_link_storia='https://www.paolac.com/edizione/'.$slug.'/';
                    }else{
                        $wk_link_storia='https://www.paolac.com/en/edizione/'.$slug.'-en/';
                    }
                    

                    $output.='<a href="'.$wk_link_storia.'" class="freccia_'.$direzionefreccia.'">';

                        $output.=__('SHOP NOW', 'webkolm');
                    

                    $output.='</a>';
                

            $output.='</div>';

        // CHIUDO BLOCCO TESTO
        $output.='</div>';

    // CHIUDO BLOCCO PAOLAC
    $output.="</div>";


    // JS SLIDER INIZIALIZZAZIONE
    global $javascript_append;
    $javascript_append.='
        <script>
            $("#slider-wk-'.$id_slider.'").flexslider({
                animation: "fade",
                animationLoop: true,
                slideshowSpeed : "3000",
                 pauseOnHover: true,
                multipleKeyboard: true,
                keyboard: true,
              
            });
        </script>';


    return $output;
}
?>