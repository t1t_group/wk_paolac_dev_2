<?php

add_action( 'vc_before_init', 'wk_bloccosfondo_build' );
function wk_bloccosfondo_build() {

    vc_map( array(
        "name" => __( "Blocco con sfondo", "webkolm" ),
        "base" => "webkolm_bloccosfondo",
        "icon" => get_template_directory_uri() . "/img/VC/w.png",
        "description" => __("Crea un blocco con gallery e testo", 'webkolm'),
        "class" => "wk-slider",
        "category" => __( "webkolm addons", "webkolm"),
        "params" => array(
            array(
                'type' => 'textfield',
                'value' => '',
                'heading' => __( "Titolo da visualizzare", "webkolm" ),
                'param_name' => 'wk_bloccosfondo_titolo',
            ),
            array(
                'type' => 'textarea',
                'value' => '',
                'heading' => __( "Testo del blocco", "webkolm" ),
                'param_name' => 'wk_bloccosfondo_testo',
                'admin_label' => true,
            ),
            array(
                'type' => 'textfield',
                'value' => '',
                'heading' => __( "Link eventuale pulsante", "webkolm" ),
                'param_name' => 'wk_bloccosfondo_link',
                "description" => __( "Se questo campo è vuoto non apparirà nessun pulsante", "webkolm" )
            ),
            array(
                'type' => 'textfield',
                'value' => '',
                'heading' => __( "Titolo eventuale pulsante", "webkolm" ),
                'param_name' => 'wk_bloccosfondo_pulsante',
                "description" => __( "Se vuoto rimane la scritta generica MORE", "webkolm" )
            ),
            array(
                'type' => 'attach_image',
                "holder" => "img",
                "class" => "",
                'heading' => __( "Sfondo", "webkolm" ),
                'param_name' => 'wk_bloccosfondo_immagine',
                "description" => __( "Immagine da impostare come sfondo", "webkolm" )
            ),
            
        )
    ) );
}


add_shortcode( 'webkolm_bloccosfondo', 'wk_bloccosfondo_func' );
function wk_bloccosfondo_func( $atts, $content = null ) {
    extract( shortcode_atts( array(
        'wk_bloccosfondo_titolo' => '',
        'wk_bloccosfondo_testo' => '',
        'wk_bloccosfondo_link' => '',
        'wk_bloccosfondo_pulsante' => '',
        'wk_bloccosfondo_immagine' => '',
    ), $atts ) );
    


    // CREO BLOCCO PAOLAC
    $random_number=rand(0,100000);
    $images_small = wp_get_attachment_image_src($wk_bloccosfondo_immagine, 'medium')[0];
    $images_big = wp_get_attachment_image_src($wk_bloccosfondo_immagine, 'large')[0];
    $output.="
    <style skip_me='1' wp_skip_me='1'>
      .wk_bloccosfondo-".$random_number." { background-image:url(".$images_small.");}
      @media (min-width: 768px) {  .wk_bloccosfondo-".$random_number." { background-image:url(".$images_big."); } }
    </style>
    <div class='wk_bloccosfondo wk_bloccosfondo-".$random_number."'>
        <div class='content-bloccosfondo'>";

        // TITOLO
        $output.='<h2 class="linea">'.$wk_bloccosfondo_titolo.'</h2>';

        // SOTTOTITOLO
        $output.='<div class="testo_bloccosfondo">'.$wk_bloccosfondo_testo.'</div>';

        // PULSANTE
        if($wk_bloccosfondo_link!=""){

            $output.='<a href="'.$wk_bloccosfondo_link.'" class="wkbutton">';

            if($wk_bloccosfondo_pulsante!=""){
                $output.=$wk_bloccosfondo_pulsante;
            }else{
                $output.=_('Read more', 'webkolm');
            }

            $output.='</a>';
        }

    // CHIUDO BLOCCO
    $output.="</div></div>";

    return $output;
}
?>