<?php


/* DESIGNER CORRELATO - COMPLETATO */

function featured_posts($type) {
    $args = array(
        'post_type' => $type,
        'posts_per_page' => -1,
        'orderby' =>'title',
        'order' => 'desc',
        'suppress_filters' => 0,
    );
    $query = new WP_Query( $args );

    $list_ids= array();
    $list_ids[__('Choose an option or leave random','webkolm')]='0';

    if ( $query->have_posts() ) :
            while ( $query->have_posts() ) : $query->the_post(); 
         // my loop code, which returns title, thumbnail, etc -- see image below
                $id_post=get_the_ID();
                $array_lingua=wpml_get_language_information($id_post);
                if($array_lingua['language_code']=="it"){
                    $nome_elenco=get_the_title().' - '.$id_post;
                    $list_ids[$nome_elenco]=$id_post;
                }

             endwhile;
         wp_reset_postdata();
         return $list_ids;
    endif;
}


/*  DESIGNER CORRELATO */

add_action( 'vc_before_init', 'wk_designer_build' );
function wk_designer_build() {
    vc_map( array(
        "name" => __( "Correlated Designer", "webkolm" ),
        "base" => "wk_designer",
        "icon" => get_template_directory_uri() . "/img/VC/w.png",
        "description" => __("Correlate a designer", 'webkolm'),
        "class" => "wk-designer",
        "category" => __( "webkolm addons", "webkolm"),
         "params" => array(
            array(
                "type" => "dropdown",
                "heading" => __( "Designer correlated", "webkolm" ),
                "param_name" => "wk_correlated_designer",
                "admin_label" => true,
                'value' => featured_posts('designer'),
                "description" => __( "Choose a designer to correlate", "webkolm" )
            ),
            
        )
    ) );
}



add_shortcode( 'wk_designer', 'wk_designer_func' );
function wk_designer_func( $atts ) {
    extract( shortcode_atts( array(
        'wk_correlated_designer' =>  ''
    ), $atts ) );


    if($wk_correlated_designer==""){
        $array_designer=featured_posts('designer');
        $num_designer=count($array_designer)-1;
        $designer_random=rand(1,$num_designer);
        $blah = array_slice($array_designer, $designer_random, 1, true); // array(0 => 1)
        foreach($blah as $key => $value) {
            $wk_correlated_designer=$value;
        }
        $random="yes";
    }

    $right_id=icl_object_id($wk_correlated_designer, 'designer', false);
    $right_id_page=icl_object_id(911, 'page', false);
    $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($right_id), 'medium' );
    $url = $thumb['0'];

    $titolo_decoro=get_the_title($right_id);
    


    echo $last_word;
    $link_decoro=get_the_permalink($right_id);
    $link_generico_decori=get_the_permalink($right_id_page);

    if($random=="yes"){
          $pieces = explode(' ', $titolo_decoro);
    $titolo_decoro = array_pop($pieces);
        $output='<div class="tile scheda_fotografica">
                    <div class="retro">
                        <a href="'.$link_decoro.'" class="foto_scheda" >
                            <div class="foto_decoro" style="background-image:url('.$url.');"></div>
                            <div class="titolo_decoro">'.$titolo_decoro.'</div>
                        </a>
                        <a href="'.$link_generico_decori.'" class="button_tile">'.__('Discover the designers','henryglass').'</a>
                    </div>
                </div>';
    }else{

         $output='<div class="tile scheda_fotografica">
                <a href="'.$link_decoro.'" class="retro">
                    <div class="foto_scheda" style="background-image:url('.$url.');">
                    </div>
                    <div class="button_tile">'.$titolo_decoro.'</div>
                </a>
            </div>';
    }

    

    return $output;        
}

?>