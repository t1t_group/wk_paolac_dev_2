<?php


add_action( 'vc_before_init', 'wk_instagram_build' );
function wk_instagram_build() {

    vc_map( array(
        "name" => __( "Webkolm Instagram Row", "webkolm" ),
        "base" => "webkolm_instagram",
        "icon" => get_template_directory_uri() . "/img/VC/w.png",
        "description" => __("Instagram thumbnail", 'webkolm'),
        "class" => "wk-instagram",
        "category" => 'Webkolm Add-on',
        "params" => array(
            array(
                'type' => 'textfield',
                'value' => '',
                'heading' => __( "Hashtag", "webkolm" ),
                'param_name' => 'wk_insta_hashtag',
                "description" => __("Leave empty for profile stream, don't write the #", 'webkolm'),
            ),
            array(
                'type' => 'textfield',
                'value' => '',
                'heading' => __( "Number of photo", "webkolm" ),
                'param_name' => 'wk_insta_number',
                "description" => __("Number of pics to show in row, default 4", 'webkolm'),
            )
        )
    ) );
}


global $javascript_append;

add_shortcode( 'webkolm_instagram', 'wk_instagram_func' );
function wk_instagram_func( $atts, $content = null ) {
    extract( shortcode_atts( array(
        'wk_insta_hashtag' => '',
        'wk_insta_number' => '4',
    ), $atts ) );

    $filtro="";
    if($wk_insta_hashtag!=""){
        $filtro="
        limit: '50',
        before: function() {
             currentCount = 0;
           },
        filter: function(image) { 
            
            var shouldDisplay = (currentCount < ".$wk_insta_number.");

                  if (shouldDisplay) {
                    if (image.tags && image.tags.indexOf('".$wk_insta_hashtag."') >= 0) {
                      currentCount += 1;
                    } else {
                      shouldDisplay = false;
                    }
                  }

                  return shouldDisplay;
        }
        "; 
    }else{
        $filtro="limit: '".$wk_insta_number."',";
    }

    // CREO SLIDER FLEXSLIDER
    $output.='<div class="wk-instagram-row" id="instafeed">';


    $output.='</div>';

    // JS SLIDER INIZIALIZZAZIONE
    global $javascript_append;
    


    $javascript_append.="
        <script>
        var feed = new Instafeed({
           
            get: 'user',
            userId: '17841400551193830',
            link:false,
            accessToken:'IGQVJWbmlEY1BwQUdWczhVcDh2ZAzF4THhQeFBVTUc2YU95amJDc0MxX2QzRVpWenBjV0R1bkN3RktpT0hTUGFrYTJGa1B5ZAFlLaW5pYWZAlMGdmVHQ1OUR0MWZArRHFlcUMwUFRjM1hpTmlQSVR1dWVlUnRMdVBybXlRbjlR',
            resolution: 'low_resolution',
            template: '<a href=\"{{link}}\" target=\"_blank\" class=\"box\" style=\"background-image:url({{image}});\"></a>',
            ".$filtro."
            
                    
        });
        feed.run();
        </script>";

    return $output;
}


?>