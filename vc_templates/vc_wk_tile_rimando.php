<?php

add_action( 'vc_before_init', 'wk_tile_rimando_build' );
function wk_tile_rimando_build() {

    vc_map( array(
        "name" => __( "Riquadro di rimando con immagine", "webkolm" ),
        "base" => "webkolm_tile_rimando",
        "icon" => get_template_directory_uri() . "/img/VC/w.png",
        "description" => __("Crea un blocco con gallery e testo", 'webkolm'),
        "class" => "wk-slider",
        "category" => __( "webkolm addons", "webkolm"),
        "params" => array(
            array(
                'type' => 'textfield',
                'value' => '',
                'heading' => __( "Titolo da visualizzare", "webkolm" ),
                'param_name' => 'wk_tile_rimando_titolo',
            ),
            array(
                'type' => 'textfield',
                'value' => '',
                'heading' => __( "Link eventuale pulsante", "webkolm" ),
                'param_name' => 'wk_tile_rimando_link',
                "description" => __( "Se questo campo è vuoto non apparirà nessun pulsante", "webkolm" )
            ),
            array(
                'type' => 'textfield',
                'value' => '',
                'heading' => __( "Titolo eventuale pulsante", "webkolm" ),
                'param_name' => 'wk_tile_rimando_pulsante',
                "description" => __( "Se vuoto rimane la scritta generica SEE MORE", "webkolm" )
            ),
            array(
                'type' => 'attach_image',
                "holder" => "img",
                "class" => "",
                'heading' => __( "Sfondo", "webkolm" ),
                'param_name' => 'wk_tile_rimando_immagine',
                "description" => __( "Immagine da impostare come sfondo", "webkolm" )
            ),
            
        )
    ) );
}


add_shortcode( 'webkolm_tile_rimando', 'wk_tile_rimando_func' );
function wk_tile_rimando_func( $atts, $content = null ) {
    extract( shortcode_atts( array(
        'wk_tile_rimando_titolo' => '',
        'wk_tile_rimando_testo' => '',
        'wk_tile_rimando_link' => '',
        'wk_tile_rimando_pulsante' => '',
        'wk_tile_rimando_immagine' => '',
    ), $atts ) );
    


    // CREO BLOCCO PAOLAC
    $random_number=rand(0,100000);
    $images_small = wp_get_attachment_image_src($wk_tile_rimando_immagine, 'medium')[0];
    $images_big = wp_get_attachment_image_src($wk_tile_rimando_immagine, 'large')[0];

    if($wk_tile_rimando_link!=""){ 
        $link = ' href="'.$wk_tile_rimando_link.'" ';
    }

    $output.='
    <a '.$link.'>
    <div class="wk_tile_rimando wk_tile_rimando-'.$random_number.'">
        <style skip_me="1" wp_skip_me="1">
          .wk_tile_rimando-'.$random_number.' .wk_tile_bg { background-image:url('.$images_small.');}
          @media (min-width: 768px) {  .wk_tile_rimando-'.$random_number.' .wk_tile_bg { background-image:url('.$images_big.'); } }
        </style>
        
        <div class="wk_tile_bg"></div>
        <div class="content-tile_rimando">';

            // TITOLO
            $output.='<h3>'.$wk_tile_rimando_titolo.'</h3>';

            // PULSANTE
            if($wk_tile_rimando_link!=""){

                $output.='<a href="'.$wk_tile_rimando_link.'" class="wk_pulsante_tile">';

                if($wk_tile_rimando_pulsante!=""){
                    $output.=$wk_tile_rimando_pulsante;
                }else{
                    $output.= __('Read more', 'webkolm');
                }

                $output.='</a>';
            }

        // CHIUDO BLOCCO
        $output.='</div></div></a>';

    return $output;
}
?>