<?php

add_action( 'vc_before_init', 'wk_spaziatore_build' );
function wk_spaziatore_build() {

    vc_map( array(
        "name" => __( "Blocco Spaziatore Webkolm", "webkolm" ),
        "base" => "webkolm_spaziatore",
        "icon" => get_template_directory_uri() . "/img/VC/w.png",
        "description" => __("Crea un blocco con gallery e testo", 'webkolm'),
        "class" => "wk-slider",
        "category" => __( "webkolm addons", "webkolm"),
        "params" => array(
            array(
                'type' => 'textfield',
                'value' => '50',
                'heading' => __( "Altezza dello spaziatore", "webkolm" ),
                'param_name' => 'wk_spaziatore_altezza',
            ),
        )
    ) );
}


add_shortcode( 'webkolm_spaziatore', 'wk_spaziatore_func' );
function wk_spaziatore_func( $atts, $content = null ) {
    extract( shortcode_atts( array(
        'wk_spaziatore_altezza' => '77px',
    ), $atts ) );
    

    if(!stripos(trim($wk_spaziatore_altezza," "), 'px')){
        $wk_spaziatore_altezza .= 'px';
    }

    // CREO BLOCCO PAOLAC
    $output.='<div class="wk_spaziatore"></div>';

    return $output;
}
?>