<?php

add_action( 'vc_before_init', 'wk_slider_build' );
function wk_slider_build() {

    vc_map( array(
        "name" => __( "Webkolm Image Slider", "webkolm" ),
        "base" => "webkolm_slider",
        "icon" => get_template_directory_uri() . "/img/VC/w.png",
        "description" => __("Create an image slider with text", 'webkolm'),
        "class" => "wk-slider",
        "category" => __( "webkolm addons", "webkolm"),
        "params" => array(
            array(
                "type" => "dropdown",
                "heading" => __( "Dimensioni slider", "webkolm" ),
                "param_name" => "wk_slider_dimension",
                "value" => array( "orizzontale", "verticale", "quadrato", "full-page" ),
                "description" => __( "Scegli la proporzione dello slider", "webkolm" )
            ),
            array(
                "type" => "checkbox",
                "heading" => __( "Only Mobile", "webkolm" ),
                "param_name" => "wk_onlymobile",
                'value' => '',
                "description" => __( "Visualizzare solo da mobile", "webkolm" )
            ),
            //PARAMS GROUP
            array(
                'type' => 'param_group',
                'value' => '',
                'param_name' => 'slides',
                // Note params is mapped inside param-group:
                'params' => array(
                    /*
                    array(
                        'type' => 'textfield',
                        'value' => '',
                        'heading' => __( "Titolo", "webkolm" ),
                        'param_name' => 'wk_slide_title',
                    ),
                    array(
                        'type' => 'textfield',
                        'value' => '',
                        'heading' => __( "Link", "webkolm" ),
                        'param_name' => 'wk_slide_link',
                        "description" => __( "Inserisci un url con http://, lascia vuoto se non vuoi linkare la slide" , "webkolm" ),
                    ),
                    */
                    array(
                        "type" => "attach_image",
                        "holder" => "img",
                        "class" => "",
                        "heading" => __( "Seleziona immagine", "webkolm" ),
                        "param_name" => "wk_slide_image",
                        "value" => "",
                    ),
                    /*
                    array(
                        "type" => "attach_image",
                        "holder" => "img",
                        "class" => "",
                        "heading" => __( "Seleziona immagine per la versione mobile", "webkolm" ),
                        "param_name" => "wk_slide_image_mobile",
                        "value" => "",
                    ),
                    array(
                        "type" => "dropdown",
                        "heading" => __( "Posìzione del titolo" ),
                        "param_name" => "wk_slide_text_position",
                        "value" => array( "top-left", "top-center", "top-right", "center-left", "center-center", "center-right", "bottom-left", "bottom-center", "bottom-right" ),
                        "description" => __( "Seleziona la posizione del titolo", "webkolm" )
                    ),
                    array(
                        "type" => "dropdown",
                        "heading" => __( "Colore titolo", "webkolm" ),
                        "param_name" => "wk_slider_colour",
                        "value" => array( "white", "black"),
                        "description" => __( "Seleziona il colore del titolo della slide", "webkolm" )
                    ), */
                )
            )
            
        )
    ) );
}


global $javascript_append;

add_shortcode( 'webkolm_slider', 'wk_slider_func' );
function wk_slider_func( $atts, $content = null ) {
    extract( shortcode_atts( array(
        'wk_slider_dimension' => 'orizzontale',
        'wk_onlymobile' => '',
    ), $atts ) );


    $slides= vc_param_group_parse_atts( $atts['slides'] );

    // TIPOLOGIA DELLO SLIDER: wk_full-page, wk_horizontal, wk_vertical, wk_squared
    $slider_class=" wk_".$wk_slider_dimension." ";


    if($wk_onlymobile){ $slider_class.=" onlymobile "; }

    // RANDOM ID SLIDER
    $id_slider=rand(0,99999);

    
    $output.='<div id="slider-wk-'.$id_slider.'" class="webkolm-slider slider-interno '.$slider_class.'" ><ul class="slides">';

    // CREO SLIDER FLEXSLIDER
   
     
    $numslide=0;
    $random_number=rand(0,100000);


    // CICLO LE SLIDES
    foreach( $slides as $slide ){
        $images_small = wp_get_attachment_image_src($slide['wk_slide_image'], 'medium')[0];
        $images_big = wp_get_attachment_image_src($slide['wk_slide_image'], 'large')[0];
        $images = wp_get_attachment_image_src($slide['wk_slide_image'], 'full')[0];


        /*
        if($wk_slider_dimension=="full-page"){


            // CHECK SLIDE TITLE
            $slide_title = "";
            if($slide['wk_slide_title'] != ""){
                $slide_title = '
                <div class="testo_slide '.$slide['wk_slide_text_position'].'">
                    <h1 class="slide-title slider-title '.$color_class.'">'. $slide['wk_slide_title'] .'</h1>
                </div>';
            }


            // SE C'E LINK ATTIVO HREF SU SLIDE
            if($slide['wk_slide_link']!=""){
                $link_slide=' href="'.$slide['wk_slide_link'].'" ';
            }else{
                $link_slide="";
            }


            // FRECCIA SCROLL-DOWN SLIDER
            $scroll_down = "";
            if($slide['wk_slider_colour'] == 'white'){
                $scroll_down .= '<div class="scroll-home nomobile trigger big-home-buttom"><img src="https://www.paolac.com/wp-content/themes/paolac/img/freccia_dx_bianca.png"></div>';
            }else{
                $scroll_down .= '<div class="scroll-home nomobile trigger big-home-buttom"><img src="https://www.paolac.com/wp-content/themes/paolac/img/freccia_dx_nera.png"></div>';
            }

            
            // NON ATTIVO BLUEIMGALLERY SE FULL PAGE
            $output.='
            <li class="slide-'.$id_slider.'-'.$numslide.' '.$li_class.'">
                <style>
                  .slideimg-'.$id_slider.'-'.$numslide.' { background-image:url('.$images_small.');}
                  @media (min-width: 768px) {  .slide-'.$id_slider.'-'.$numslide.' { background-image:url('.$images_big.'); } }
                  @media (min-width: 1400px) {  .slide-'.$id_slider.'-'.$numslide.' { background-image:url('.$images.'); } }
                </style>
                '. $slide_title .'
                '. $scroll_down .'
            </li>';

        } else {
            */

            $output.='
            <li class="slide-'.$id_slider.'-'.$numslide.' '.$li_class.' ">
                <style skip_me="1" wp_skip_me="1">
                  .slide-'.$id_slider.'-'.$numslide.' { background-image:url('.$images_small.');}
                  @media (min-width: 768px) {  .slide-'.$id_slider.'-'.$numslide.' { background-image:url("'.$images_big.'""); } }
                  @media (min-width: 1800px) {  .slide-'.$id_slider.'-'.$numslide.' { background-image:url("'.$images.'""); } }
                </style>
            </li>';

        //}

        $numslide++;
    }

    // CHIUDO SLIDER
    $output .='</ul>
    </div>';
    



    // JS SLIDER INIZIALIZZAZIONE
    global $javascript_append;
    $javascript_append.='
        <script>
            $("#slider-wk-'.$id_slider.'").flexslider({
                animation: "fade",
                animationLoop: true,
                slideshowSpeed : "2500",
                 pauseOnHover: true,
                multipleKeyboard: true,
                keyboard: true,
              
            });
        </script>';


    return $output;
}
?>