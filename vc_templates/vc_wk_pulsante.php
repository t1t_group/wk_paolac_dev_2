<?php

add_action( 'vc_before_init', 'wk_pulsante_build' );
function wk_pulsante_build() {

    vc_map( array(
        "name" => __( "WK Pulsante", "webkolm" ),
        "base" => "webkolm_pulsante",
        "icon" => get_template_directory_uri() . "/img/VC/w.png",
        "description" => __("Crea un pulsante con freccia", 'webkolm'),
        "class" => "wk-pulsante",
        "category" => __( "webkolm addons", "webkolm"),
        "params" => array(
            array(
                'type' => 'textfield',
                'value' => '',
                'heading' => __( "Testo del pulsante", "webkolm" ),
                'param_name' => 'wk_pulsante_testo',
            ),
            array(
                'type' => 'textfield',
                'value' => '',
                'heading' => __( "Link pulsante", "webkolm" ),
                'param_name' => 'wk_pulsante_link',
                "description" => __( "Se questo campo è vuoto non apparirà nessun pulsante", "webkolm" )
            ),
            array(
                "type" => "checkbox",
                "heading" => __( "Apri in una nuova scheda", "webkolm" ),
                "param_name" => "wk_target",
                'value' => '',
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Allineamento del pulsante", "webkolm" ),
                "param_name" => "wk_direction",
                "value" => array("center", "left", "right"),
                "description" => __( "Scegli la proporzione del pulsante", "webkolm" )
            ),
        )
    ) );
}


add_shortcode( 'webkolm_pulsante', 'wk_pulsante_func' );
function wk_pulsante_func( $atts, $content = null ) {
    extract( shortcode_atts( array(
        'wk_pulsante_testo' => '',
        'wk_pulsante_link' => '',
        'wk_target' => '',
        'wk_direction' => 'center'
    ), $atts ) );
    
    $output = "";
    if(($wk_pulsante_link!="") && ($wk_pulsante_testo!="")){ 

        if(!empty($wk_target)){
            $target = ' target="_blank" ';
        }

        $output='
        <div class="wk_pulsante_container align-'.$wk_direction.'">
            <a href="'. $wk_pulsante_link .'" class="freccia_dx" '.$target.'>'. $wk_pulsante_testo .'</a>
        </div>';
    }

    return $output;
}
?>