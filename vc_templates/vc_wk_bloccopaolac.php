<?php

add_action( 'vc_before_init', 'wk_bloccopaolac_build' );
function wk_bloccopaolac_build() {

    vc_map( array(
        "name" => __( "Testo e immagini PaolaC", "webkolm" ),
        "base" => "webkolm_bloccopaolac",
        "icon" => get_template_directory_uri() . "/img/VC/w.png",
        "description" => __("Crea un blocco con gallery e testo", 'webkolm'),
        "class" => "wk-bloccopaolac",
        "category" => __( "webkolm addons", "webkolm"),
        "params" => array(
            array(
                'type' => 'textfield',
                'value' => '',
                'heading' => __( "Titolo da visualizzare", "webkolm" ),
                'param_name' => 'wk_bloccopaolac_titolo',
            ),
            array(
                'type' => 'textarea',
                'value' => '',
                'heading' => __( "Testo del blocco", "webkolm" ),
                'param_name' => 'wk_bloccopaolac_testo',
                'admin_label' => true,
            ),
            array(
                'type' => 'textfield',
                'value' => '',
                'heading' => __( "Link eventuale pulsante", "webkolm" ),
                'param_name' => 'wk_bloccopaolac_link',
                "description" => __( "Se questo campo è vuoto non apparirà nessun pulsante", "webkolm" )
            ),
            array(
                'type' => 'textfield',
                'value' => '',
                'heading' => __( "Titolo eventuale pulsante", "webkolm" ),
                'param_name' => 'wk_bloccopaolac_pulsante',
                "description" => __( "Se vuoto rimane la scritta generica MORE", "webkolm" )
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Disposizione del testo", "webkolm" ),
                "param_name" => "wk_bloccopaolac_disposizione",
                "value" => array( "sotto", "destra", "sinistra" ),
                "description" => __( "Posizione del testo rispetto alla gallery", "webkolm" )
            ),
            array(
                "type" => "attach_image",
                "holder" => "img",
                "class" => "",
                "heading" => __( "Immagine sopra il titolo", "webkolm" ),
                "param_name" => "wk_header_image",
                "value" => "",
            ),
            //PARAMS GROUP
            array(
                'type' => 'param_group',
                'value' => '',
                'param_name' => 'slides',
                // Note params is mapped inside param-group:
                'params' => array(
                    array(
                        "type" => "attach_image",
                        "holder" => "img",
                        "class" => "",
                        "heading" => __( "Select image", "webkolm" ),
                        "param_name" => "wk_slide_image",
                        "value" => "",
                    ),
                )
            )
            
        )
    ) );
}


global $javascript_append;

add_shortcode( 'webkolm_bloccopaolac', 'wk_bloccopaolac_func' );
function wk_bloccopaolac_func( $atts, $content = null ) {
    extract( shortcode_atts( array(
        'wk_bloccopaolac_titolo' => '',
        'wk_bloccopaolac_testo' => '',
        'wk_bloccopaolac_link' => '',
        'wk_bloccopaolac_pulsante' => '',
        'wk_bloccopaolac_disposizione' => 'sotto',
        'wk_header_image' => ''
    ), $atts ) );
    $slides= vc_param_group_parse_atts( $atts['slides'] );


    // RANDOM ID SLIDER
    $id_slider=rand(0,99999);


    // CREO BLOCCO PAOLAC
    $output.="<div class='wk_bloccopaolac posizione_testo".$wk_bloccopaolac_disposizione."'>";

        // CREO SLIDER FLEXSLIDER
        $output.='<div id="slider-wk-'.$id_slider.'" class="slider-interno slider_bloccopaolac '.$slider_class.' module" ><ul class="slides">';
        $numslide=0;
        $random_number=rand(0,100000);

            // CICLO LE SLIDES
            foreach( $slides as $slide ){
                $images_small = wp_get_attachment_image_src($slide['wk_slide_image'], 'medium')[0];
                $images_big = wp_get_attachment_image_src($slide['wk_slide_image'], 'large')[0];

                $output.='
                    <style skip_me="1" wp_skip_me="1">
                      .slideimg-'.$id_slider.'-'.$numslide.' { background-image:url('.$images_small.');}
                      @media (min-width: 768px) {  .slideimg-'.$id_slider.'-'.$numslide.' { background-image:url('.$images_big.'); } }
                    </style>
                    <li class="slide-'.$id_slider.'-'.$numslide.' '.$li_class.' ">
                        <div class="slideimg-'.$id_slider.'-'.$numslide.' slideimg"></div>
                    </li>';

                $numslide++;
            }

        // CHIUDO SLIDER
        $output .='</ul></div>';

        // CREO BLOCCO TESTO
        $output.='<div class="testo_bloccopaolac module">';

            if(!empty($wk_header_image)){
                $wk_img_header = wp_get_attachment_image_src($wk_header_image, 'medium')[0];
                $output.= '<div class="header_img_bloccopaolac"><img src="'.$wk_img_header.'" /></div>';
            }
            

            // CARICO TITOLO
            $output.='<div class="titolo_bloccopaolac"><h2 class="linea">'.$wk_bloccopaolac_titolo.'</h2></div>';

            // CARICO CONTENUTO
            $output.='<div class="contenuto_bloccopaolac"><div>'.$wk_bloccopaolac_testo.'</div>';

                // CARICO TASTO
                if($wk_bloccopaolac_link!=""){

                    if($wk_bloccopaolac_disposizione=="destra"){
                        $classe_pulsante="freccia_sx";
                    }else{
                        $classe_pulsante="freccia_dx";
                    }

                    $output.='<a href="'.$wk_bloccopaolac_link.'" class="'.$classe_pulsante.'">';

                    if($wk_bloccopaolac_pulsante!=""){
                        $output.=$wk_bloccopaolac_pulsante;
                    }else{
                        $output.=_('Read more', 'webkolm');
                    }

                    $output.='</a>';
                }

            $output.='</div>';

        // CHIUDO BLOCCO TESTO
        $output.='</div>';

    // CHIUDO BLOCCO PAOLAC
    $output.="</div>";


    // JS SLIDER INIZIALIZZAZIONE
    global $javascript_append;
    $javascript_append.='
        <script>
            $("#slider-wk-'.$id_slider.'").flexslider({
                animation: "fade",
                animationLoop: true,
                slideshowSpeed : "3000",
                 pauseOnHover: true,
                multipleKeyboard: true,
                keyboard: true,
              
            });
        </script>';


    return $output;
}
?>