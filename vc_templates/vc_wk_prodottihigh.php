<?php
global $woocommerce;

add_action( 'vc_before_init', 'wk_prodottihigh_build' );
function wk_prodottihigh_build() {

    vc_map( array(
        "name" => __( "Prodotti in evidenza", "webkolm" ),
        "base" => "webkolm_prodottihigh",
        "icon" => get_template_directory_uri() . "/img/VC/w.png",
        "description" => __("Blocco con tre prodotti tra quelli in evidenza", 'webkolm'),
        "class" => "wk-prodottihigh",
        "category" => __( "webkolm addons", "webkolm"), 
    ) );
}


add_shortcode( 'webkolm_prodottihigh', 'wk_prodottihigh_func' );
function wk_prodottihigh_func( $atts, $content = null ) {
  
    
    $titolo=__("Highlight", "paolac");
    

    $output.='
    <div class="row" id="trigger">
        <div class="col-12">
            <h3 class="linea">'.$titolo.'</h3>
        </div>
    </div>';

    $wkyour_query = new WP_Query( array(
        'post_type' => 'product',
        'post_status' => 'publish',
        'posts_per_page' => 8,
        'meta_query' => array(
              array(
               'key' => 'wpcf-evidenzia-in-homepage',
               'value' => 1,
                  'compare' => '=',
             ),              
         ),
        'orderby'=>'rand',
        'order'=>'ASC'
    ));


    $output.='
    <div class="highlights-container">
      <div class="highlights-row" id="target">';

        while ( $wkyour_query->have_posts() ) : $wkyour_query->the_post(); global $product;
            
            $custom_field = get_post_custom($post->ID);
            $url_prod=get_permalink();
            $sottotitolo_prod=get_post_meta( get_the_ID(), 'wpcf-sottotitolo' );
            $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium' );
            $url = $thumb['0'];

            //$prezzo = ;

            $output.='<div class="prodotto-correlato highlights-item module">
              <a href="'. $url_prod .'">
                <div class="prodotto_mask mask_outline" style="background-image:url(\''. $url .'\');">';
                if ( has_term( 'novita', 'utilizzo' ) || has_term( 'novita-new', 'product_cat' ) ) {
                    $output.='<span class="new_flag">NEW</span>';
                }
                if ( has_term( 'exclusive-deal', 'utilizzo' ) ) {
                  $output.='<span class="offerta-esclusiva_flag">' . __('Exclusive deal', 'webkolm') . '</span>';
                }
            $output.='</div>
                <h4 class="prodotto_titolo">'.get_the_title().'</h4><span class="prodotto_sottotitolo">'.$sottotitolo_prod[0].'</span><br/><span class="prodotto_prezzo">'.$product->get_price_html().'</span>
              </a>
            </div>';
              
          endwhile;
          // reset post data (important!)
          wp_reset_postdata();
      $output.='
      </div>
    </div>';


/*
    $output.='
    <div class="row">
      <div class="wpb_column vc_column_container vc_column-inner  vc_col-sm-12">
        <h2>'.$titolo.'</h2>
        <div class="row nuovi_prodotti products products_home">';

           //PRENDO 3 PRODOTTI RANDOM TRA QUELLI SEGNALATI "IN EVIDENZA"

              $your_query = new WP_Query( array(
               'post_type' => array( 'product' ),                
              'posts_per_page' => 4,
              'post_status' => 'publish',
             
               'meta_query' => array(
                    array(
                     'key' => 'wpcf-evidenzia-in-homepage',
                     'value' => 1,
                        'compare' => '=',
                   ),

                                   
               ),
                'orderby'=>'rand',
                'order'=>'ASC'
            ));


            
            $numero_prodotto=1;
            while ( $your_query->have_posts() ) : $your_query->the_post();
                
                $url_prod=get_permalink();
                $sottotitolo_prod=get_post_meta( get_the_ID(), 'wpcf-sottotitolo' );

                //$sottotitolo=types_render_field("sottotitolo");
                //$sottotitolo_prod=strip_tags(types_render_field("sottotitolo"));
                $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium' );
                $url = $thumb['0'];
                $product = new WC_Product(get_the_ID()); 
                $prezzo=$product->get_price_html();
                $titolo=get_the_title();


                if($numero_prodotto=="4"){ $classe_mobile="onlymobile"; }else{ $classe_mobile=""; }
                if(has_term('','edizione')){ $classe_edition="editions";}else{ $classe_edition=""; }
                
                $output.='<div class="col-4 module '.$classe_mobile.'">
                  <a href="'.$url_prod.'" class="  '.$classe_edition.' ">';
                    $output.='<div class="mask_outline"><img src="'.$url.'"/></div><h4 class="prodotto_titolo">'.$titolo.'</h4><span class="prodotto_sottotitolo">'.$sottotitolo_prod[0].'</span><span class="prodotto_prezzo">'.$prezzo.'</span></a></div>';

                $numero_prodotto++;
            endwhile;
            // reset post data (important!)
            wp_reset_postdata();
           
        $output.='</div>
      </div>
    </div>'; */


    return $output;
}

?>