<?php
global $woocommerce;
global $product;

/*
$attributes = array(
    'type' => 'checkbox',
    'heading' => "<br/>Hide from mobile",
    'param_name' => 'wk_nomobile',
    'value' => "",
    'weight' =>4,
    'description' => __( "Select to hide this content from mobile version", "webkolm" )
);
vc_add_param( 'vc_row', $attributes ); 
vc_add_param( 'vc_column', $attributes ); 


*/

$attributes = array(
    'type' => 'checkbox',
    'heading' => "<br/>Full width image",
    'param_name' => 'wk_full_width',
    'value' => "",
    'weight' =>4,
    'description' => __( "Select to getting 100% width to the image", "webkolm" )
);
vc_add_param( 'vc_single_image', $attributes );


/* ADD CLASS TO VC ELEMENT */

$attributes = array(
    "type" => "checkbox",
    'heading' => "<br/>Larghezza della riga fissa",
    'param_name' => 'wk_fixrow',
    'value' => "",
    'weight' =>2,
    'description' => __( "La riga si estenderà al massimo fino a 1380px e sarà centrata", "webkolm" )
);
vc_add_param( 'vc_row', $attributes ); 

$attributes = array(
    "type" => "checkbox",
    'heading' => "<br/>Larghezza della riga fissa",
    'param_name' => 'wk_fixrow',
    'value' => "",
    'weight' =>2,
    'description' => __( "La riga si estenderà al massimo fino a 1380px e sarà centrata", "webkolm" )
);
vc_add_param( 'vc_row_inner', $attributes ); 


include('vc_templates/vc_wk_bloccopaolac.php');
include('vc_templates/vc_wk_prodottihigh.php');
include('vc_templates/vc_wk_bloccosfondo.php');
include('vc_templates/vc_wk_bloccostorie.php');
include('vc_templates/vc_wk_blocconews.php');
include('vc_templates/vc_wk_slider.php');
include('vc_templates/vc_wk_slider_home.php');
include('vc_templates/vc_wk_spaziatore.php');
include('vc_templates/vc_wk_tile_rimando.php');
include('vc_templates/vc_wk_pulsante.php');
include('vc_templates/vc_wk_instagram.php');

?>