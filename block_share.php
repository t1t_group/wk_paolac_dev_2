<?php 
   $social_url=get_the_permalink();
   $social_title=get_the_title();
   $social_img=get_the_title();
   $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium' );
	$social_media = $thumb['0'];
 ?>
 <div class="share_buttons">
 	<span class="share_title"><?php _e('share', 'paolac'); ?></span>
 	<ul class="share_list">
 		<li class="i_facebook"><a href="http://www.facebook.com/share.php?u=<?= $social_url ?>&title=<?= $social_title ?>" target="_blank"><?php get_template_part('img/icon_facebook.svg');?></a></li>
 		<li class="i_twitter"><a href="http://twitter.com/intent/tweet?status=<?php _e('Discover', 'paolac'); ?> PAOLA C - <?= $social_title ?>+<?= $social_url ?>" target="_blank"><?php get_template_part('img/icon_twitter.svg');?></a></li>
 		<li class="i_pinterest"><a href="http://pinterest.com/pin/create/bookmarklet/?media=<?= $social_media ?>&url=<?= $social_url ?>&is_video=false&description=<?=$social_title?>" target="_blank"><?php get_template_part('img/icon_pinterest.svg');?></a></li>
 		<li class="i_mail"><a href="mailto:?subject=Paola C - <?= $social_title ?>&body=<?php _e('Look at this product!');?> <?= $social_url ?>"><?php get_template_part('img/icon_mail.svg');?></a></li>
 	</ul>
 </div>