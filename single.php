<?php

 if($_GET['rel']!='tab'){
  get_header(); } else { ?>

<script>
     $(".overlay-close").click(function(){
          $("body").toggleClass("bloccoscroll");
          $("html").toggleClass("bloccoscroll");
          $("#singolo-overlay").removeClass("open");
          $("#singolo-overlay").removeClass("loadingOff");
          $(".wrapper-singolo").fadeOut();
          
          var paginaold="<?php echo $_POST['paginaold'];?>"
          window.history.pushState({path:paginaold},'',paginaold);
    });
</script>

<?php } ?>


<?php if(have_posts()) : while(have_posts()) : the_post(); 

    $thumb_m = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );
    $url_m = $thumb_m['0'];
  ?>
  <div class="wrapper-singolo <?php if($_GET['rel']!='tab'){ echo "pagina-singola";}?>" >
    <span class="overlay-close"></span>

    <div class="row">
      <div class="col-2"></div>
      <div class="col-8 module">
      	<div class="slider-news <?php if(has_category('press')) { echo "slider-press"; } ?>">
        <ul class="slides">
        	<li style="  background-image:url(<?php echo $url_m; ?>); " >
              <img src="<?php  echo $url_m; ?>"  alt="<?php the_title() ?>" title="<?php the_title(); ?>" class="onlymobile imgslide"/>
        	</li>
        	<?php 
        		// SLIDER
        		$gallery=types_render_field("galleria", array("raw"=>"true"));
        		    
        		
        		// STAMPO LA GALLERY SE PRESENTE
        		
        		if($gallery!=""){
		          $post_content = $gallery;
		          preg_match('/\[gallery.*ids=.(.*).\]/', $post_content, $ids);
		          $array_id = explode(",", $ids[1]);

		          
		          foreach ($array_id as &$item) {
		              $url_thumb=wp_get_attachment_image_src( $item, 'medium' );
		              $didascalia=wp_get_attachment($item);
		              $didascalia=$didascalia['caption'];
		              ?>
		              <li style="background-image:url(<?php echo $url_thumb[0]; ?>); ">
		                <img src="<?php echo $url_thumb[0]; ?>" class="onlymobile slideimg" />
		              </li>
		              <?php 
		          }
        		}
        	?>
        </ul>
        </div>
      </div>
    </div>

    <?php if(has_category('eventi') || has_category('events') ) { ?><div class="row">
      <div class="col-2"></div>
      <div class="col-8 content-news module">
      	<?php /*if(!has_category('press')) { ?><h3><?php the_title();?></h3><?php }*/ ?>
      	<h3><?php the_title();?></h3>
        <div class="padding10">
        	<?php
        		$periodo=types_render_field("periodo", array("row" => "true"));
            $sottotitolo=types_render_field("sottotitolo", array("row" => "true"));
        		?>
        		<span class="periodo_single"><?= $periodo ?><br/><?= $sottotitolo ?></span>
        	<?php 

        	the_content();
          ?></div><?php
        	$allegato=types_render_field("allegato", array("raw"=>"true"));
        	if($allegato!=""){
        		?><a href="<?= $allegato ?>" class="centrato"><?php _e('Download PDF','paolac');?></a><?php

        	}
        	?>
      </div>
    </div>
    <?php }?>
    <?php if(has_category('press')) { ?><div class="row">
        
          <?php 

          $allegato=types_render_field("allegato", array("raw"=>"true"));
          if($allegato!=""){
            ?><a href="<?= $allegato ?>" class="centrato" target="_blank" style="margin-top:0;"><?php _e('Download PDF','paolac');?></a><?php

          }
          ?>
    </div>
    <?php }?>

  </div>
  <?php if($_GET['rel']!='tab'){  }else { ?><div id="loader-singolo"></div><?php } ?>

<?php endwhile; endif; ?>


<?php if($_GET['rel']!='tab'){ get_footer(); }else{


	?>
	<script>
	if($(".slider-news").length > 0){
		$('.slider-news').flexslider({
		    animation: "fade",
		    animationLoop: true,
		    slideshowSpeed : "4000",
		     pauseOnHover: true,
		    multipleKeyboard: true,
		    keyboard: true
		});
	}


  // DETERMINO ALTEZZA DEL BOX 
  function marginBox(){
    $altezza_box=$(".wrapper-singolo").outerHeight()+40;
    $altezza_page=$(window).height();

    if($altezza_box>=$altezza_page){
      $(".wrapper-singolo").addClass("marginato");
    }else{
      $(".wrapper-singolo").removeClass("marginato");
    }
  }
  marginBox()


  /* MANAGE RESIZE */

  if(is_touch_device()){
    $( window ).on( "orientationchange", function( event ) {
      marginBox()

    });
  }else{
    $(window).on('resize', function(e) {

      fissaFooter();
      marginBox()
    });
  }

	</script>
	<?php
} ?>
