<div class="row">
	<div class="col-12">
		<!-- BREADCRUMBS -->
		<ul class="breadcrumb-shop">

			<?php 

				if(has_term('','edizione')){ 

					// LINK TAXONOMY EDITIONS
					$tassonomia=get_permalink( icl_object_id(1358, 'page', false) );
					echo '<li><a href="'.$tassonomia.'">Editions</a></li>';
					
					// LINK SINGLE TERMS
					$terms = get_the_terms( get_the_ID(), 'edizione' ); 
				    foreach ( $terms as $term ) {
				        $nome = $term->name;
				        $url = get_term_link( $term );
				    }
					echo '<li><a href="'.$url.'">'.$nome.'</a></li>';

					

				}/*
				else if(has_term('','collezione')){ 
					// LINK TAXONOMY COLLEZIONE
					$tassonomia=get_permalink( icl_object_id(1360, 'page', false) );
					echo '<li><a href="'.$tassonomia.'">Collection</a></li>';
					// LINK SINGLE TERMS
					$terms = get_the_terms( get_the_ID(), 'collezione' ); 
				    foreach ( $terms as $term ) {
				        $nome = $term->name;
				        $url = get_term_link( $term );
				    }
					echo '<li><a href="'.$url.'">'.$nome.'</a></li>';

				}*/
				else {
					?><li><a href="<?php echo get_permalink( woocommerce_get_page_id( 'shop' ) );?>"><?php _e('Collection', 'paolac');?></a></li><?php
					// LINK SINGLE TERMS
					$terms = get_the_terms( get_the_ID(), 'product_cat' ); 
				    foreach ( $terms as $term ) {
				        $nome = $term->name;
				        $url = get_term_link( $term );
				    }
				    if(ICL_LANGUAGE_CODE=="it"){
				    	$nome=substr($nome, 0, strpos($nome, ' /'));
				    }
				    else { 
				    	$nome=strstr($nome, ' /'); 
				    	$nome=substr($nome, 2); 
				    }
					echo '<li><a href="'.$url.'">'.$nome.'</a></li>';

				}
			?>
			<li><?php the_title ()?></li>
		</ul>

	</div>
</div>