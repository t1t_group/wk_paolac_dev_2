<?php
/**
 * Template Name: Designer
 *
 */
get_header(); ?>
<div class="wrap">
	<?php if (have_posts()) : ?>
		<?php while (have_posts()) : the_post(); ?>
			<div class="page-contatti">
				<div class="row">
					<div class="col-12">
						<h2 class="linea"><?php the_title();?></h2>
					</div>
				</div>
				<div class="row grid-designer">
					<!--<div class="grid-sizer" style="display:none;">grid sizer</div>-->
					<?php
						// CARICO TUTTI I DESIGNER

						$your_query = new WP_Query( array(
						    'post_type' => array( 'designers'),				
						    'posts_per_page' => -1,
							'post_status' => 'publish',
							'orderby' => 'menu_order',
		                	'order'=>'ASC'
					     ));

						while ( $your_query->have_posts() ) : $your_query->the_post();
							$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );
		              		$url = $thumb['0'];

							?>

								<div href="<?php echo get_the_permalink();?>" class="col-4 module item_designer grid-item item-overlay">
									<img src="<?php echo $url;?>"/>
									<h4 class="prodotto_titolo"><?php the_title();?></h4>
								</div>


							<?php
						
						endwhile;
						// reset post data (important!)
						wp_reset_postdata();

						?>

					
				</div>
			</div>
		<?php endwhile; ?>
	<?php endif; ?>
</div>
<?php get_footer(); ?>