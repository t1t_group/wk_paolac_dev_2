<?php 
get_header(); ?>
<div class="wrap">
	<div class="page-contatti">
		<div class="row">
			<div class="col-12">
				<h2 class="titolo_categoria"><?php single_cat_title( '', true ); ?></h2>
			</div>
		</div>
		<?php 
		// RICAVO CATEGORIA

		$cat_title = single_cat_title( '', false );
		$cat_id = get_cat_ID($cat_title);
		$untranslated = icl_object_id($cat_id, 'category');
		if($untranslated==8 || $untranslated==9){ 
			$class="press-grid";
		}
		elseif ($untranslated==1 || $untranslated==10){
			$class="event-grid";
		}

		$col2=1;
		$col3=1;
		$col4=1;
		?>
		<div class="row <?= $class ?>">
			<?php if (have_posts()) : ?>
				<?php while (have_posts()) : the_post(); 
						$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium' );
						$url = $thumb['0'];
						$periodo=types_render_field("periodo", array("row" => "true"));
				?>

					<div href="<?php echo get_the_permalink();?>" class="col-4 module item_category <?php if($col2==1){ echo " reset";} if($col3==1){ echo " reset3";} if($col4==1){ echo " reset4";} ?> item-overlay">
						<div class="cat_img_mask" style="background-image:url('<?php echo $url;?>')"></div>
						<h4 class="prodotto_titolo"><?php the_title();?></h4>
						<span class="periodo"><?= $periodo ?></span>
					</div>	
					<?php 
						if($col2==2){ $col2=0;} 
						if($col3==3){ $col3=0;} 
						if($col4==4){ $col4=0;}
						$col4++;
						$col2++;
						$col3++;
					?>
				<?php endwhile; ?>
			<?php endif; ?>
			<?php twentythirteen_paging_nav();?>
		</div>
	</div>
</div>
<?php get_footer(); ?>
