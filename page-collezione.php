<?php
/**
 * 
 * Template Name: Collezioni
 */
get_header();
?>

	<?php if (have_posts()) : ?>
		<?php while (have_posts()) : the_post(); ?>
			<div class="page-contatti">
				<div class="row">
					<div class="col-12">
						<h2><?php the_title();?></h2>
					</div>
				</div>
				<div class="row grid">
					<?php

						$your_query = new Query_By_Taxonomy(
						    array(
						      'post_type' => 'product',
						      'posts_per_page' => -1,
						      'taxonomy' => 'collezione',
						  )
						);

						woocommerce_product_loop_start();

						while ( $your_query->have_posts() ) : $your_query->the_post();
							

							?>


									<?php wc_get_template_part( 'content', 'product' ); ?>



							<?php
						
						endwhile;
						// reset post data (important!)
						wp_reset_postdata();

						woocommerce_product_loop_end();

						?>

					
				</div>
			</div>
		<?php endwhile; ?>
	<?php endif; ?>
<?php get_footer(); ?>