    $(document).ready(function(){
 
        $('h1 a, h2 a').ieffembedfix();

		hide_h2();
		show_h2();
 
    });

	function hide_h2(){
		$('.post h2').animate({opacity:0}, 0 );
	}

	function show_h2(){
		$('.post h2').animate({opacity:0}, 0 );
        $('.post a').bind({
			'mouseenter': function() {
//				$(this).next('h2').css('display','block');
				$(this).next('h2').stop(false,true).animate({opacity:.9}, 500,'easeOutQuint' );
			},
			'mouseleave': function() {
//				$(this).next('h2').css('display','none');
				$(this).next('h2').stop(false,true).animate({opacity:0}, 500,'easeInQuint' );
			}
        });
	}