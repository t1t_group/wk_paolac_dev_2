
var paginaold_very=window.location.href;

/* DIMENSIONI SCHERMO */
	 var $ = jQuery.noConflict();
	var $width = $(window).width();
	var $heigth = $(window).height();
	var $docheigth = document.documentElement.scrollHeight;
	var $scrollspace = $docheigth-$heigth;

function is_touch_device() {
  return 'ontouchstart' in window        // works on most browsers 
      || navigator.maxTouchPoints;       // works on IE10/11 and Surface
};


function parallax(e, target, layer) {
    var layer_coeff = 10 / layer;
    var x = ($(window).width() - target.offsetWidth) / 2 - (e.pageX - ($(window).width() / 2)) / layer_coeff;
    var y = ($(window).height() - target.offsetHeight) / 2 - (e.pageY - ($(window).height() / 2)) / layer_coeff;
    $(target).offset({ top: y ,left : x });
};

	  
$(document).ready( function() {

	var timeout = setTimeout(function(){
		$(".pbSubmit").addClass("freccia_dx");
	},1000);

	$(window).on('load',function() {
    	if($(".expand_row").length){
    		$(".expand_row ").slideToggle(function(){
    			$("#logo_loader").hide();
    			$("#loader").fadeOut(500);
    			$('body').delay(500).addClass('caricato').removeClass('noncaricato');
    		});
    	}else{
    		$("#logo_loader").hide();
    		$("#loader").fadeOut(500);
    		$('body').delay(500).addClass('caricato').removeClass('noncaricato');
    	}
    	
    });



	/***	MAIN MENU TOGGLE 	***/
	function main_menu_toggle(scrollTop){
	 	if(scrollTop > 100) {
	 		document.querySelector('body').classList.add('wk-attivo');
	 	} else {
	 		document.querySelector('body').classList.remove('wk-attivo');
	 	}
	}
	// init
  	main_menu_toggle(window.pageYOffset);

  	// HEADER menu toggle
  	window.addEventListener('scroll', function() {

  		main_menu_toggle(window.pageYOffset);
  		
  	});
	
	
	// OTTIMIZZAZIONE PER RETINA
	
	if (window.devicePixelRatio == 2) {

          var images = $("img.hires");

          // loop through the images and make them hi-res
          for(var i = 0; i < images.length; i++) {

            // create new image name
            var imageType = images[i].src.substr(-4);
            var imageName = images[i].src.substr(0, images[i].src.length - 4);
            imageName += "@x2" + imageType;

            //rename image
            images[i].src = imageName;
          }
     }
	 

	 
	
	if(!is_touch_device()){
		
		$(".touch").addClass("notouch");
		$("body").addClass("notouch");
		
	}
	
	
	
	
	
	if($width<768)
	{
		
		/* PER SMARTPHONE */
		
		
		/* TASTO PER APERTURA MENU */
		$("#hamburger-menu").on('click', function (){
		  $('body').toggleClass('nav-open');
		  $(this).toggleClass('is-active');
		});
			
		
		
		
		
		$("nav.onlymobile .menu").on('click', 'li a', function(){
			var $this=$(this);
			if($this.hasClass("attivo"))
			{
				$this.toggleClass("attivo");
				$this.next('ul.sub-menu').slideToggle();
			}
			else{
				$this.next('ul.sub-menu').slideToggle();
				$this.toggleClass("attivo");	
			}
			
			if($(this).siblings("ul.sub-menu").size()!=0){
						  return false;
						}
		});
		
		
		$("nav.onlymobile .menu li.current-menu-ancestor > a").addClass("attivo");

		$('.slider-prodotti-salone').flexslider({
				    animation: "fade",
				    animationLoop: true,
				    slideshow:true,
				    slideshowSpeed : "4000",
				     pauseOnHover: true,
				    multipleKeyboard: true,
				    keyboard: true
				});
		
	}else {

		/* GESTIONE SEARCH BAR */

		$(".toggle-ricerca").on('click', function(){
			$(".search-form").toggleClass('attivo');
		});


		// TASTO HOME

		// TEMPORARY HOME
		$('#temporary-home').mousemove(function (e) {    
		    parallax(e, document.getElementById('nh0'), 1.6);
		    //parallax(e, document.getElementById('nh1'), 4);
		    //parallax(e, document.getElementById('nh2'), 1.4);
		    parallax(e, document.getElementById('nh3'), 2.2);
		    parallax(e, document.getElementById('nh4'), 3.6);
		    parallax(e, document.getElementById('nhlogo'), 1.2);
		});
	}


		

	/* FILTER NAVIGATION */
	$('.filter-selection').change(function() {
	  // set the window's location property to the value of the option the user has selected
	  window.location = $(this).val();
	});	


	$('#clear-filter').on('click', function() {
	  window.location = $(this).attr('data-url');
	});	



	/* SLIDER SITO */

	if($("#slider-editions").length > 0){
		$('#slider-editions').flexslider({
		    animation: "fade",
		    animationLoop: true,
		    slideshow:false,
		    slideshowSpeed : "4000",
		    pauseOnHover: true,
		    multipleKeyboard: true,
		    keyboard: true,
		    start: function(slider) {
	            $('.total-slides').text(slider.count);
	            $('.current-slide').text(slider.currentSlide+1);
	          },
	          after: function(slider) {
	            $('.current-slide').text(slider.currentSlide+1);
	          }
		  
		});
	}

	if($(".slider-news").length > 0){
		$('.slider-news').flexslider({
		    animation: "fade",
		    animationLoop: true,
		    slideshow:false,
		    slideshowSpeed : "4000",
		    pauseOnHover: true,
		    multipleKeyboard: true,
		    keyboard: true
		});
	}

	if($("#slider-prodotto").length > 0){
		$('#slider-prodotto').flexslider({
		    animation: "fade",
		    slideshow:false,
		    animationLoop: true,
		    slideshowSpeed : "4000",
		    pauseOnHover: true,
		    multipleKeyboard: true,
		    controlNav: "thumbnails",
		    keyboard: true
		});
	}

	if($(".slider-interno").length > 0){
		$('.slider-interno').flexslider({
		    animation: "fade",
		    animationLoop: true,
		    slideshow:false,
		    slideshowSpeed : "4000",
		    pauseOnHover: true,
		    multipleKeyboard: true,
		    keyboard: true
		});
	}


	if($width<768){
			if($("#lookbook_slider").length > 0){
		        $("#lookbook_slider").flexslider({
		            animation: "slide",
				    animationLoop: false,
				    keyboard: true,
				    itemWidth: ($('.lookbook-row').innerWidth()),
				    directionNav: true,
				    minItems: 1,
			    	maxItems: 1,	
		        });
			}

	} else {
		if($("#lookbook_slider").length > 0){
	        $("#lookbook_slider").flexslider({
	            animation: "slide",
			    animationLoop: false,
			    keyboard: true,
			    itemWidth: ($('.lookbook-row').innerWidth()*0.5),
			    minItems: 2,
			    maxItems: 2,
			    directionNav: true,
	        });
		}

		// SCROLLBAR FILTRI SHOP
		if($('.filter-panel')[0]){
			new SimpleBar($('.filter-panel')[0]);
		}
		

		/* ACCORDION PER FILTRI DESIGNER
		$('.filtri-designer').on('click', function(){
			$('.filter-designer').slideToggle();
		});*/

	}


	/* ISOTOPE PER GRIGLIA DESIGNER 
	if($('.grid').length>0){
		var $grid = $('.grid').imagesLoaded( function() {
		  // init Isotope after all images have loaded
		  $grid.isotope({
		    // options...
		    itemSelector: '.grid-item'
		  });
		});
	}*/


	/* APERUTRA OVERLAY AJAX*/
	$.ajaxSetup({cache:false});
        $(".item-overlay").click(function(event){
			
            $("body").toggleClass("bloccoscroll");
            $("html").toggleClass("bloccoscroll");

			
			$("#singolo-overlay").toggleClass("open");
			
			//get the link location that was clicked
			pageurl = $(this).attr('href');
            var post_link = $(this).attr("href");
			
			/* PASSO COME VARIABILE LA VECCHIA PAGE */
			var paginaold=window.location.href;
			
			
			//to get the ajax content and display in div with id 'content'
			$.ajax({
				type:"POST",
				url:pageurl+'?rel=tab',
				data: { paginaold : paginaold_very },
				success: function(data){
					$('#singolo-overlay').html(data).addClass("loadingOff");
					
					},
					cache: false
			});
			
			//to change the browser URL to the given link location
			if(pageurl!=window.location){
				 window.history.pushState({path:pageurl},'',pageurl);
				}
				
			
			
			
        return false;
        });


	
	/* GESTIONE TOGGLE EDITIONS */

	$(".mostra_prodotti").on('click', function(){
		$(this).parents('.expandible').children('.expand_row').slideToggle();
		$(this).toggleClass('attivo');
		$('html, body').animate({scrollTop: $(this).parents('.expandible').children('.expand_row').offset().top}, 1000);
	});

	$(".close_row").on('click', function(){
		$(this).parents('.expand_row').slideToggle();
		$(this).parents('.expandible').find('.mostra_prodotti').toggleClass('attivo');
		$('html, body').animate({scrollTop: $(this).parents('.expandible').find('.mostra_prodotti').offset().top -200}, 1000);
	});


	/* GESTIONE FLEXSLIDER CON LE VARIANTI WOOCOMMERCE */

	$prezzo_originale=$(".dati-prodotto .price").html();
	$(".dati-prodotto .price").attr('original-price',$prezzo_originale);

	$(".variations select").on('change', function() { 

		$value=$(this).val();

		if($value){


		setTimeout(
		  function() 
		  {
		  	// CAMBIO PREZZO PRINCIPALE
		  	$prezzo_nuovo=$(".woocommerce-variation-price").html();
		  	//console.log('test2');
		  	if($prezzo_nuovo!="" && $prezzo_nuovo){
		  		$(".dati-prodotto .price").html($prezzo_nuovo);
		  	}
		  	
		    //do something special
		  }, 100);

		}else{
			$prezzo_originario=$(".dati-prodotto .price").attr('original-price');
			$(".dati-prodotto .price").html($prezzo_originario);
		}
		/* wc-variation-selection-needed*/
	});

	$('#immagine-principale').on('load', function () {
		//console.log('testone3');
   	 	$starturl=$("#immagine-principale").attr('src');
		$('#immagine-principale').closest('a').attr('href', $starturl);
		//$('#immagine-principale').removeAttr('srcset').removeAttr('sizes');
		$('#slider-prodotto').flexslider(0);
		$('#slider-prodotto').flexslider('pause');
		

	});
	

	/* GESTIONE TAB PRODOTTO */
	if($(".list_tab_prodotto").length > 0){
		$(".list_tab_prodotto li:first-child h5").addClass('attivo');
		$(".tab_prodotto").first().show();	
	}
	$(".list_tab_prodotto h5").on("click", function(){
		$(".list_tab_prodotto h5").removeClass('attivo');
		$(this).addClass('attivo');
		$(".tab_prodotto").hide();
		$id_tab=$(this).attr('data-tab');
		//console.log($id_tab);
		$("#"+$id_tab).show();
	});




		 var filters = {};

		$(".filter-container .filter-item").on('click', function(){

			window.scrollTo({top: 0, behavior: 'smooth'});
			var $this = $(this);
			$this.toggleClass('attivo');
			
			// Toggle effetto attivo filtri
			toggleActiveFiltersItems($this);

			// Toggle visibilità clear filters
			toggleClearFilters()

			var valore = $this.attr('data-category');
			var group = $this.attr('data-gruppo');

		   	// create array for filter group, if not there yet
			var filterGroup = filters[ group ];
			if ( !filterGroup ) {
				filterGroup = filters[ group ] = [];
			}



		    // add/remove filter
		    if ( $this.hasClass('attivo') ) {
		        // add filter
		        if($this.parents('.filter-category')[0]){

			        $.each($this.siblings('.attivo'), function(item){
			        	var valore_att = $(this).attr('data-category');
			        	var index = filterGroup.indexOf( valore_att );
			        	filterGroup.splice( index, 1 );
			        	$(this).removeClass('attivo');
			        });
			    }

		        filterGroup.push( valore );

		    } else {
		        // remove filter
		        var index = filterGroup.indexOf( valore );
		        filterGroup.splice( index, 1 );
		    } 

			var comboFilter = getComboFilter();
			if(comboFilter == ""){
				comboFilter = '.wk_parent, .wk_single';
				$(".filter-button.wk_pulsante_container button").removeClass('attivo').attr("disabled", true);
			} else {
				$(".filter-button.wk_pulsante_container button").addClass('attivo').attr("disabled", false);
			}

			$('.grid-shop .grid').isotope({ 
				itemSelector : '.grid-item',
				filter: comboFilter,
				//sortBy: 'new'
	    	});

	    	//$grid_shop.isotope('bindResize');
			//$(window).trigger('resize');
		     
		});


		// Toggle effetto attivo filtri
		function toggleActiveFiltersItems(elem){
			// Classe per effetto attivo/disattivo dei filtri
			if(elem.closest('.filter-container').find('.filter-item.attivo').length !== 0){
				elem.closest('.filter-container').addClass('attivo');
			} else {
				elem.closest('.filter-container').removeClass('attivo');
			}
		}

		// Toggle visibilità clear filters
		function toggleClearFilters(){
			// Classe per effetto attivo/disattivo del pulsante "clear filters"
			if($(".filter-item.attivo").length !== 0){
				$('.clean-filters').addClass('attivo');
			} else {
				$('.clean-filters').removeClass('attivo');
			}
		}


		function getComboFilter() {
			var combo = [];
			for ( var prop in filters ) {
			 var group = filters[ prop ];
			 if ( !group.length ) {
			   // no filters in group, carry on
			   continue;
			 }
			 // add first group
			 if ( !combo.length ) {
			   combo = group.slice(0);
			   continue;
			 }
			 // add additional groups
			 var nextCombo = [];
			 // split group into combo: [ A, B ] & [ 1, 2 ] => [ A1, A2, B1, B2 ]
			 for ( var i=0; i < combo.length; i++ ) {
			   for ( var j=0; j < group.length; j++ ) {
			     var item = combo[i] + group[j];
			     //console.log(item);
			     nextCombo.push( item );
			   }
			 }
			 combo = nextCombo;
			}

			//console.log(combo);
			var comboFilter = combo.join(', ');


			return comboFilter;
		}


	$(".filter-header").on('click', function(){
		$(this).closest(".filter-panel").toggleClass("attivo");
		$(".filter-button.wk_pulsante_container").fadeToggle();
	});

	$(".filter-button.wk_pulsante_container button").on('click', function(){
		$(".filter-header").closest(".filter-panel").toggleClass("attivo");
		$(".filter-button.wk_pulsante_container").fadeToggle();
	});

	/**********		HIGHLIGHTS CONTAINER ON SCROLL.    **********/

	var controller = new ScrollMagic.Controller();

	// build tween
	var tween = TweenMax.to("#target", 1, {x: -($("#target").innerWidth() - $(".highlights-container").innerWidth()), ease: Linear.easeOut});

	// build scene
	var scene = new ScrollMagic.Scene({triggerElement: "#target", triggerHook: 1, duration: ($heigth - 110 - $(".highlights-container").outerHeight()), offset: ($(".highlights-container").outerHeight() + 50)})
					.setTween(tween)
					//.setPin("#target", {pushFollowers: false})
					//.addIndicators() // add indicators (requires plugin)
					.addTo(controller);





	/********		RICERCA 		************/

	var mainContainer = $('#container'),
	    openCtrl = $('.btn--search'),
	    closeCtrl = $('#btn-search-close'),
	    searchContainer = document.querySelector('.search'),
	    inputSearch = searchContainer.querySelector('.search__input'),
	    bordoDx = document.getElementById('bordodx'),
	    bordoSx = document.getElementById('bordosx');


	    //console.log(openCtrl);

	function init() {
	    initEvents();   
	}

	function initEvents() {
	    $(openCtrl).on('click', openSearch);
	    $(closeCtrl).on('click', closeSearch);
	    document.addEventListener('keyup', function(ev) {
	        // escape key.
	        if( ev.keyCode == 27 ) {
	            closeSearch();
	        }
	    });
	}

	function openSearch() {
	    //console.log('vvv');
	    $(mainContainer).addClass('main-wrap--move');
	    $(searchContainer).addClass('search--open');
	    setTimeout(function() {
	        inputSearch.focus();
	    }, 600);
	}

	function closeSearch() {
	    $(mainContainer).removeClass('main-wrap--move');
	    $(searchContainer).removeClass('search--open');
	    inputSearch.blur();
	    inputSearch.value = '';
	}

	init();



	/********		GRADIENTE HOME    	**********/

	/*if($("#change-background")[0]){
		var scene = new ScrollMagic.Scene({triggerElement: "#change-background", duration: 2000})
					// animate color and top border in relation to scroll position
					.setTween("#change-background", {backgroundColor: "#fafafa"}) // the tween durtion can be omitted and defaults to 1
					//.addIndicators({name: "2 (duration: 1000)"}) // add indicators (requires plugin)
					.addTo(controller);
	}*/




	function setCookie(cname, cvalue, exdays) {
	  var d = new Date();
	  d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
	  var expires = "expires="+d.toUTCString();
	  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
	}

	function getCookie(cname) {
	  var name = cname + "=";
	  var ca = document.cookie.split(';');
	  for(var i = 0; i < ca.length; i++) {
	    var c = ca[i];
	    while (c.charAt(0) == ' ') {
	      c = c.substring(1);
	    }
	    if (c.indexOf(name) == 0) {
	      return c.substring(name.length, c.length);
	    }
	  }
	  return "";
	}

	function checkCookie(cname) {
	  var cookie_cnt = getCookie(cname);
	  if (cookie_cnt != "") {
	    return true;
	  }

	  return false;
	}


	var PPtimeout = setTimeout(function(){
		if(!checkCookie('paolac_newsletter')){
			$("#newsletter-popup").addClass('attivo');
			setCookie('paolac_newsletter', true, 10);
		}

	}, 5000);


	$("#newsletter-popup .close-overlay").on('click', function(){
		$("#newsletter-popup").removeClass('attivo');
	});


	


});	/* FINE DOCUMENT READY */



(function($) {

  /**
   * Copyright 2012, Digital Fusion
   * Licensed under the MIT license.
   * http://teamdf.com/jquery-plugins/license/
   *
   * @author Sam Sehnert
   * @desc A small plugin that checks whether elements are within
   *     the user visible viewport of a web browser.
   *     only accounts for vertical position, not horizontal.
*/

  $.fn.visible = function(partial) {
    
      var $t            = $(this),
          $w            = $(window),
          viewTop       = $w.scrollTop(),
          viewBottom    = viewTop + $w.height(),
          _top          = $t.offset().top,
          _bottom       = _top + $t.height(),
          compareTop    = partial === true ? _bottom : _top,
          compareBottom = partial === true ? _top : _bottom;
    
    return ((compareBottom <= viewBottom) && (compareTop >= viewTop));

  };
    
})(jQuery);

var win = $(window);

var allMods = $(".module");

allMods.each(function(i, el) {
  var el = $(el);
  if (el.visible(true)) {
    el.addClass("already-visible"); 
  } 
});

win.scroll(function(event) {
  
  allMods.each(function(i, el) {
    var el = $(el);
    if (el.visible(true)) {
      el.delay(500).addClass("come-in"); 
    } 
  });
  
});   



/******* 	GRIGLIA SHOP 	********/

$(window).on('load', function(){


	 // GESTISCO ATTIVAZIONE FILTRI


	 function getFiltersByGetVars(){
	 	var vars = getUrlParameter();
	 	
	 	if(vars){
	 		$.each( vars, function( key, value ) {
	 		  	$('.filter-item[data-category=".'+value[0]+'"]').trigger('click');
	 		  	//console.log('filters');
	 		});
	 	}
	 }

	 var getUrlParameter = function getUrlParameter(sParam) {
	     var sPageURL = window.location.search.substring(1),
	         sURLVariables = sPageURL.split('&'),
	         sParameterName,
	         i;
	     var results = new Array();
	     if(sPageURL != ""){
	 	    for (i = 0; i < sURLVariables.length; i++) {
	 	        sParameterName = sURLVariables[i].split('=');
	 	        results.push(sParameterName );

	 	       if (sParameterName[0] === sParam) {
	 	            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
	 	        }
	 	    }
	 	    return results;
	 	 }

	     return false;
	 };


	 // GESTISCO ATTIVAZIONE FILTRI

	var timeout = setTimeout(function(){
		getFiltersByGetVars()
	}, 500);

	var $grid_shop = $('.grid-shop .grid');
			  
    $grid_shop.imagesLoaded( function(){

      	$grid_shop.isotope({
          itemSelector : '.grid-item',
          layoutMode: 'fitRows',
          filter: '.wk_parent, .wk_single',
        });

	  $(".grid-shop .grid-item").css({opacity:1});
	
	});
});
/* MANAGE RESIZE 
if(is_touch_device()){
	$( window ).on( "orientationchange", function( event ) {


	});
}else{
	$(window).on('resize', function(e) {

		//fissaFooter();

	});
}
*/

// GESTIONE MASONRY + INFINITE SCROLL

	$(function(){
		
		/*
	    var $container = $('#mas_prodotti');
		  
		    $container.imagesLoaded( function(){
		      $container.masonry({
		        itemSelector : '.grid-item',
		        isAnimated: true
		      });
			  $("#mas_prodotti .grid-item").css({opacity:1});
		
		    });
*/

		var $container = $('.grid');
		  
		    $container.imagesLoaded( function(){
		      $container.masonry({
		        itemSelector : '.grid-item',
		        isAnimated: true
		      });
			  $(".grid .grid-item").css({opacity:1});
		
		    });

		      
	});



// DIVISIONE MENU WIDE
var right_menu_items = $('header nav #menu_wide .menu-item:eq(3), header .wrap nav #menu_wide .menu-item:eq(4), header .wrap nav #menu_wide .menu-item:eq(5)');

$(right_menu_items).remove();
$('header nav #menu_wide').attr('id', 'menu_wide_left');
$('header nav #menu_wide_right').append(right_menu_items);
$('header nav').fadeIn();



/*

	$('#mas_prodotti').infinitescroll({
		navSelector  : "div.navigation",            
               // selector for the paged navigation (it will be hidden)
        nextSelector : "div.navigation span.next-entries",    
               // selector for the NEXT link (to page 2)
        itemSelector : "#mas_prodotti li.grid-item"
               // selector for all items you'll retrieve
    },

    function( newElements ) {
    	
    	var $newElems = jQuery(newElements).css({ opacity: 1 });
    	$newElems.imagesLoaded(function(){
		    $newElems.animate({ opacity: 1 });
		    $('#mas_prodotti').masonry( 'appended', $newElems, true );
	    });
    	
    	$('#mas_prodotti .grid-item').showPostType();
    	
	});
*/



