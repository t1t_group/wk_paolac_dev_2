<?php 
global $javascript_append;
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?> >
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
    
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-touch-fullscreen" content="YES">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=5.0, user-scalable=yes ">

	<title><?php wp_title(); ?></title>
	
    
    <!-- FONT LIB -->
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
 
	 <link rel="stylesheet" type="text/css" href="<?php echo bloginfo( 'stylesheet_directory' );?>/css/stile.css" />

    <!--link rel="shortcut icon" type="image/x-icon" href="<?php echo bloginfo( 'stylesheet_directory' ); ?>/img/favicon.ico?v=4"-->
    <!--
    <link rel="apple-touch-icon" href="<?php echo bloginfo( 'stylesheet_directory' ); ?>/img/Icon-Small@2x.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo bloginfo( 'stylesheet_directory' ); ?>/img/Icon-72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo bloginfo( 'stylesheet_directory' ); ?>/img/Icon@2x.png">
 
    <link rel="apple-touch-icon" sizes="512x512" href="<?php echo bloginfo( 'stylesheet_directory' ); ?>/img/iTunesArtwork.png"> -->
    		<!--[if IE]>
      		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    		<![endif]-->


    
	
    <?php
	
			echo '<meta property="fb:admins" content="PaolaC"/>';
			echo '<meta property="og:title" content="' . get_the_title() . '"/>';
			echo '<meta property="og:type" content="article"/>';
			echo '<meta property="og:url" content="' . get_permalink() . '"/>';
			echo '<meta property="og:site_name" content="PaolaC"/>';
		 if(!has_post_thumbnail( $post->ID )) { //the post does not have featured image, use a default image
		 
		 	$url_logo=""; 
			echo '<meta property="og:image" content="' .$url_logo. '"/>';
		 }
		 else{
			  $thumbnail_src = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'medium' );
			  echo '<meta property="og:image" content="' . esc_attr( $thumbnail_src[0] ) . '"/>';
		 }
	
	?>
	<?php wp_head();
	
	global $woocommerce;
	global $lista_related;
	global $storia_related;
				 ?>

	<meta name="p:domain_verify" content="a241989c8aa0631f98b0a2f0ed7b0697"/>    
</head>

<?php 
    if(isset($_GET['restyle'])){
        $class = " restyle2020 ";
    }
?>

<body class="noncaricato <?php if(is_front_page()){ echo " homepage "; /*" bloccoscrollwide";*/ } if(has_term('','edizione') && !is_shop() ){ echo " rame ";} if(is_page_template('page-edizione.php')){ echo " rame ";} if(is_page_template('page-shop.php')){ echo " shop ";} ?>  <?php echo $class; ?>">
	<svg class="hidden">
		<defs>
			<symbol id="icon-search" viewBox="0 0 24 24">
				<title>search</title>
				<path d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z"/>
			</symbol>
			<symbol id="icon-cross" viewBox="0 0 24 24">
				<title>cross</title>
				<path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"/>
			</symbol>
		</defs>
	</svg>
	<!--div id="bordosx" class="bordo-sinistro nomobile"></div>
	<div id="bordodx" class="bordo-destro nomobile"></div-->
	<div id="loader">
	    <div id="logo_loader">
	    </div>
	</div>



	

<?php

if(true && is_front_page()){ ?>

<?php }else{ ?>
<?php 


if(is_front_page()){  
	// COLLEGO IMMAGINE HOMEPAGE

	/* PRENATALE 2017
	$array_foto=array(4535, 4537, 4539, 4541);
	$rand_keys = array_rand($array_foto, 1);
	$id_element=$array_foto[$rand_keys];
	
	*/


	/* NUOVA HOME AGOSTO 2018*/

	// Recupero ID immagine caricata su OptionTree
	$id_element=ot_get_option( 'immagine_header' , 'raw'); 
	$id_element=pippin_get_image_id($id_element);
	$colore_freccia=ot_get_option( 'colore_freccia' , 'raw'); 

	//$id_element=6377;
	
	$thumb_s = wp_get_attachment_image_src( $id_element, 'medium' );
	$thumb_m = wp_get_attachment_image_src( $id_element, 'large' );
	$thumb_w = wp_get_attachment_image_src( $id_element, 'full' );
	

 ?>
<style skip_me="1" wp_skip_me="1">
  .hero-home-img { background-image:url('<?php echo $thumb_s['0'] ?>');}
  @media (min-width: 768px) { .hero-home-img { background-image:url('<?php echo $thumb_m['0'] ?>'); } }
  @media (min-width: 1400px) { .hero-home-img { background-image:url('<?php echo $thumb_w['0'] ?>'); } }
</style>

<div class="hero-home header nomobile big-home-buttom">
	<div class="hero-home-img">
	</div>

	<div class="scroll-home nomobile trigger big-home-buttom">
		<?php //get_template_part('img/scroll-down.svg'); ?>
		<!--<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
			 viewBox="0 0 30 45" enable-background="new 0 0 30 45" xml:space="preserve">
		<polygon fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" points="15.785,33.246 15.785,6.033 14.216,6.033 14.216,33.246 
			9.519,33.246 15,38.967 20.48,33.246 "/>
		</svg>-->
		<?php 
			if($colore_freccia=="bianca"){
				?><img src="<?php echo bloginfo( 'stylesheet_directory' ); ?>/img/freccia_dx_nera.png"><?php
			}else{
				?><img src="<?php echo bloginfo( 'stylesheet_directory' ); ?>/img/freccia_dx_nera.png"><?php
			}
		?>
	</div>
	<!--<a class="overpage scroll-home trigger nomobile"></a>-->
</div>

<?php 
}
?>
<?php  } ?>

<div id="container" class="container">
	

<header id="header" class=" <?php if(has_term('','edizione')){ echo "editions";} if(!is_front_page()){ echo "wk_noanimation"; } ?>">
	<div class="bordo-sopra nomobile">
		<div class="link_left">
			<?php 
				$id_page=icl_object_id(4457, 'page', false);
				$link_page=get_permalink( $id_page );
			?>
			<button id="btn-search" class="btn btn--search"><svg class="icon icon--search"><use xlink:href="#icon-search"></use></svg><?php echo __("Search","webkolm"); ?></button>
		</div>

	 	<ul class="link_right">
	 		<li class="nomobile"><a href="<?php echo get_permalink( icl_object_id(62, 'page', false) );?>">
	 			<svg enable-background="new 0 0 512 512" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><path d="m256 0c-99.252 0-180 80.748-180 180 0 33.534 9.289 66.26 26.869 94.652l142.885 230.257c2.737 4.411 7.559 7.091 12.745 7.091h.119c5.231-.041 10.063-2.804 12.75-7.292l139.243-232.488c16.61-27.792 25.389-59.681 25.389-92.22 0-99.252-80.748-180-180-180zm128.866 256.818-126.594 211.368-129.905-209.34c-14.633-23.632-22.567-50.896-22.567-78.846 0-82.71 67.49-150.2 150.2-150.2s150.1 67.49 150.1 150.2c0 27.121-7.411 53.688-21.234 76.818z"/><path d="m256 90c-49.626 0-90 40.374-90 90 0 49.309 39.717 90 90 90 50.903 0 90-41.233 90-90 0-49.626-40.374-90-90-90zm0 150.2c-33.257 0-60.2-27.033-60.2-60.2 0-33.084 27.116-60.2 60.2-60.2s60.1 27.116 60.1 60.2c0 32.683-26.316 60.2-60.1 60.2z"/></svg>
	 			<?php _e('Where to buy', 'webkolm') ?></a></li>
	 		<li class="nomobile"><a href="<?php echo get_permalink( icl_object_id(46, 'page', false) );?>"><?php _e('Contacts', 'webkolm') ?></a></li>
	 	</ul>
	</div>

	<div class="wrap">

		<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home" class="titolo_sito onlymobile">
			<svg id="logosito" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 501 147.25"><g data-name="Livello 2"><g id="payoff" data-name="Livello 1"><polygon points="161.13 125.03 166.76 140.93 172.36 125.03 178.29 125.03 178.29 146.95 173.76 146.95 173.76 140.96 174.22 130.62 168.3 146.95 165.2 146.95 159.3 130.63 159.75 140.96 159.75 146.95 155.24 146.95 155.24 125.03 161.13 125.03" style="fill:#1e1c1a"/><rect x="197.93" y="125.04" width="4.52" height="21.91" style="fill:#1e1c1a"/><polygon points="226.56 143.32 236.15 143.32 236.15 146.95 222.05 146.95 222.05 125.03 226.56 125.03 226.56 143.32" style="fill:#1e1c1a"/><path d="M260.11,138.78h5.48l-2.76-8.21Zm6.7,3.65h-7.92l-1.5,4.52h-4.81L260.74,125h4.19l8.2,21.91h-4.8Z" style="fill:#1e1c1a"/><polygon points="308.41 146.95 303.89 146.95 295.1 132.53 295.1 146.95 290.59 146.95 290.59 125.03 295.1 125.03 303.91 139.48 303.91 125.03 308.41 125.03 308.41 146.95" style="fill:#1e1c1a"/><path d="M341.19,135.49a9.14,9.14,0,0,0-1.24-5.23,4.35,4.35,0,0,0-7,0,9,9,0,0,0-1.25,5.18v1.07a9.38,9.38,0,0,0,1.23,5.21,4,4,0,0,0,3.55,1.85,4,4,0,0,0,3.49-1.79,9.22,9.22,0,0,0,1.24-5.2Zm4.57,1a13.22,13.22,0,0,1-1.14,5.67,8.62,8.62,0,0,1-3.27,3.77,9.69,9.69,0,0,1-9.75,0,8.7,8.7,0,0,1-3.31-3.74,12.87,12.87,0,0,1-1.19-5.59v-1.08a13.13,13.13,0,0,1,1.17-5.7,8.64,8.64,0,0,1,3.29-3.78,9.64,9.64,0,0,1,9.74,0,8.73,8.73,0,0,1,3.3,3.78,13.07,13.07,0,0,1,1.16,5.68Z" style="fill:#1e1c1a"/></g><g id="logo"><path d="M28.93,44.41A20.63,20.63,0,0,0,33,44a10.44,10.44,0,0,0,3.58-1.47,16.5,16.5,0,0,0,5.64-6.11,17.6,17.6,0,0,0,2.06-8.52,17.61,17.61,0,0,0-1.56-7.36,18.87,18.87,0,0,0-4.3-6,21.3,21.3,0,0,0-6.31-4.09A19.11,19.11,0,0,0,24.46,9h-12V44.41ZM2.57,6,0,1.75H29.38A25.41,25.41,0,0,1,39.15,3.6,24.51,24.51,0,0,1,47,8.73a24.2,24.2,0,0,1,5.25,7.58,22.75,22.75,0,0,1,1.9,9.22,22.19,22.19,0,0,1-3.24,11.84,23.46,23.46,0,0,1-8.72,8.35l-4.46,2.51a29.75,29.75,0,0,1-6.15,2.4,26.15,26.15,0,0,1-6.81.87H12.51V81.18H2.57Z"/><path d="M102.32,47.9,87.35,15l-14.63,33ZM83.33,6l-2-4.25H92.16l36.08,79.43H117.4L105.56,55H69.48L59.54,76.93,61,81.18H49Z"/><path d="M193.36,72.13a31.1,31.1,0,0,0,11.51-12.28A36,36,0,0,0,209,42.77,34.65,34.65,0,0,0,206.26,29a35.37,35.37,0,0,0-18.82-18.94,35.08,35.08,0,0,0-14-2.78,46.79,46.79,0,0,0-8.21.71A19.64,19.64,0,0,0,158,10.91a31.07,31.07,0,0,0-11.5,12.28,35.85,35.85,0,0,0-4.14,17.08A34.82,34.82,0,0,0,145.05,54a36.27,36.27,0,0,0,7.48,11.3A34.56,34.56,0,0,0,163.82,73,35.73,35.73,0,0,0,178,75.73,45.86,45.86,0,0,0,186.1,75,19.56,19.56,0,0,0,193.36,72.13Zm1,5.35a46.52,46.52,0,0,1-9.83,4,40.85,40.85,0,0,1-11.06,1.47A42,42,0,0,1,144,71.15a39.91,39.91,0,0,1-8.77-12.77A38.7,38.7,0,0,1,132,42.77a37.91,37.91,0,0,1,5.58-20.29A42.49,42.49,0,0,1,152.48,8l4.35-2.4a40.76,40.76,0,0,1,9.95-4.15A42.46,42.46,0,0,1,178,0a41.69,41.69,0,0,1,29.38,11.79,39.9,39.9,0,0,1,8.77,12.82A40.14,40.14,0,0,1,217.83,51a40.57,40.57,0,0,1-4.14,9.66,39.49,39.49,0,0,1-6.42,8.18A40.81,40.81,0,0,1,199,75.07Z"/><polygon points="233.69 6 231.12 1.75 243.63 1.75 243.63 73.98 278.82 73.98 275.91 81.19 233.69 81.19 233.69 6"/><path d="M335.67,47.9,320.71,15l-14.64,33ZM316.68,6l-2-4.25h10.84l36.08,79.43H350.76L338.91,55H302.83l-9.94,21.93,1.45,4.25H282.39Z"/><path d="M472.07,75.07q-2.69,1.65-5.25,3a37.29,37.29,0,0,1-10.34,3.77,59.35,59.35,0,0,1-12,1.14,42.27,42.27,0,0,1-29.55-11.78,40.38,40.38,0,0,1-8.88-12.77,38.09,38.09,0,0,1-3.24-15.61,39,39,0,0,1,1.45-10.69,39.47,39.47,0,0,1,4.14-9.6,38.88,38.88,0,0,1,6.48-8.13,40.75,40.75,0,0,1,8.48-6.27l4.47-2.51a45.5,45.5,0,0,1,10-4.1,41,41,0,0,1,11-1.47A51,51,0,0,1,471,4.8l-3.35,8.84A49.36,49.36,0,0,0,457,9a43,43,0,0,0-12.4-1.69,39.61,39.61,0,0,0-8.1.82,25.23,25.23,0,0,0-7.43,2.78,32.61,32.61,0,0,0-11.73,12.33,34.86,34.86,0,0,0-4.24,17A33.55,33.55,0,0,0,415.94,54a36.89,36.89,0,0,0,7.7,11.3,36,36,0,0,0,25.53,10.42A44.59,44.59,0,0,0,461,74.31a47.79,47.79,0,0,0,10.22-4.15Z"/><path d="M501,75.62a5.31,5.31,0,0,1-2,4.47,6.8,6.8,0,0,1-4.3,1.53,6.17,6.17,0,0,1-4-1.36,4.49,4.49,0,0,1-1.67-3.66,5.32,5.32,0,0,1,2-4.42,7.08,7.08,0,0,1,4.47-1.58A5.77,5.77,0,0,1,499.38,72,4.54,4.54,0,0,1,501,75.62Z"/></g></g></svg>
	    </a>
		
	    <nav class="onlymobile">
	    	<div class="mobile-menu hamburger" id="hamburger-menu" data-action="toggleMenu">
	          <span class="line"></span>
	          <span class="line"></span>
	          <span class="line"></span>
	        </div>
			<div class="nav_wrapper">
		    	<?php /*  MENU MOBILE */
					wp_nav_menu( array(
				  		'theme_location'  =>'menumobile' ,
						'container'       => '',
						'container_class' => false,
						)
		        	);
		        ?>

    	        <ul class="secondary-menu">
    	        	<li class="secondary-item"><a id="btn-search" class="btn btn--search"><svg class="icon icon--search"><use xlink:href="#icon-search"></use></svg></a></li>
    	        </ul>


    	        <ul class="bottom_menu">
    	        	<li class="lingua_mobile"><?php custom_language_selector_mobile(); ?></li>
    	        	<li class="where-to-by">
    	        		<a href="<?php echo get_permalink( icl_object_id(62, 'page', false) );?>">
    	 				<svg enable-background="new 0 0 512 512" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><path d="m256 0c-99.252 0-180 80.748-180 180 0 33.534 9.289 66.26 26.869 94.652l142.885 230.257c2.737 4.411 7.559 7.091 12.745 7.091h.119c5.231-.041 10.063-2.804 12.75-7.292l139.243-232.488c16.61-27.792 25.389-59.681 25.389-92.22 0-99.252-80.748-180-180-180zm128.866 256.818-126.594 211.368-129.905-209.34c-14.633-23.632-22.567-50.896-22.567-78.846 0-82.71 67.49-150.2 150.2-150.2s150.1 67.49 150.1 150.2c0 27.121-7.411 53.688-21.234 76.818z"/><path d="m256 90c-49.626 0-90 40.374-90 90 0 49.309 39.717 90 90 90 50.903 0 90-41.233 90-90 0-49.626-40.374-90-90-90zm0 150.2c-33.257 0-60.2-27.033-60.2-60.2 0-33.084 27.116-60.2 60.2-60.2s60.1 27.116 60.1 60.2c0 32.683-26.316 60.2-60.1 60.2z"/></svg>
    	 				<?php _e('Where to buy', 'webkolm') ?>
    	 				</a>
    	 			</li>
    	        </ul>
    	        

    	        
		        
	        </div>

 			<!-- PULSANTE CARRELLO -->
			<a class="search_button onlymobile" href="<?php echo WC()->cart->get_cart_url(); ?>">	 
				<?php get_template_part('img/shopper.svg'); ?>
				(<?php
					if(WC()->cart->cart_contents_count>0)
				    {
				    	echo WC()->cart->cart_contents_count;
				    }
				    else{
				    	echo '0';	
				    }
				 ?>)
	        </a>
	    </nav>

	    
	    
	    <nav class="nomobile" id="wk_sticky">
    	   	<ul class="link_left">
    			<li><?php custom_language_selector(); ?></li>
    		</ul>

	    	<?php  /*  MENU PRINCIPALE */
	        	wp_nav_menu( array(
	                'theme_location'   => 'menuwide',
	                'container'       => 'ul',
	                'menu_id'         => 'menu_wide'
	              )
	            ); 
			?>

			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home" class="titolo_sito">
				<svg id="logosito" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 501 147.25"><g data-name="Livello 2"><g id="payoff" data-name="Livello 1"><polygon points="161.13 125.03 166.76 140.93 172.36 125.03 178.29 125.03 178.29 146.95 173.76 146.95 173.76 140.96 174.22 130.62 168.3 146.95 165.2 146.95 159.3 130.63 159.75 140.96 159.75 146.95 155.24 146.95 155.24 125.03 161.13 125.03" style="fill:#1e1c1a"/><rect x="197.93" y="125.04" width="4.52" height="21.91" style="fill:#1e1c1a"/><polygon points="226.56 143.32 236.15 143.32 236.15 146.95 222.05 146.95 222.05 125.03 226.56 125.03 226.56 143.32" style="fill:#1e1c1a"/><path d="M260.11,138.78h5.48l-2.76-8.21Zm6.7,3.65h-7.92l-1.5,4.52h-4.81L260.74,125h4.19l8.2,21.91h-4.8Z" style="fill:#1e1c1a"/><polygon points="308.41 146.95 303.89 146.95 295.1 132.53 295.1 146.95 290.59 146.95 290.59 125.03 295.1 125.03 303.91 139.48 303.91 125.03 308.41 125.03 308.41 146.95" style="fill:#1e1c1a"/><path d="M341.19,135.49a9.14,9.14,0,0,0-1.24-5.23,4.35,4.35,0,0,0-7,0,9,9,0,0,0-1.25,5.18v1.07a9.38,9.38,0,0,0,1.23,5.21,4,4,0,0,0,3.55,1.85,4,4,0,0,0,3.49-1.79,9.22,9.22,0,0,0,1.24-5.2Zm4.57,1a13.22,13.22,0,0,1-1.14,5.67,8.62,8.62,0,0,1-3.27,3.77,9.69,9.69,0,0,1-9.75,0,8.7,8.7,0,0,1-3.31-3.74,12.87,12.87,0,0,1-1.19-5.59v-1.08a13.13,13.13,0,0,1,1.17-5.7,8.64,8.64,0,0,1,3.29-3.78,9.64,9.64,0,0,1,9.74,0,8.73,8.73,0,0,1,3.3,3.78,13.07,13.07,0,0,1,1.16,5.68Z" style="fill:#1e1c1a"/></g><g id="logo"><path d="M28.93,44.41A20.63,20.63,0,0,0,33,44a10.44,10.44,0,0,0,3.58-1.47,16.5,16.5,0,0,0,5.64-6.11,17.6,17.6,0,0,0,2.06-8.52,17.61,17.61,0,0,0-1.56-7.36,18.87,18.87,0,0,0-4.3-6,21.3,21.3,0,0,0-6.31-4.09A19.11,19.11,0,0,0,24.46,9h-12V44.41ZM2.57,6,0,1.75H29.38A25.41,25.41,0,0,1,39.15,3.6,24.51,24.51,0,0,1,47,8.73a24.2,24.2,0,0,1,5.25,7.58,22.75,22.75,0,0,1,1.9,9.22,22.19,22.19,0,0,1-3.24,11.84,23.46,23.46,0,0,1-8.72,8.35l-4.46,2.51a29.75,29.75,0,0,1-6.15,2.4,26.15,26.15,0,0,1-6.81.87H12.51V81.18H2.57Z"/><path d="M102.32,47.9,87.35,15l-14.63,33ZM83.33,6l-2-4.25H92.16l36.08,79.43H117.4L105.56,55H69.48L59.54,76.93,61,81.18H49Z"/><path d="M193.36,72.13a31.1,31.1,0,0,0,11.51-12.28A36,36,0,0,0,209,42.77,34.65,34.65,0,0,0,206.26,29a35.37,35.37,0,0,0-18.82-18.94,35.08,35.08,0,0,0-14-2.78,46.79,46.79,0,0,0-8.21.71A19.64,19.64,0,0,0,158,10.91a31.07,31.07,0,0,0-11.5,12.28,35.85,35.85,0,0,0-4.14,17.08A34.82,34.82,0,0,0,145.05,54a36.27,36.27,0,0,0,7.48,11.3A34.56,34.56,0,0,0,163.82,73,35.73,35.73,0,0,0,178,75.73,45.86,45.86,0,0,0,186.1,75,19.56,19.56,0,0,0,193.36,72.13Zm1,5.35a46.52,46.52,0,0,1-9.83,4,40.85,40.85,0,0,1-11.06,1.47A42,42,0,0,1,144,71.15a39.91,39.91,0,0,1-8.77-12.77A38.7,38.7,0,0,1,132,42.77a37.91,37.91,0,0,1,5.58-20.29A42.49,42.49,0,0,1,152.48,8l4.35-2.4a40.76,40.76,0,0,1,9.95-4.15A42.46,42.46,0,0,1,178,0a41.69,41.69,0,0,1,29.38,11.79,39.9,39.9,0,0,1,8.77,12.82A40.14,40.14,0,0,1,217.83,51a40.57,40.57,0,0,1-4.14,9.66,39.49,39.49,0,0,1-6.42,8.18A40.81,40.81,0,0,1,199,75.07Z"/><polygon points="233.69 6 231.12 1.75 243.63 1.75 243.63 73.98 278.82 73.98 275.91 81.19 233.69 81.19 233.69 6"/><path d="M335.67,47.9,320.71,15l-14.64,33ZM316.68,6l-2-4.25h10.84l36.08,79.43H350.76L338.91,55H302.83l-9.94,21.93,1.45,4.25H282.39Z"/><path d="M472.07,75.07q-2.69,1.65-5.25,3a37.29,37.29,0,0,1-10.34,3.77,59.35,59.35,0,0,1-12,1.14,42.27,42.27,0,0,1-29.55-11.78,40.38,40.38,0,0,1-8.88-12.77,38.09,38.09,0,0,1-3.24-15.61,39,39,0,0,1,1.45-10.69,39.47,39.47,0,0,1,4.14-9.6,38.88,38.88,0,0,1,6.48-8.13,40.75,40.75,0,0,1,8.48-6.27l4.47-2.51a45.5,45.5,0,0,1,10-4.1,41,41,0,0,1,11-1.47A51,51,0,0,1,471,4.8l-3.35,8.84A49.36,49.36,0,0,0,457,9a43,43,0,0,0-12.4-1.69,39.61,39.61,0,0,0-8.1.82,25.23,25.23,0,0,0-7.43,2.78,32.61,32.61,0,0,0-11.73,12.33,34.86,34.86,0,0,0-4.24,17A33.55,33.55,0,0,0,415.94,54a36.89,36.89,0,0,0,7.7,11.3,36,36,0,0,0,25.53,10.42A44.59,44.59,0,0,0,461,74.31a47.79,47.79,0,0,0,10.22-4.15Z"/><path d="M501,75.62a5.31,5.31,0,0,1-2,4.47,6.8,6.8,0,0,1-4.3,1.53,6.17,6.17,0,0,1-4-1.36,4.49,4.49,0,0,1-1.67-3.66,5.32,5.32,0,0,1,2-4.42,7.08,7.08,0,0,1,4.47-1.58A5.77,5.77,0,0,1,499.38,72,4.54,4.54,0,0,1,501,75.62Z"/></g></g></svg>
			</a>

			<ul id="menu_wide_right" class="menu"></ul>

		 	<ul class="link_right">
		 		<li>
		 			<!-- PULSANTE CARRELLO -->
					<a class="search_button nomobile" href="<?php echo WC()->cart->get_cart_url(); ?>">	 
						<?php get_template_part('img/shopper.svg'); ?>
						(<?php
							if(WC()->cart->cart_contents_count>0)
						    {
						    	echo WC()->cart->cart_contents_count;
						    }
						    else{
						    	echo '0';	
						    }
						 ?>)
			        </a>
			    </li>
		 	</ul>
		</nav>



 	</div> <!-- WRAP HEAD-->
	
</header>


<div class="contenuti-pagina">

<?php 
if(is_single()){ ?>

	<div class="wrap">

<?php } ?>
