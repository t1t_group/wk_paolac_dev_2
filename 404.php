<?php
/*
Template Name: 404
*/
?>

<?php get_header(); ?>


<div class="page-contatti">
		<div class="row">
			<div class="col-12">
				<h2 class="titolo_categoria">404</h2>
			</div>
		</div>
		
		<div class="row">
			<div class="col-12">
				<p><?php _e('The page you are searching is not available, sorry!', 'paolac'); ?></p>
			</div>
		</div>
	</div>
<?php get_footer(); ?>

